# consulatApp
This application was generated using JHipster 4.10.0, you can find documentation and help at [http://www.jhipster.tech/documentation-archive/v4.10.0](http://www.jhipster.tech/documentation-archive/v4.10.0).

## Base de donneées

L'application utilise la SGBD posgresql. Il faut se connecter (psql postgres) et creer:

1/ La base de données nommée "consulat"
CREATE DABABASE consulat

2/ L'utilisateur "consulat" avec mot de passe "consulat" par defaut
CREATE USER consulat WITH PASSWORD 'consulat';

3/ Donner tous les privileges à l'utilisateur "consulat"
GRANT ALL PRIVILEGES ON DATABASE consulat TO consulat;

Les tables seront créées par liquibase au premier lancemebt de l'application (en developpement ou en production).


## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

0. java 1.8: because of springboot version  used, java 11 and above won't work
1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.
2. [Npm] or [Yarn]: use one of these to manage Node dependencies.
  
After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

    npm install or yarn install

We use [Gulp][] as our build system. Install the Gulp command-line tool globally with:

    npm global add gulp-cli

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./mvnw
    gulp


## Building for production

To optimize the consulatApp application for production, run:

    ./mvnw -Pprod -DskipTests clean package

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

    java -jar target/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.



## consulatApp fonctionalites

Administrator de l'application 
-------------------------------
   login=consulat, password=admin

Cet administrateur a pour roles :
  - creation des consulats
  - creation des utilisateurs (ie  administrateurs des consulats. Il faut leur mettre le profil PROFIL_CONSULAT_ADMIN et indiquer le consulat qu'ils doivent administrer).
  - saisie des pays
  - saisie des etats
  - saisie des villes
  - saisie des types d'association et des associations

Administrateur d'un consulat
-------------------------------

Il peut:

- saisir les ressortissants
- saisir les types d'association et les associations
- saisir les informations  et les evenements du consulat ou d'une association

## Using Docker to simplify development (optional)

You can use Docker to improve your JHipster development experience. A number of docker-compose configuration are available in the [src/main/docker](src/main/docker) folder to launch required third party services.
For example, to start a postgresql database in a docker container, run:

    docker-compose -f src/main/docker/postgresql.yml up -d

To stop it and remove the container, run:

    docker-compose -f src/main/docker/postgresql.yml down

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

    ./mvnw package -Pprod dockerfile:build

Then run:

    docker-compose -f src/main/docker/app.yml up -d

