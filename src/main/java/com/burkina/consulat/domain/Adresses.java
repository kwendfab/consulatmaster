package com.burkina.consulat.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Adresses.
 */
@Entity
@Table(name = "adresses")
public class Adresses implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rue")
    private String rue;

    @Column(name = "no_porte")
    private String noPorte;

    @Column(name = "mail")
    private String mail;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "autre")
    private String autre;

    @Column(name = "ville")
    private String ville;

    @Column(name = "etat")
    private String etat;

    @Column(name = "actuel")
    private Boolean actuel;

    @ManyToOne
    private Members members;

    @ManyToOne
    private City city;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRue() {
        return rue;
    }

    public Adresses rue(String rue) {
        this.rue = rue;
        return this;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getNoPorte() {
        return noPorte;
    }

    public Adresses noPorte(String noPorte) {
        this.noPorte = noPorte;
        return this;
    }

    public void setNoPorte(String noPorte) {
        this.noPorte = noPorte;
    }

    public String getMail() {
        return mail;
    }

    public Adresses mail(String mail) {
        this.mail = mail;
        return this;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public Adresses telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getZipCode() {
        return zipCode;
    }

    public Adresses zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAutre() {
        return autre;
    }

    public Adresses autre(String autre) {
        this.autre = autre;
        return this;
    }

    public void setAutre(String autre) {
        this.autre = autre;
    }

    public String getVille() {
        return ville;
    }

    public Adresses ville(String ville) {
        this.ville = ville;
        return this;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getEtat() {
        return etat;
    }

    public Adresses etat(String etat) {
        this.etat = etat;
        return this;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Boolean isActuel() {
        return actuel;
    }

    public Adresses actuel(Boolean actuel) {
        this.actuel = actuel;
        return this;
    }

    public void setActuel(Boolean actuel) {
        this.actuel = actuel;
    }

    public Members getMembers() {
        return members;
    }

    public Adresses members(Members members) {
        this.members = members;
        return this;
    }

    public void setMembers(Members members) {
        this.members = members;
    }

    public City getCity() {
        return city;
    }

    public Adresses city(City city) {
        this.city = city;
        return this;
    }

    public void setCity(City city) {
        this.city = city;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Adresses adresses = (Adresses) o;
        if (adresses.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), adresses.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Adresses{" +
            "id=" + getId() +
            ", rue='" + getRue() + "'" +
            ", noPorte='" + getNoPorte() + "'" +
            ", mail='" + getMail() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", zipCode='" + getZipCode() + "'" +
            ", autre='" + getAutre() + "'" +
            ", ville='" + getVille() + "'" +
            ", etat='" + getEtat() + "'" +
            ", actuel='" + isActuel() + "'" +
            "}";
    }
}
