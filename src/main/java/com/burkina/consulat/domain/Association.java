package com.burkina.consulat.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Association.
 */
@Entity
@Table(name = "association")
public class Association implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @Column(name = "nom")
    private String nom;

    @Column(name = "poste")
    private String poste;

    @Column(name = "date_creation")
    private LocalDate dateCreation;

    @Column(name = "jhi_type")
    private String type;

    @OneToMany(mappedBy = "association")
    @JsonIgnore
    private Set<InfoAssociation> infoAssociations = new HashSet<>();

    @OneToMany(mappedBy = "association")
    @JsonIgnore
    private Set<InfoPoste> infoPostes = new HashSet<>();

    @OneToOne
    @JoinColumn(unique = true)
    private Adresses adresses;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public Association numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public Association nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPoste() {
        return poste;
    }

    public Association poste(String poste) {
        this.poste = poste;
        return this;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public LocalDate getDateCreation() {
        return dateCreation;
    }

    public Association dateCreation(LocalDate dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(LocalDate dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getType() {
        return type;
    }

    public Association type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<InfoAssociation> getInfoAssociations() {
        return infoAssociations;
    }

    public Association infoAssociations(Set<InfoAssociation> infoAssociations) {
        this.infoAssociations = infoAssociations;
        return this;
    }


    public Set<InfoPoste> getInfoPostes() {
        return infoPostes;
    }

    public void setInfoPostes(Set<InfoPoste> infoPostes) {
        this.infoPostes = infoPostes;
    }

    public Association addInfoAssociation(InfoAssociation infoAssociation) {
        this.infoAssociations.add(infoAssociation);
        infoAssociation.setAssociation(this);
        return this;
    }

    public Association removeInfoAssociation(InfoAssociation infoAssociation) {
        this.infoAssociations.remove(infoAssociation);
        infoAssociation.setAssociation(null);
        return this;
    }

    public void setInfoAssociations(Set<InfoAssociation> infoAssociations) {
        this.infoAssociations = infoAssociations;
    }

    public Adresses getAdresses() {
        return adresses;
    }

    public Association adresses(Adresses adresses) {
        this.adresses = adresses;
        return this;
    }

    public void setAdresses(Adresses adresses) {
        this.adresses = adresses;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Association association = (Association) o;
        if (association.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), association.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Association{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", nom='" + getNom() + "'" +
            ", poste='" + getPoste() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
