package com.burkina.consulat.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.burkina.consulat.domain.enumeration.DocId;

/**
 * A BfIdDoc.
 */
@Entity
@Table(name = "bf_id_doc")
public class BfIdDoc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @Column(name = "autorite")
    private String autorite;

    @Enumerated(EnumType.STRING)
    @Column(name = "doc_id")
    private DocId docId;

    @Column(name = "date_delivr")
    private LocalDate dateDelivr;

    @Column(name = "date_expr")
    private LocalDate dateExpr;

    @Column(name = "ville")
    private String ville;

    @ManyToOne
    private City city;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public BfIdDoc numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAutorite() {
        return autorite;
    }

    public BfIdDoc autorite(String autorite) {
        this.autorite = autorite;
        return this;
    }

    public void setAutorite(String autorite) {
        this.autorite = autorite;
    }

    public DocId getDocId() {
        return docId;
    }

    public BfIdDoc docId(DocId docId) {
        this.docId = docId;
        return this;
    }

    public void setDocId(DocId docId) {
        this.docId = docId;
    }

    public LocalDate getDateDelivr() {
        return dateDelivr;
    }

    public BfIdDoc dateDelivr(LocalDate dateDelivr) {
        this.dateDelivr = dateDelivr;
        return this;
    }

    public void setDateDelivr(LocalDate dateDelivr) {
        this.dateDelivr = dateDelivr;
    }

    public LocalDate getDateExpr() {
        return dateExpr;
    }

    public BfIdDoc dateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
        return this;
    }

    public void setDateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
    }

    public String getVille() {
        return ville;
    }

    public BfIdDoc ville(String ville) {
        this.ville = ville;
        return this;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public City getCity() {
        return city;
    }

    public BfIdDoc city(City city) {
        this.city = city;
        return this;
    }

    public void setCity(City city) {
        this.city = city;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BfIdDoc bfIdDoc = (BfIdDoc) o;
        if (bfIdDoc.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bfIdDoc.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BfIdDoc{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", autorite='" + getAutorite() + "'" +
            ", docId='" + getDocId() + "'" +
            ", dateDelivr='" + getDateDelivr() + "'" +
            ", dateExpr='" + getDateExpr() + "'" +
            ", ville='" + getVille() + "'" +
            "}";
    }
}
