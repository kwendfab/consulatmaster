package com.burkina.consulat.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Consulat.
 */
@Entity
@Table(name = "consulat")
public class Consulat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "nom")
    private String nom;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @Lob
    @Column(name = "logo_bf")
    private byte[] logoBf;

    @Column(name = "logo_bf_content_type")
    private String logoBfContentType;

    @Lob
    @Column(name = "logo_usa")
    private byte[] logoUsa;

    @Column(name = "logo_usa_content_type")
    private String logoUsaContentType;

    @Lob
    @Column(name = "armoirie")
    private byte[] armoirie;

    @Column(name = "armoirie_content_type")
    private String armoirieContentType;

    @Lob
    @Column(name = "signature")
    private byte[] signature;

    @Column(name = "signature_content_type")
    private String signatureContentType;

    @Size(max = 50)
    @Column(name = "en_tete", length = 50)
    private String enTete;

    @Size(max = 50)
    @Column(name = "titre", length = 50)
    private String titre;

    @Lob
    @Column(name = "image_verso")
    private byte[] imageVerso;

    @Column(name = "image_verso_content_type")
    private String imageVersoContentType;

    @Size(max = 1000)
    @Column(name = "texte_verso", length = 1000)
    private String texteVerso;

    @Size(max = 10)
    @Column(name = "code_carte", length = 10)
    private String codeCarte;

    @OneToOne
    @JoinColumn(unique = true)
    private UserExtra userExtra;

    @OneToOne
    @JoinColumn(unique = true)
    private Adresses adresses;

    @OneToOne
    @JoinColumn(unique = true)
    private Country country;

    @OneToMany(mappedBy = "consulat")
    @JsonIgnore
    private Set<Members> members = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Consulat code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public Consulat nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public Consulat description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getLogoBf() {
        return logoBf;
    }

    public Consulat logoBf(byte[] logoBf) {
        this.logoBf = logoBf;
        return this;
    }

    public void setLogoBf(byte[] logoBf) {
        this.logoBf = logoBf;
    }

    public String getLogoBfContentType() {
        return logoBfContentType;
    }

    public Consulat logoBfContentType(String logoBfContentType) {
        this.logoBfContentType = logoBfContentType;
        return this;
    }

    public void setLogoBfContentType(String logoBfContentType) {
        this.logoBfContentType = logoBfContentType;
    }

    public byte[] getLogoUsa() {
        return logoUsa;
    }

    public Consulat logoUsa(byte[] logoUsa) {
        this.logoUsa = logoUsa;
        return this;
    }

    public void setLogoUsa(byte[] logoUsa) {
        this.logoUsa = logoUsa;
    }

    public String getLogoUsaContentType() {
        return logoUsaContentType;
    }

    public Consulat logoUsaContentType(String logoUsaContentType) {
        this.logoUsaContentType = logoUsaContentType;
        return this;
    }

    public void setLogoUsaContentType(String logoUsaContentType) {
        this.logoUsaContentType = logoUsaContentType;
    }

    public byte[] getArmoirie() {
        return armoirie;
    }

    public Consulat armoirie(byte[] armoirie) {
        this.armoirie = armoirie;
        return this;
    }

    public void setArmoirie(byte[] armoirie) {
        this.armoirie = armoirie;
    }

    public String getArmoirieContentType() {
        return armoirieContentType;
    }

    public Consulat armoirieContentType(String armoirieContentType) {
        this.armoirieContentType = armoirieContentType;
        return this;
    }

    public void setArmoirieContentType(String armoirieContentType) {
        this.armoirieContentType = armoirieContentType;
    }

    public byte[] getSignature() {
        return signature;
    }

    public Consulat signature(byte[] signature) {
        this.signature = signature;
        return this;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public String getSignatureContentType() {
        return signatureContentType;
    }

    public Consulat signatureContentType(String signatureContentType) {
        this.signatureContentType = signatureContentType;
        return this;
    }

    public void setSignatureContentType(String signatureContentType) {
        this.signatureContentType = signatureContentType;
    }

    public String getEnTete() {
        return enTete;
    }

    public Consulat enTete(String enTete) {
        this.enTete = enTete;
        return this;
    }

    public void setEnTete(String enTete) {
        this.enTete = enTete;
    }

    public String getTitre() {
        return titre;
    }

    public Consulat titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public byte[] getImageVerso() {
        return imageVerso;
    }

    public Consulat imageVerso(byte[] imageVerso) {
        this.imageVerso = imageVerso;
        return this;
    }

    public void setImageVerso(byte[] imageVerso) {
        this.imageVerso = imageVerso;
    }

    public String getImageVersoContentType() {
        return imageVersoContentType;
    }

    public Consulat imageVersoContentType(String imageVersoContentType) {
        this.imageVersoContentType = imageVersoContentType;
        return this;
    }

    public void setImageVersoContentType(String imageVersoContentType) {
        this.imageVersoContentType = imageVersoContentType;
    }

    public String getTexteVerso() {
        return texteVerso;
    }

    public Consulat texteVerso(String texteVerso) {
        this.texteVerso = texteVerso;
        return this;
    }

    public void setTexteVerso(String texteVerso) {
        this.texteVerso = texteVerso;
    }

    public String getCodeCarte() {
        return codeCarte;
    }

    public Consulat codeCarte(String codeCarte) {
        this.codeCarte = codeCarte;
        return this;
    }

    public void setCodeCarte(String codeCarte) {
        this.codeCarte = codeCarte;
    }

    public UserExtra getUserExtra() {
        return userExtra;
    }

    public Consulat userExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
        return this;
    }

    public void setUserExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
    }

    public Adresses getAdresses() {
        return adresses;
    }

    public Consulat adresses(Adresses adresses) {
        this.adresses = adresses;
        return this;
    }

    public void setAdresses(Adresses adresses) {
        this.adresses = adresses;
    }

    public Country getCountry() {
        return country;
    }

    public Consulat country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Set<Members> getMembers() {
        return members;
    }

    public Consulat members(Set<Members> members) {
        this.members = members;
        return this;
    }

    public Consulat addMembers(Members members) {
        this.members.add(members);
        members.setConsulat(this);
        return this;
    }

    public Consulat removeMembers(Members members) {
        this.members.remove(members);
        members.setConsulat(null);
        return this;
    }

    public void setMembers(Set<Members> members) {
        this.members = members;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Consulat consulat = (Consulat) o;
        if (consulat.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), consulat.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Consulat{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", nom='" + getNom() + "'" +
            ", description='" + getDescription() + "'" +
            ", logoBf='" + getLogoBf() + "'" +
            ", logoBfContentType='" + logoBfContentType + "'" +
            ", logoUsa='" + getLogoUsa() + "'" +
            ", logoUsaContentType='" + logoUsaContentType + "'" +
            ", armoirie='" + getArmoirie() + "'" +
            ", armoirieContentType='" + armoirieContentType + "'" +
            ", signature='" + getSignature() + "'" +
            ", signatureContentType='" + signatureContentType + "'" +
            ", enTete='" + getEnTete() + "'" +
            ", titre='" + getTitre() + "'" +
            ", imageVerso='" + getImageVerso() + "'" +
            ", imageVersoContentType='" + imageVersoContentType + "'" +
            ", texteVerso='" + getTexteVerso() + "'" +
            ", codeCarte='" + getCodeCarte() + "'" +
            "}";
    }
}
