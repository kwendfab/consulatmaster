package com.burkina.consulat.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Country.
 */
@Entity
@Table(name = "country")
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "libele")
    private String libele;

    @OneToMany(mappedBy = "country")
    @JsonIgnore
    private Set<States> states = new HashSet<>();

    @OneToMany(mappedBy = "countries")
    @JsonIgnore
    private Set<City> cities = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Country code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibele() {
        return libele;
    }

    public Country libele(String libele) {
        this.libele = libele;
        return this;
    }

    public void setLibele(String libele) {
        this.libele = libele;
    }

    public Set<States> getStates() {
        return states;
    }

    public Country states(Set<States> states) {
        this.states = states;
        return this;
    }

    public Country addStates(States states) {
        this.states.add(states);
        states.setCountry(this);
        return this;
    }

    public Country removeStates(States states) {
        this.states.remove(states);
        states.setCountry(null);
        return this;
    }

    public void setStates(Set<States> states) {
        this.states = states;
    }

    public Set<City> getCities() {
        return cities;
    }

    public Country cities(Set<City> cities) {
        this.cities = cities;
        return this;
    }

    public Country addCity(City city) {
        this.cities.add(city);
        city.setCountries(this);
        return this;
    }

    public Country removeCity(City city) {
        this.cities.remove(city);
        city.setCountries(null);
        return this;
    }

    public void setCities(Set<City> cities) {
        this.cities = cities;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Country country = (Country) o;
        if (country.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), country.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Country{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", libele='" + getLibele() + "'" +
            "}";
    }
}
