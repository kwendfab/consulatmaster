package com.burkina.consulat.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A CountryIdDoc.
 */
@Entity
@Table(name = "country_id_doc")
public class CountryIdDoc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @Column(name = "autorite")
    private String autorite;

    @Column(name = "date_delivr")
    private LocalDate dateDelivr;

    @Column(name = "date_expr")
    private LocalDate dateExpr;

    @Column(name = "etat")
    private String etat;

    @Column(name = "ville")
    private String ville;

    @ManyToOne
    private City city;

    @ManyToOne
    private States states;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public CountryIdDoc numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAutorite() {
        return autorite;
    }

    public CountryIdDoc autorite(String autorite) {
        this.autorite = autorite;
        return this;
    }

    public void setAutorite(String autorite) {
        this.autorite = autorite;
    }

    public LocalDate getDateDelivr() {
        return dateDelivr;
    }

    public CountryIdDoc dateDelivr(LocalDate dateDelivr) {
        this.dateDelivr = dateDelivr;
        return this;
    }

    public void setDateDelivr(LocalDate dateDelivr) {
        this.dateDelivr = dateDelivr;
    }

    public LocalDate getDateExpr() {
        return dateExpr;
    }

    public CountryIdDoc dateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
        return this;
    }

    public void setDateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
    }

    public String getEtat() {
        return etat;
    }

    public CountryIdDoc etat(String etat) {
        this.etat = etat;
        return this;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getVille() {
        return ville;
    }

    public CountryIdDoc ville(String ville) {
        this.ville = ville;
        return this;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public City getCity() {
        return city;
    }

    public CountryIdDoc city(City city) {
        this.city = city;
        return this;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public States getStates() {
        return states;
    }

    public CountryIdDoc states(States states) {
        this.states = states;
        return this;
    }

    public void setStates(States states) {
        this.states = states;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CountryIdDoc countryIdDoc = (CountryIdDoc) o;
        if (countryIdDoc.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), countryIdDoc.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CountryIdDoc{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", autorite='" + getAutorite() + "'" +
            ", dateDelivr='" + getDateDelivr() + "'" +
            ", dateExpr='" + getDateExpr() + "'" +
            ", etat='" + getEtat() + "'" +
            ", ville='" + getVille() + "'" +
            "}";
    }
}
