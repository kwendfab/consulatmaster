package com.burkina.consulat.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DocumentLegal.
 */
@Entity
@Table(name = "document_legal")
public class DocumentLegal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @Column(name = "date_expr")
    private LocalDate dateExpr;

    @Column(name = "libelle")
    private String libelle;

    @ManyToOne
    private Country country;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public DocumentLegal numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public LocalDate getDateExpr() {
        return dateExpr;
    }

    public DocumentLegal dateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
        return this;
    }

    public void setDateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
    }

    public String getLibelle() {
        return libelle;
    }

    public DocumentLegal libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Country getCountry() {
        return country;
    }

    public DocumentLegal country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DocumentLegal documentLegal = (DocumentLegal) o;
        if (documentLegal.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documentLegal.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DocumentLegal{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", dateExpr='" + getDateExpr() + "'" +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}
