package com.burkina.consulat.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A InfoParents.
 */
@Entity
@Table(name = "info_parents")
public class InfoParents implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nom_pere")
    private String nomPere;

    @Column(name = "prenom_pere")
    private String prenomPere;

    @Column(name = "nom_mere")
    private String nomMere;

    @Column(name = "prenom_mere")
    private String prenomMere;

    @Column(name = "no_pere")
    private String noPere;

    @Column(name = "no_mere")
    private String noMere;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomPere() {
        return nomPere;
    }

    public InfoParents nomPere(String nomPere) {
        this.nomPere = nomPere;
        return this;
    }

    public void setNomPere(String nomPere) {
        this.nomPere = nomPere;
    }

    public String getPrenomPere() {
        return prenomPere;
    }

    public InfoParents prenomPere(String prenomPere) {
        this.prenomPere = prenomPere;
        return this;
    }

    public void setPrenomPere(String prenomPere) {
        this.prenomPere = prenomPere;
    }

    public String getNomMere() {
        return nomMere;
    }

    public InfoParents nomMere(String nomMere) {
        this.nomMere = nomMere;
        return this;
    }

    public void setNomMere(String nomMere) {
        this.nomMere = nomMere;
    }

    public String getPrenomMere() {
        return prenomMere;
    }

    public InfoParents prenomMere(String prenomMere) {
        this.prenomMere = prenomMere;
        return this;
    }

    public void setPrenomMere(String prenomMere) {
        this.prenomMere = prenomMere;
    }

    public String getNoPere() {
        return noPere;
    }

    public InfoParents noPere(String noPere) {
        this.noPere = noPere;
        return this;
    }

    public void setNoPere(String noPere) {
        this.noPere = noPere;
    }

    public String getNoMere() {
        return noMere;
    }

    public InfoParents noMere(String noMere) {
        this.noMere = noMere;
        return this;
    }

    public void setNoMere(String noMere) {
        this.noMere = noMere;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InfoParents infoParents = (InfoParents) o;
        if (infoParents.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), infoParents.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InfoParents{" +
            "id=" + getId() +
            ", nomPere='" + getNomPere() + "'" +
            ", prenomPere='" + getPrenomPere() + "'" +
            ", nomMere='" + getNomMere() + "'" +
            ", prenomMere='" + getPrenomMere() + "'" +
            ", noPere='" + getNoPere() + "'" +
            ", noMere='" + getNoMere() + "'" +
            "}";
    }
}
