package com.burkina.consulat.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A InfoPoste.
 */
@Entity
@Table(name = "info_poste")
public class InfoPoste implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date_debut")
    private LocalDate dateDebut;

    @Column(name = "date_fin")
    private LocalDate dateFin;

    @Column(name = "actuelle")
    private Boolean actuelle;

    @ManyToOne
    private Members members;

    @ManyToOne
    private Poste poste;

    @ManyToOne
    private Association association;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public InfoPoste dateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
        return this;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public InfoPoste dateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
        return this;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public Boolean isActuelle() {
        return actuelle;
    }

    public InfoPoste actuelle(Boolean actuelle) {
        this.actuelle = actuelle;
        return this;
    }

    public void setActuelle(Boolean actuelle) {
        this.actuelle = actuelle;
    }

    public Members getMembers() {
        return members;
    }

    public InfoPoste members(Members members) {
        this.members = members;
        return this;
    }

    public void setMembers(Members members) {
        this.members = members;
    }

    public Poste getPoste() {
        return poste;
    }

    public InfoPoste poste(Poste poste) {
        this.poste = poste;
        return this;
    }

    public void setPoste(Poste poste) {
        this.poste = poste;
    }

    public Association getAssociation() {
        return association;
    }

    public InfoPoste association(Association association) {
        this.association = association;
        return this;
    }

    public void setAssociation(Association association) {
        this.association = association;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InfoPoste infoPoste = (InfoPoste) o;
        if (infoPoste.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), infoPoste.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InfoPoste{" +
            "id=" + getId() +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", actuelle='" + isActuelle() + "'" +
            "}";
    }
}
