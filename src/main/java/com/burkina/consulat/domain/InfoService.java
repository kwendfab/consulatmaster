package com.burkina.consulat.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A InfoService.
 */
@Entity
@Table(name = "info_service")
public class InfoService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date_integration")
    private LocalDate dateIntegration;

    @ManyToOne
    private MembreService membreService;

    @ManyToOne
    private Members members;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateIntegration() {
        return dateIntegration;
    }

    public InfoService dateIntegration(LocalDate dateIntegration) {
        this.dateIntegration = dateIntegration;
        return this;
    }

    public void setDateIntegration(LocalDate dateIntegration) {
        this.dateIntegration = dateIntegration;
    }

    public MembreService getMembreService() {
        return membreService;
    }

    public InfoService membreService(MembreService membreService) {
        this.membreService = membreService;
        return this;
    }

    public void setMembreService(MembreService membreService) {
        this.membreService = membreService;
    }

    public Members getMembers() {
        return members;
    }

    public InfoService members(Members members) {
        this.members = members;
        return this;
    }

    public void setMembers(Members members) {
        this.members = members;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InfoService infoService = (InfoService) o;
        if (infoService.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), infoService.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InfoService{" +
            "id=" + getId() +
            ", dateIntegration='" + getDateIntegration() + "'" +
            "}";
    }
}
