package com.burkina.consulat.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.burkina.consulat.domain.enumeration.Sexe;

/**
 * A Members.
 */
@Entity
@Table(name = "members")
public class Members implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;


    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "date_naissance")
    private LocalDate dateNaissance;

    @Column(name = "date_create")
    private LocalDate dateCreate;

    @Column(name = "date_expr")
    private LocalDate dateExpr;

    @Column(name = "lieu_naissance")
    private String lieuNaissance;

    @Enumerated(EnumType.STRING)
    @Column(name = "sexe")
    private Sexe sexe;

    @Column(name = "mariage")
    private Boolean mariage;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "date_arrivee")
    private LocalDate dateArrivee;

    @Column(name = "ville_arrivee")
    private String villeArrivee;

    @Column(name = "etat_arrivee")
    private String etatArrivee;

    @Column(name = "taille")
    private String taille;


    @Column(name = "metier")
    private String metier;

    @Column(name = "no_conjoint")
    private String noConjoint;

    @Column(name = "meme_pays_conjoint")
    private Boolean memePaysConjoint;

    @Column(name = "nom_conjoint")
    private String nomConjoint;

    @Column(name = "prenom_conjoint")
    private String prenomConjoint;

    @Column(name = "parents_usa")
    private Boolean parentsUsa;

    @Column(name = "mum_in_usa")
    private Boolean mumInUsa;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "photo_content_type")
    private String photoContentType;

    @Lob
    @Column(name = "signature")
    private byte[] signature;

    @Column(name = "signature_content_type")
    private String signatureContentType;

    @Column(name = "occupation")
    private String occupation;

    @Column(name = "ville_residence")
    private String villeResidence;

    @Column(name = "etat_residence")
    private String etatResidence;

    @OneToOne
    @JoinColumn(unique = true)
    private UserExtra userExtra;

    @OneToOne
    @JoinColumn(unique = true)
    private CountryIdDoc country;

    @OneToOne
    @JoinColumn(unique = true)
    private BfIdDoc bfIdDoc;

    @OneToOne
    @JoinColumn(unique = true)
    private DocumentLegal documentLegal;

    @OneToOne
    @JoinColumn(unique = true)
    private InfoParents infoParents;

    @OneToOne
    @JoinColumn(unique = true)
    private City villeArrive;

    @OneToMany(mappedBy = "members")
    @JsonIgnore
    private Set<InfoPoste> infoPostes = new HashSet<>();

    @OneToMany(mappedBy = "members")
    @JsonIgnore
    private Set<Adresses> adresses = new HashSet<>();

    @OneToMany(mappedBy = "members")
    @JsonIgnore
    private Set<InfoService> infoServices = new HashSet<>();

    @ManyToOne
    private Consulat consulat;

    @OneToMany(mappedBy = "members")
    @JsonIgnore
    private Set<InfoAssociation> infoAssociations = new HashSet<>();

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public String getSignatureContentType() {
        return signatureContentType;
    }

    public void setSignatureContentType(String signatureContentType) {
        this.signatureContentType = signatureContentType;
    }

    public LocalDate getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(LocalDate dateCreate) {
        this.dateCreate = dateCreate;
    }

    public LocalDate getDateExpr() {
        return dateExpr;
    }

    public void setDateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
    }

    public Boolean getMariage() {
        return mariage;
    }

    public String getEtatArrivee() {
        return etatArrivee;
    }

    public void setEtatArrivee(String etatArrivee) {
        this.etatArrivee = etatArrivee;
    }

    public Boolean getMemePaysConjoint() {
        return memePaysConjoint;
    }

    public Boolean getParentsUsa() {
        return parentsUsa;
    }

    public Boolean getMumInUsa() {
        return mumInUsa;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public Members numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public Members nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Members prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public Members dateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
        return this;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public Members lieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
        return this;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public Sexe getSexe() {
        return sexe;
    }

    public Members sexe(Sexe sexe) {
        this.sexe = sexe;
        return this;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public Boolean isMariage() {
        return mariage;
    }

    public Members mariage(Boolean mariage) {
        this.mariage = mariage;
        return this;
    }

    public void setMariage(Boolean mariage) {
        this.mariage = mariage;
    }

    public String getTelephone() {
        return telephone;
    }

    public Members telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public LocalDate getDateArrivee() {
        return dateArrivee;
    }

    public Members dateArrivee(LocalDate dateArrivee) {
        this.dateArrivee = dateArrivee;
        return this;
    }

    public void setDateArrivee(LocalDate dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    public String getVilleArrivee() {
        return villeArrivee;
    }

    public Members villeArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
        return this;
    }

    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    public String getMetier() {
        return metier;
    }

    public Members metier(String metier) {
        this.metier = metier;
        return this;
    }

    public void setMetier(String metier) {
        this.metier = metier;
    }

    public String getNoConjoint() {
        return noConjoint;
    }

    public Members noConjoint(String noConjoint) {
        this.noConjoint = noConjoint;
        return this;
    }

    public void setNoConjoint(String noConjoint) {
        this.noConjoint = noConjoint;
    }

    public Boolean isMemePaysConjoint() {
        return memePaysConjoint;
    }

    public Members memePaysConjoint(Boolean memePaysConjoint) {
        this.memePaysConjoint = memePaysConjoint;
        return this;
    }

    public void setMemePaysConjoint(Boolean memePaysConjoint) {
        this.memePaysConjoint = memePaysConjoint;
    }

    public String getNomConjoint() {
        return nomConjoint;
    }

    public Members nomConjoint(String nomConjoint) {
        this.nomConjoint = nomConjoint;
        return this;
    }

    public void setNomConjoint(String nomConjoint) {
        this.nomConjoint = nomConjoint;
    }

    public String getPrenomConjoint() {
        return prenomConjoint;
    }

    public Members prenomConjoint(String prenomConjoint) {
        this.prenomConjoint = prenomConjoint;
        return this;
    }

    public void setPrenomConjoint(String prenomConjoint) {
        this.prenomConjoint = prenomConjoint;
    }

    public Boolean isParentsUsa() {
        return parentsUsa;
    }

    public Members parentsUsa(Boolean parentsUsa) {
        this.parentsUsa = parentsUsa;
        return this;
    }

    public void setParentsUsa(Boolean parentsUsa) {
        this.parentsUsa = parentsUsa;
    }

    public Boolean isMumInUsa() {
        return mumInUsa;
    }

    public Members mumInUsa(Boolean mumInUsa) {
        this.mumInUsa = mumInUsa;
        return this;
    }

    public void setMumInUsa(Boolean mumInUsa) {
        this.mumInUsa = mumInUsa;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public Members photo(byte[] photo) {
        this.photo = photo;
        return this;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public Members photoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
        return this;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public String getOccupation() {
        return occupation;
    }

    public Members occupation(String occupation) {
        this.occupation = occupation;
        return this;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getVilleResidence() {
        return villeResidence;
    }

    public Members villeResidence(String villeResidence) {
        this.villeResidence = villeResidence;
        return this;
    }

    public void setVilleResidence(String villeResidence) {
        this.villeResidence = villeResidence;
    }

    public String getEtatResidence() {
        return etatResidence;
    }

    public Members etatResidence(String etatResidence) {
        this.etatResidence = etatResidence;
        return this;
    }

    public void setEtatResidence(String etatResidence) {
        this.etatResidence = etatResidence;
    }

    public UserExtra getUserExtra() {
        return userExtra;
    }

    public Members userExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
        return this;
    }

    public void setUserExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
    }

    public CountryIdDoc getCountry() {
        return country;
    }

    public Members country(CountryIdDoc countryIdDoc) {
        this.country = countryIdDoc;
        return this;
    }

    public void setCountry(CountryIdDoc countryIdDoc) {
        this.country = countryIdDoc;
    }

    public BfIdDoc getBfIdDoc() {
        return bfIdDoc;
    }

    public Members bfIdDoc(BfIdDoc bFIdDoc) {
        this.bfIdDoc = bFIdDoc;
        return this;
    }

    public void setBfIdDoc(BfIdDoc bFIdDoc) {
        this.bfIdDoc = bFIdDoc;
    }

    public DocumentLegal getDocumentLegal() {
        return documentLegal;
    }

    public Members documentLegal(DocumentLegal documentLegal) {
        this.documentLegal = documentLegal;
        return this;
    }

    public void setDocumentLegal(DocumentLegal documentLegal) {
        this.documentLegal = documentLegal;
    }

    public InfoParents getInfoParents() {
        return infoParents;
    }

    public Members infoParents(InfoParents infoParents) {
        this.infoParents = infoParents;
        return this;
    }

    public void setInfoParents(InfoParents infoParents) {
        this.infoParents = infoParents;
    }

    public City getVilleArrive() {
        return villeArrive;
    }

    public Members villeArrive(City city) {
        this.villeArrive = city;
        return this;
    }

    public void setVilleArrive(City city) {
        this.villeArrive = city;
    }

    public Set<InfoPoste> getInfoPostes() {
        return infoPostes;
    }

    public Members infoPostes(Set<InfoPoste> infoPostes) {
        this.infoPostes = infoPostes;
        return this;
    }

    public Members addInfoPoste(InfoPoste infoPoste) {
        this.infoPostes.add(infoPoste);
        infoPoste.setMembers(this);
        return this;
    }

    public Members removeInfoPoste(InfoPoste infoPoste) {
        this.infoPostes.remove(infoPoste);
        infoPoste.setMembers(null);
        return this;
    }

    public void setInfoPostes(Set<InfoPoste> infoPostes) {
        this.infoPostes = infoPostes;
    }

    public Set<Adresses> getAdresses() {
        return adresses;
    }

    public Members adresses(Set<Adresses> adresses) {
        this.adresses = adresses;
        return this;
    }

    public Members addAdresses(Adresses adresses) {
        this.adresses.add(adresses);
        adresses.setMembers(this);
        return this;
    }

    public Members removeAdresses(Adresses adresses) {
        this.adresses.remove(adresses);
        adresses.setMembers(null);
        return this;
    }

    public void setAdresses(Set<Adresses> adresses) {
        this.adresses = adresses;
    }

    public Set<InfoService> getInfoServices() {
        return infoServices;
    }

    public Members infoServices(Set<InfoService> infoServices) {
        this.infoServices = infoServices;
        return this;
    }

    public Members addInfoService(InfoService infoService) {
        this.infoServices.add(infoService);
        infoService.setMembers(this);
        return this;
    }

    public Members removeInfoService(InfoService infoService) {
        this.infoServices.remove(infoService);
        infoService.setMembers(null);
        return this;
    }

    public void setInfoServices(Set<InfoService> infoServices) {
        this.infoServices = infoServices;
    }

    public Consulat getConsulat() {
        return consulat;
    }

    public Members consulat(Consulat consulat) {
        this.consulat = consulat;
        return this;
    }

    public void setConsulat(Consulat consulat) {
        this.consulat = consulat;
    }

    public Set<InfoAssociation> getInfoAssociations() {
        return infoAssociations;
    }

    public Members infoAssociations(Set<InfoAssociation> infoAssociations) {
        this.infoAssociations = infoAssociations;
        return this;
    }

    public Members addInfoAssociation(InfoAssociation infoAssociation) {
        this.infoAssociations.add(infoAssociation);
        infoAssociation.setMembers(this);
        return this;
    }

    public Members removeInfoAssociation(InfoAssociation infoAssociation) {
        this.infoAssociations.remove(infoAssociation);
        infoAssociation.setMembers(null);
        return this;
    }

    public void setInfoAssociations(Set<InfoAssociation> infoAssociations) {
        this.infoAssociations = infoAssociations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Members members = (Members) o;
        if (members.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), members.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Members{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", dateNaissance='" + getDateNaissance() + "'" +
            ", lieuNaissance='" + getLieuNaissance() + "'" +
            ", sexe='" + getSexe() + "'" +
            ", mariage='" + isMariage() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", dateArrivee='" + getDateArrivee() + "'" +
            ", villeArrivee='" + getVilleArrivee() + "'" +
            ", metier='" + getMetier() + "'" +
            ", noConjoint='" + getNoConjoint() + "'" +
            ", memePaysConjoint='" + isMemePaysConjoint() + "'" +
            ", nomConjoint='" + getNomConjoint() + "'" +
            ", prenomConjoint='" + getPrenomConjoint() + "'" +
            ", parentsUsa='" + isParentsUsa() + "'" +
            ", mumInUsa='" + isMumInUsa() + "'" +
            ", photo='" + getPhoto() + "'" +
            ", photoContentType='" + photoContentType + "'" +
            ", occupation='" + getOccupation() + "'" +
            ", villeResidence='" + getVilleResidence() + "'" +
            ", etatResidence='" + getEtatResidence() + "'" +
            "}";
    }
}
