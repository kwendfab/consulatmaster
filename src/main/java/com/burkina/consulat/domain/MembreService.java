package com.burkina.consulat.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A MembreService.
 */
@Entity
@Table(name = "membre_service")
public class MembreService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "jhi_date")
    private ZonedDateTime date;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "consulat")
    private String consulat;

    @OneToMany(mappedBy = "membreService")
    private Set<InfoService> infoServis = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("membreServices")
    private UserExtra userExtra;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public MembreService numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescription() {
        return description;
    }

    public MembreService description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLibelle() {
        return libelle;
    }

    public MembreService libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public MembreService date(ZonedDateTime date) {
        this.date = date;
        return this;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public MembreService type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConsulat() {
        return consulat;
    }

    public MembreService consulat(String consulat) {
        this.consulat = consulat;
        return this;
    }

    public void setConsulat(String consulat) {
        this.consulat = consulat;
    }

    public Set<InfoService> getInfoServis() {
        return infoServis;
    }

    public MembreService infoServis(Set<InfoService> infoServices) {
        this.infoServis = infoServices;
        return this;
    }

    public MembreService addInfoServi(InfoService infoService) {
        this.infoServis.add(infoService);
        infoService.setMembreService(this);
        return this;
    }

    public MembreService removeInfoServi(InfoService infoService) {
        this.infoServis.remove(infoService);
        infoService.setMembreService(null);
        return this;
    }

    public void setInfoServis(Set<InfoService> infoServices) {
        this.infoServis = infoServices;
    }

    public UserExtra getUserExtra() {
        return userExtra;
    }

    public MembreService userExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
        return this;
    }

    public void setUserExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MembreService)) {
            return false;
        }
        return id != null && id.equals(((MembreService) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MembreService{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", description='" + getDescription() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", date='" + getDate() + "'" +
            ", type='" + getType() + "'" +
            ", consulat='" + getConsulat() + "'" +
            "}";
    }
}
