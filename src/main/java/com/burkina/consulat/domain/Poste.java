package com.burkina.consulat.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Poste.
 */
@Entity
@Table(name = "poste")
public class Poste implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @Column(name = "libelle")
    private String libelle;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @OneToMany(mappedBy = "poste")
    @JsonIgnore
    private Set<InfoPoste> infoOccupations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public Poste numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLibelle() {
        return libelle;
    }

    public Poste libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public Poste description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<InfoPoste> getInfoOccupations() {
        return infoOccupations;
    }

    public Poste infoOccupations(Set<InfoPoste> infoPostes) {
        this.infoOccupations = infoPostes;
        return this;
    }

    public Poste addInfoOccupation(InfoPoste infoPoste) {
        this.infoOccupations.add(infoPoste);
        infoPoste.setPoste(this);
        return this;
    }

    public Poste removeInfoOccupation(InfoPoste infoPoste) {
        this.infoOccupations.remove(infoPoste);
        infoPoste.setPoste(null);
        return this;
    }

    public void setInfoOccupations(Set<InfoPoste> infoPostes) {
        this.infoOccupations = infoPostes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Poste poste = (Poste) o;
        if (poste.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), poste.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Poste{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
