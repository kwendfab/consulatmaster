package com.burkina.consulat.domain.enumeration;

/**
 * The DocId enumeration.
 */
public enum DocId {
    CNIB, PASSPORT
}
