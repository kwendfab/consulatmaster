package com.burkina.consulat.domain.enumeration;

/**
 * The Sexe enumeration.
 */
public enum Sexe {
    MALE, FEMALE
}
