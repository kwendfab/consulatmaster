package com.burkina.consulat.repository;

import com.burkina.consulat.domain.Adresses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Adresses entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdressesRepository extends JpaRepository<Adresses, Long> {
   Adresses findByMembersIdAndActuelIsTrue(Long id);

    @Query("SELECT COUNT(*) From Adresses a where a.city.states.libelle=:etat and a.members is not null")
    Integer countEtatMembre(@Param(value = "etat") String etat);



    @Query(value = "select adress from Adresses adress where (lower(adress.members.nom) like concat('%',lower(:nom),'%') or :nom is null) and (lower(adress.members.prenom) like concat('%',lower(:prenom),'%') or :prenom is null)   ) ",
        countQuery = "select count(*) from Adresses adress where (lower(adress.members.nom) like concat('%',lower(:nom),'%') or :nom is null) and (lower(adress.members.prenom) like concat('%',lower(:prenom),'%') or :prenom is null)  ) ")
    Page<Adresses> findByMultiParamAdress(@Param("nom")String nom, @Param("prenom") String prenom,@Param("pageable")Pageable pageable);
}
