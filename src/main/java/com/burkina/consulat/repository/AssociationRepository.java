package com.burkina.consulat.repository;

import com.burkina.consulat.domain.Association;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.time.LocalDate;


/**
 * Spring Data JPA repository for the Association entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssociationRepository extends JpaRepository<Association, Long> {
    @Query("SELECT coalesce(max(cs.numero),'')  FROM Association cs")
    Long getMaxNumeroAssociation();

    @Query ("SELECT l.nom From Association l")
    String[] TabOfAssoc();



    @Query(value = "select assoc from Association assoc where (lower(assoc.nom) like concat('%',lower(:nom),'%') or :nom is null) and (lower(assoc.type) like concat('%',lower(:type),'%') or :type is null) and (assoc.dateCreation=:dateCreation or :dateCreation is null) and (lower(assoc.adresses.rue) like concat('%',lower(:adresse),'%') or :adresse  is null) ) ",
        countQuery = "select count(*) from Association assoc where (lower(assoc.nom) like concat('%',lower(:nom),'%') or :nom is null) and (lower(assoc.type) like concat('%',lower(:type),'%') or :type is null) and (assoc.dateCreation=:dateCreation or :dateCreation is null) and (lower(assoc.adresses.rue) like concat('%',lower(:adresse),'%') or :adresse  is null) ) ")
    Page<Association> findByMultiParamAssoc(@Param("nom")String nom, @Param("type") String type,@Param("dateCreation")LocalDate dateCreation, @Param("adresse") String adresse, @Param("pageable")Pageable pageable);


}
