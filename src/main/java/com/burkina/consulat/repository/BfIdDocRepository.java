package com.burkina.consulat.repository;

import com.burkina.consulat.domain.BfIdDoc;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the BfIdDoc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BfIdDocRepository extends JpaRepository<BfIdDoc, Long> {

}
