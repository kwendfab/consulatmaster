package com.burkina.consulat.repository;

import com.burkina.consulat.domain.City;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the City entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    List<City> findByStatesId(Long id);
    List<City> findByCountriesId(Long id);

    @Query("SELECT coalesce(max(cs.code),'')  FROM City cs")
    Long getMaxNumeroCity();

}
