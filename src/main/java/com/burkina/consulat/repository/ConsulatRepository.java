package com.burkina.consulat.repository;

import com.burkina.consulat.domain.Consulat;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Consulat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConsulatRepository extends JpaRepository<Consulat, Long> {
    @Query("SELECT coalesce(max(cs.code),'')  FROM Consulat cs")
    Long getMaxNumeroConsulat();

}
