package com.burkina.consulat.repository;

import com.burkina.consulat.domain.CountryIdDoc;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CountryIdDoc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CountryIdDocRepository extends JpaRepository<CountryIdDoc, Long> {

}
