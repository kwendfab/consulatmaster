package com.burkina.consulat.repository;

import com.burkina.consulat.domain.Country;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Country entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
    @Query("SELECT coalesce(max(cs.code),'')  FROM Country cs")
    Long getMaxNumeroCountry();

}
