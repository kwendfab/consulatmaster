package com.burkina.consulat.repository;

import com.burkina.consulat.domain.DocumentLegal;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DocumentLegal entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DocumentLegalRepository extends JpaRepository<DocumentLegal, Long> {
    @Query("SELECT coalesce(max(cs.numero),'')  FROM DocumentLegal cs")
    Long getMaxNumeroDocument();

}
