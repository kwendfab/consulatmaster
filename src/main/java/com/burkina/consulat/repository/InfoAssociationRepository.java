package com.burkina.consulat.repository;

import com.burkina.consulat.domain.InfoAssociation;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the InfoAssociation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InfoAssociationRepository extends JpaRepository<InfoAssociation, Long> {
    List<InfoAssociation> findByMembersId(Long id);
    InfoAssociation findByAssociationIdAndMembersId(Long assocId,Long membersId);

    @Query("SELECT COUNT(*) From InfoAssociation i where i.association.nom=:nom")
    Integer countInfoAssociation(@Param(value = "nom") String nom);

}
