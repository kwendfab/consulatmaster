package com.burkina.consulat.repository;

import com.burkina.consulat.domain.InfoParents;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the InfoParents entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InfoParentsRepository extends JpaRepository<InfoParents, Long> {

}
