package com.burkina.consulat.repository;

import com.burkina.consulat.domain.InfoPoste;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the InfoPoste entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InfoPosteRepository extends JpaRepository<InfoPoste, Long> {

    List<InfoPoste> findByMembersId(Long id);
    InfoPoste findByMembersIdAndAssociationIdAndActuelleIsTrue(Long memberId,Long assocId);

}
