package com.burkina.consulat.repository;

import com.burkina.consulat.domain.InfoService;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the InfoService entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InfoServiceRepository extends JpaRepository<InfoService, Long> {

}
