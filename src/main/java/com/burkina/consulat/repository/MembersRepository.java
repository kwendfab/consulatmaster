package com.burkina.consulat.repository;

import com.burkina.consulat.domain.Consulat;
import com.burkina.consulat.domain.Members;
import com.burkina.consulat.domain.enumeration.Sexe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.time.LocalDate;
import java.util.List;


/**
 * Spring Data JPA repository for the Members entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MembersRepository extends JpaRepository<Members, Long> {
    @Query("SELECT coalesce(max(cs.numero),'')  FROM Members cs")
    Long getMaxNumeroMembers();

    @Query(value = "select membre from Members membre where (membre.consulat.id=:consulatId) and (lower(membre.nom) like concat('%',lower(:nom),'%') or :nom is null) and (lower(membre.prenom) like concat('%',lower(:prenom),'%') or :prenom is null) and (lower(membre.bfIdDoc.numero) like concat('%',lower(:numeroBf),'%') or :numeroBf is null) and (lower(membre.country.numero) like concat('%',lower(:numeroCountry),'%') or :numeroCountry  is null) and (membre.dateArrivee=:dateArrivee or :dateArrivee is null) and (membre.mariage=:mariage or :mariage is null) and (membre.sexe=:sexe or :sexe is null) and (lower(membre.metier) like concat('%',lower(:metier),'%') or :metier is null) and (lower(membre.occupation) like concat('%',lower(:occupation),'%') or :occupation is null) and (membre.parentsUsa=:parentsUsa or :parentsUsa is null) and (membre.mumInUsa=:mumInUsa or :mumInUsa is null) and (membre.country.autorite=:autorite or :autorite is null) and (membre.villeResidence=:villeResidence or :villeResidence is null) and (membre.etatResidence=:etatResidence or :etatResidence is null) ",
        countQuery = "select count(*) from Members membre where (membre.consulat.id=:consulatId )and (lower(membre.nom) like concat('%',lower(:nom),'%') or :nom is null) and (lower(membre.prenom) like concat('%',lower(:prenom),'%') or :prenom is null) and (lower(membre.bfIdDoc.numero) like concat('%',lower(:numeroBf),'%') or :numeroBf is null) and (lower(membre.country.numero) like concat('%',lower(:numeroCountry),'%') or :numeroCountry is null) and (membre.dateArrivee=:dateArrivee or :dateArrivee is null) and (membre.mariage=:mariage or :mariage is null) and (membre.sexe=:sexe or :sexe is null) and (lower(membre.metier) like concat('%',lower(:metier),'%') or :metier is null) and (lower(membre.occupation) like concat('%',lower(:occupation),'%') or :occupation is null) and (membre.parentsUsa=:parentsUsa or :parentsUsa is null) and (membre.mumInUsa=:mumInUsa or :mumInUsa is null) and (membre.country.autorite=:autorite or :autorite is null) and (membre.villeResidence=:villeResidence or :villeResidence is null) and (membre.etatResidence=:etatResidence or :etatResidence is null) ")
    Page<Members> findByMultiParamMembre(@Param("nom")String nom, @Param("prenom") String prenom, @Param("numeroBf") String numeroBf, @Param("numeroCountry")String numero, @Param("dateArrivee")LocalDate dateArrivee, @Param("mariage")Boolean mariage, @Param("sexe")Sexe sexe, @Param("metier")String metier, @Param("occupation")String occupation, @Param("parentsUsa")Boolean parentsUsa, @Param("mumInUsa")Boolean mumInUsa, @Param("autorite")String autorite, @Param("villeResidence")String villeResidence, @Param("etatResidence")String etatResidence, @Param("consulatId")Long consulatId, @Param("pageable")Pageable pageable);

    @Query("SELECT COUNT(*) From Members m where m.sexe=:sexe")
    Integer countMembreSexe(@Param(value = "sexe") Sexe sexe);

    @Query("SELECT COUNT(*) From Members m where m.dateArrivee BETWEEN :dateDebut AND :dateFin")
    Integer countByDateArrivee(@Param(value = "dateDebut") LocalDate dateDebut,@Param(value = "dateFin") LocalDate dateFin);

    @Query("SELECT COUNT(*) From Members m where m.dateNaissance BETWEEN :dateDebut AND :dateFin")
    Integer countByDateNaissance(@Param(value = "dateDebut") LocalDate dateDebut,@Param(value = "dateFin") LocalDate dateFin);


    Integer countAllBy();

}
