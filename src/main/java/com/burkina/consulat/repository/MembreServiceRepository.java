package com.burkina.consulat.repository;

import com.burkina.consulat.domain.MembreService;
import com.burkina.consulat.domain.UserExtra;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;


/**
 * Spring Data JPA repository for the MembreService entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MembreServiceRepository extends JpaRepository<MembreService, Long>{
    @Query("SELECT coalesce(max(cs.numero),'')  FROM MembreService cs")
    Long getMaxServiceNumber();

    List<MembreService> findTop10ByTypeOrderByDateDesc(String type);

    Page<MembreService> findByUserExtra(UserExtra userExtra, Pageable pageable);

    @Query(value = "select m From MembreService m where m.userExtra=:userExtra and m.date BETWEEN :dateDebut AND :dateFin order by m.date desc)",
    countQuery = "select m From MembreService m where m.userExtra=:userExtra and m.date BETWEEN :dateDebut AND :dateFin order by m.date desc")
    Page<MembreService> findBydateAndUserExtra (@Param(value = "dateDebut") ZonedDateTime dateDebut, @Param(value = "dateFin") ZonedDateTime dateFin,@Param(value = "userExtra") UserExtra userExtra,@Param(value = "pageable") Pageable pageable);


    @Query("select m From MembreService m where m.type=:type and m.date BETWEEN :dateDebut AND :dateFin order by m.date desc ")
        List<MembreService> findBydate (@Param(value = "dateDebut") ZonedDateTime dateDebut,@Param(value = "type") String type, @Param(value = "dateFin") ZonedDateTime dateFin);


}
