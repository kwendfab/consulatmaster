package com.burkina.consulat.repository;

import com.burkina.consulat.domain.Poste;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Poste entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PosteRepository extends JpaRepository<Poste, Long> {
    @Query("SELECT coalesce(max(cs.numero),'')  FROM Poste cs")
    Long getMaxNumeroPoste();

}
