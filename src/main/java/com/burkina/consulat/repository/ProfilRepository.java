package com.burkina.consulat.repository;

import com.burkina.consulat.domain.Profil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Profil entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProfilRepository extends JpaRepository<Profil, Long> {
    @Query("select distinct profil from Profil profil left join fetch profil.roles")
    List<Profil> findAllWithEagerRelationships();

    @Query("select profil from Profil profil left join fetch profil.roles where profil.id =:id")
    Profil findOneWithEagerRelationships(@Param("id") Long id);

    Boolean existsByNom(String nom);
    Integer countAllBy();
    Integer countAllByAdmin(Boolean admin);
    Page<Profil> findByAdmin(Boolean admin, Pageable pageable);
    List<Profil> findByAdmin(Boolean admin);
    Page<Profil> findById(Long id, Pageable pageable);
    Integer countById(Long id);


}
