package com.burkina.consulat.repository;

import com.burkina.consulat.domain.States;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the States entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StatesRepository extends JpaRepository<States, Long> {

States findByLibelle(String libelle);
    List<States> findByCountryId(Long id);

    @Query("SELECT coalesce(max(cs.code),'')  FROM States cs")
    Long getMaxNumeroStates();

    @Query ("SELECT l.libelle From States l")
    String[] TabOfEtat();

}
