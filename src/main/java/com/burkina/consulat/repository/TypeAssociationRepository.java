package com.burkina.consulat.repository;
import com.burkina.consulat.domain.TypeAssociation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TypeAssociation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeAssociationRepository extends JpaRepository<TypeAssociation, Long> {

}
