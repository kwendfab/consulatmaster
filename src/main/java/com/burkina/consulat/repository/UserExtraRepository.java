package com.burkina.consulat.repository;

import com.burkina.consulat.domain.UserExtra;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the UserExtra entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserExtraRepository extends JpaRepository<UserExtra, Long> {
    List<UserExtra> findByProfilId(Long profilId);
    Page<UserExtra> findByProfilId(Long profilId, Pageable pageable);
    Integer countAllByProfilId(Long id);
    Integer countAllBy();
    UserExtra findOneByUserId(Long userId);


}
