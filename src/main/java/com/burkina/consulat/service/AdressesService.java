package com.burkina.consulat.service;

import com.burkina.consulat.domain.Adresses;
import com.burkina.consulat.domain.Association;
import com.burkina.consulat.repository.AdressesRepository;
import com.burkina.consulat.service.dto.AdressesDTO;
import com.burkina.consulat.service.mapper.AdressesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Service Implementation for managing Adresses.
 */
@Service
@Transactional
public class AdressesService {

    private final Logger log = LoggerFactory.getLogger(AdressesService.class);

    private final AdressesRepository adressesRepository;

    private final AdressesMapper adressesMapper;

    public AdressesService(AdressesRepository adressesRepository, AdressesMapper adressesMapper) {
        this.adressesRepository = adressesRepository;
        this.adressesMapper = adressesMapper;
    }

    /**
     * Save a adresses.
     *
     * @param adressesDTO the entity to save
     * @return the persisted entity
     */
    public AdressesDTO save(AdressesDTO adressesDTO) {
        log.debug("Request to save Adresses : {}", adressesDTO);
        Adresses adresses = adressesMapper.toEntity(adressesDTO);
        adresses = adressesRepository.save(adresses);
        return adressesMapper.toDto(adresses);
    }

    /**
     *  Get all the adresses.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AdressesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Adresses");
        return adressesRepository.findAll(pageable)
            .map(adressesMapper::toDto);
    }

    /**
     *  Get one adresses by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AdressesDTO findOne(Long id) {
        log.debug("Request to get Adresses : {}", id);
        Adresses adresses = adressesRepository.findOne(id);
        return adressesMapper.toDto(adresses);
    }



    @Transactional(readOnly = true)
    public AdressesDTO findByMemberId(Long id) {
        Adresses adresses = adressesRepository.findByMembersIdAndActuelIsTrue(id);
        return adressesMapper.toDto(adresses);
    }

    @Transactional(readOnly = true)
    public Page<AdressesDTO> findByParams(String nom, String prenom,Pageable pageable) {
        List<AdressesDTO> list = new ArrayList<>();
        if(nom==null)
            nom="";

        if(prenom==null)
            prenom="";

        log.debug("Request to get all Members");
        Page<Adresses> assocPage = adressesRepository.findByMultiParamAdress(nom,prenom,pageable);

        return assocPage.map(adressesMapper::toDto);
    }






    /**
     *  Delete the  adresses by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Adresses : {}", id);
        adressesRepository.delete(id);
    }
}
