package com.burkina.consulat.service;

import com.burkina.consulat.domain.Association;
import com.burkina.consulat.domain.City;
import com.burkina.consulat.domain.Consulat;
import com.burkina.consulat.domain.UserExtra;
import com.burkina.consulat.repository.AssociationRepository;
import com.burkina.consulat.repository.CityRepository;
import com.burkina.consulat.service.dto.AdressesDTO;
import com.burkina.consulat.service.dto.AssociationDTO;
import com.burkina.consulat.service.mapper.AssociationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


/**
 * Service Implementation for managing Association.
 */
@Service
@Transactional
public class AssociationService {

    private final Logger log = LoggerFactory.getLogger(AssociationService.class);

    private final AssociationRepository associationRepository;

    private final AssociationMapper associationMapper;

    private final CityRepository cityRepository;

    private final AdressesService adressesService;

    private final UserExtraService userExtraService;

    public AssociationService(AssociationRepository associationRepository, AssociationMapper associationMapper, CityRepository cityRepository, AdressesService adressesService, UserExtraService userExtraService) {
        this.associationRepository = associationRepository;
        this.associationMapper = associationMapper;
        this.cityRepository = cityRepository;
        this.adressesService = adressesService;
        this.userExtraService = userExtraService;
    }

    /**
     * Save a association.
     *
     * @param associationDTO the entity to save
     * @return the persisted entity
     */
    public AssociationDTO save(AssociationDTO associationDTO) {
        log.debug("Request to save Association : {}", associationDTO);
        associationDTO.setNumero(this.getAssociation());
        AdressesDTO adressesDTO = associationDTO.getAdresseDTO();

        City cityAdresse = cityRepository.findOne(adressesDTO.getCityId());

        adressesDTO.setVille(cityAdresse.getLibelle());
        adressesDTO.setEtat(cityAdresse.getStates().getLibelle());
        adressesDTO = adressesService.save(adressesDTO);
        associationDTO.setAdressesId(adressesDTO.getId());
        Association association = associationMapper.toEntity(associationDTO);
        association = associationRepository.save(association);
        return associationMapper.toDto(association);
    }

    public String getAssociation() throws NumberFormatException {
        String code = null;
        if(associationRepository.getMaxNumeroAssociation()!=null) {
            Long maximum = associationRepository.getMaxNumeroAssociation();
            code = String.format("%09d", maximum + 1);
        }

        else
        {
            code = String.format("%09d", 1);
        }

        return code;
    }

    /**
     *  Get all the associations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AssociationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Associations");
        return associationRepository.findAll(pageable)
            .map(associationMapper::toDto);
    }



    @Transactional(readOnly = true)
    public Page<AssociationDTO> findByParams(String nom, String type,LocalDate dateCreation,String adresse,Pageable pageable) {
        List<AssociationDTO> list = new ArrayList<>();

        if(nom==null)
            nom="";

        if(type==null)
            type="";

        if(adresse==null)
            adresse="";

        log.debug("Request to get all Members");
        UserExtra userExtra = userExtraService.getUserExtraConnected();
        log.debug("*********************************" + userExtra);
            Page<Association> assocPage = associationRepository.findByMultiParamAssoc(nom,type,dateCreation,adresse,pageable);

            return assocPage.map(associationMapper::toDto);
    }











    /**
     *  Get one association by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AssociationDTO findOne(Long id) {
        log.debug("Request to get Association : {}", id);
        Association association = associationRepository.findOne(id);
        return associationMapper.toDto(association);
    }

    /**
     *  Delete the  association by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Association : {}", id);
        associationRepository.delete(id);
    }
}
