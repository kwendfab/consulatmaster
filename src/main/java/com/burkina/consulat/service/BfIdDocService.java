package com.burkina.consulat.service;

import com.burkina.consulat.domain.BfIdDoc;
import com.burkina.consulat.repository.BfIdDocRepository;
import com.burkina.consulat.service.dto.BfIdDocDTO;
import com.burkina.consulat.service.mapper.BfIdDocMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing BfIdDoc.
 */
@Service
@Transactional
public class BfIdDocService {

    private final Logger log = LoggerFactory.getLogger(BfIdDocService.class);

    private final BfIdDocRepository bfIdDocRepository;

    private final BfIdDocMapper bfIdDocMapper;

    public BfIdDocService(BfIdDocRepository bfIdDocRepository, BfIdDocMapper bfIdDocMapper) {
        this.bfIdDocRepository = bfIdDocRepository;
        this.bfIdDocMapper = bfIdDocMapper;
    }

    /**
     * Save a bfIdDoc.
     *
     * @param bfIdDocDTO the entity to save
     * @return the persisted entity
     */
    public BfIdDocDTO save(BfIdDocDTO bfIdDocDTO) {
        log.debug("Request to save BfIdDoc : {}", bfIdDocDTO);
        BfIdDoc bfIdDoc = bfIdDocMapper.toEntity(bfIdDocDTO);
        bfIdDoc = bfIdDocRepository.save(bfIdDoc);
        return bfIdDocMapper.toDto(bfIdDoc);
    }

    /**
     *  Get all the bfIdDocs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BfIdDocDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BfIdDocs");
        return bfIdDocRepository.findAll(pageable)
            .map(bfIdDocMapper::toDto);
    }

    /**
     *  Get one bfIdDoc by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BfIdDocDTO findOne(Long id) {
        log.debug("Request to get BfIdDoc : {}", id);
        BfIdDoc bfIdDoc = bfIdDocRepository.findOne(id);
        return bfIdDocMapper.toDto(bfIdDoc);
    }

    /**
     *  Delete the  bfIdDoc by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BfIdDoc : {}", id);
        bfIdDocRepository.delete(id);
    }
}
