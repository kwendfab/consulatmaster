package com.burkina.consulat.service;

import com.burkina.consulat.domain.City;
import com.burkina.consulat.repository.CityRepository;
import com.burkina.consulat.service.dto.CityDTO;
import com.burkina.consulat.service.mapper.CityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing City.
 */
@Service
@Transactional
public class CityService {

    private final Logger log = LoggerFactory.getLogger(CityService.class);

    private final CityRepository cityRepository;

    private final CityMapper cityMapper;

    public CityService(CityRepository cityRepository, CityMapper cityMapper) {
        this.cityRepository = cityRepository;
        this.cityMapper = cityMapper;
    }

    /**
     * Save a city.
     *
     * @param cityDTO the entity to save
     * @return the persisted entity
     */
    public CityDTO save(CityDTO cityDTO) {
        log.debug("Request to save City : {}", cityDTO);
       // cityDTO.setCode(this.getStatesNumero());
        City city = cityMapper.toEntity(cityDTO);
        city = cityRepository.save(city);
        return cityMapper.toDto(city);
    }
   /* public String getStatesNumero() throws NumberFormatException {
        String code = null;
        if(cityRepository.getMaxNumeroCity()!=null) {
            Long maximum = cityRepository.getMaxNumeroCity();
            code = String.format("%09d", maximum + 1);
        }

        else
        {
            code = String.format("%09d", 1);
        }

        return code;
    }*/
    /**
     *  Get all the cities.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Cities");
        return cityRepository.findAll(pageable)
            .map(cityMapper::toDto);
    }

    /**
     *  Get one city by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CityDTO findOne(Long id) {
        log.debug("Request to get City : {}", id);
        City city = cityRepository.findOne(id);
        return cityMapper.toDto(city);
    }



    /**
     *  Get one city by id.
     *
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public List<CityDTO> findStateId(Long id) {
        List<City> cities = cityRepository.findByStatesId(id);
        return cityMapper.toDto(cities);
    }

    /**
     *  Get one city by id.
     *
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public List<CityDTO> findCountryId(Long id) {
        List<City> cities = cityRepository.findByCountriesId(id);
        return cityMapper.toDto(cities);
    }



    /**
     *  Delete the  city by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete City : {}", id);
        cityRepository.delete(id);
    }
}
