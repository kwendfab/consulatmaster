package com.burkina.consulat.service;

import com.burkina.consulat.domain.City;
import com.burkina.consulat.domain.Consulat;
import com.burkina.consulat.repository.CityRepository;
import com.burkina.consulat.repository.ConsulatRepository;
import com.burkina.consulat.service.dto.AdressesDTO;
import com.burkina.consulat.service.dto.ConsulatDTO;
import com.burkina.consulat.service.mapper.ConsulatMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Consulat.
 */
@Service
@Transactional
public class ConsulatService {

    private final Logger log = LoggerFactory.getLogger(ConsulatService.class);

    private final ConsulatRepository consulatRepository;

    private final ConsulatMapper consulatMapper;

    private final CityRepository cityRepository;

    private final AdressesService adressesService;

    public ConsulatService(ConsulatRepository consulatRepository, ConsulatMapper consulatMapper, CityRepository cityRepository, AdressesService adressesService) {
        this.consulatRepository = consulatRepository;
        this.consulatMapper = consulatMapper;
        this.cityRepository = cityRepository;
        this.adressesService = adressesService;
    }

    /**
     * Save a consulat.
     *
     * @param consulatDTO the entity to save
     * @return the persisted entity
     */
    public ConsulatDTO save(ConsulatDTO consulatDTO) {
        log.debug("Request to save Consulat : {}", consulatDTO);
        consulatDTO.setCode(this.getNumeroConsulat());

        AdressesDTO adressesDTO = consulatDTO.getAdresseDTO();

        City cityAdresse = cityRepository.findOne(adressesDTO.getCityId());

        adressesDTO.setVille(cityAdresse.getLibelle());
        adressesDTO.setEtat(cityAdresse.getStates().getLibelle());
        adressesDTO = adressesService.save(adressesDTO);
        consulatDTO.setAdressesId(adressesDTO.getId());
        Consulat consulat = consulatMapper.toEntity(consulatDTO);

        consulat = consulatRepository.save(consulat);
        return consulatMapper.toDto(consulat);
    }

    public String getNumeroConsulat() throws NumberFormatException {
        String code = null;
        if(consulatRepository.getMaxNumeroConsulat()!=null) {
            Long maximum = consulatRepository.getMaxNumeroConsulat();
            code = String.format("%09d", maximum + 1);
        }

        else
        {
            code = String.format("%09d", 1);
        }

        return code;
    }


            /**
             *  Get all the consulats.
             *
             *  @param pageable the pagination information
             *  @return the list of entities
             */
    @Transactional(readOnly = true)
    public Page<ConsulatDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Consulats");
        return consulatRepository.findAll(pageable)
            .map(consulatMapper::toDto);
    }

    /**
     *  Get one consulat by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ConsulatDTO findOne(Long id) {
        log.debug("Request to get Consulat : {}", id);
        Consulat consulat = consulatRepository.findOne(id);
        return consulatMapper.toDto(consulat);
    }

    /**
     *  Delete the  consulat by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Consulat : {}", id);
        consulatRepository.delete(id);
    }
}
