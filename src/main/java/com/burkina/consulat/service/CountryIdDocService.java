package com.burkina.consulat.service;

import com.burkina.consulat.domain.CountryIdDoc;
import com.burkina.consulat.repository.CountryIdDocRepository;
import com.burkina.consulat.service.dto.CountryIdDocDTO;
import com.burkina.consulat.service.mapper.CountryIdDocMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing CountryIdDoc.
 */
@Service
@Transactional
public class CountryIdDocService {

    private final Logger log = LoggerFactory.getLogger(CountryIdDocService.class);

    private final CountryIdDocRepository countryIdDocRepository;

    private final CountryIdDocMapper countryIdDocMapper;

    public CountryIdDocService(CountryIdDocRepository countryIdDocRepository, CountryIdDocMapper countryIdDocMapper) {
        this.countryIdDocRepository = countryIdDocRepository;
        this.countryIdDocMapper = countryIdDocMapper;
    }

    /**
     * Save a countryIdDoc.
     *
     * @param countryIdDocDTO the entity to save
     * @return the persisted entity
     */
    public CountryIdDocDTO save(CountryIdDocDTO countryIdDocDTO) {
        log.debug("Request to save CountryIdDoc : {}", countryIdDocDTO);
        CountryIdDoc countryIdDoc = countryIdDocMapper.toEntity(countryIdDocDTO);
        countryIdDoc = countryIdDocRepository.save(countryIdDoc);
        return countryIdDocMapper.toDto(countryIdDoc);
    }

    /**
     *  Get all the countryIdDocs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CountryIdDocDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CountryIdDocs");
        return countryIdDocRepository.findAll(pageable)
            .map(countryIdDocMapper::toDto);
    }

    /**
     *  Get one countryIdDoc by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CountryIdDocDTO findOne(Long id) {
        log.debug("Request to get CountryIdDoc : {}", id);
        CountryIdDoc countryIdDoc = countryIdDocRepository.findOne(id);
        return countryIdDocMapper.toDto(countryIdDoc);
    }

    /**
     *  Delete the  countryIdDoc by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CountryIdDoc : {}", id);
        countryIdDocRepository.delete(id);
    }
}
