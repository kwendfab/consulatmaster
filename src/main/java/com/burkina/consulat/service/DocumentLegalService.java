package com.burkina.consulat.service;

import com.burkina.consulat.domain.DocumentLegal;
import com.burkina.consulat.repository.DocumentLegalRepository;
import com.burkina.consulat.service.dto.DocumentLegalDTO;
import com.burkina.consulat.service.mapper.DocumentLegalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing DocumentLegal.
 */
@Service
@Transactional
public class DocumentLegalService {

    private final Logger log = LoggerFactory.getLogger(DocumentLegalService.class);

    private final DocumentLegalRepository documentLegalRepository;

    private final DocumentLegalMapper documentLegalMapper;

    public DocumentLegalService(DocumentLegalRepository documentLegalRepository, DocumentLegalMapper documentLegalMapper) {
        this.documentLegalRepository = documentLegalRepository;
        this.documentLegalMapper = documentLegalMapper;
    }

    /**
     * Save a documentLegal.
     *
     * @param documentLegalDTO the entity to save
     * @return the persisted entity
     */
    public DocumentLegalDTO save(DocumentLegalDTO documentLegalDTO) {
        log.debug("Request to save DocumentLegal : {}", documentLegalDTO);
        documentLegalDTO.setNumero(this.getNumeroDocument());
        DocumentLegal documentLegal = documentLegalMapper.toEntity(documentLegalDTO);
        documentLegal = documentLegalRepository.save(documentLegal);
        return documentLegalMapper.toDto(documentLegal);
    }


    public String getNumeroDocument() throws NumberFormatException {
        String code = null;
        if(documentLegalRepository.getMaxNumeroDocument()!=null) {
            Long maximum = documentLegalRepository.getMaxNumeroDocument();
            code = String.format("%03d", maximum + 1);
        }

        else
        {
            code = String.format("%03d", 1);
        }

        return code;
    }

    /**
     *  Get all the documentLegals.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DocumentLegalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DocumentLegals");
        return documentLegalRepository.findAll(pageable)
            .map(documentLegalMapper::toDto);
    }

    /**
     *  Get one documentLegal by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DocumentLegalDTO findOne(Long id) {
        log.debug("Request to get DocumentLegal : {}", id);
        DocumentLegal documentLegal = documentLegalRepository.findOne(id);
        return documentLegalMapper.toDto(documentLegal);
    }

    /**
     *  Delete the  documentLegal by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DocumentLegal : {}", id);
        documentLegalRepository.delete(id);
    }
}
