package com.burkina.consulat.service;

import com.burkina.consulat.domain.Association;
import com.burkina.consulat.domain.InfoAssociation;
import com.burkina.consulat.domain.InfoPoste;
import com.burkina.consulat.repository.InfoAssociationRepository;
import com.burkina.consulat.repository.InfoPosteRepository;
import com.burkina.consulat.service.dto.AssociationDTO;
import com.burkina.consulat.service.dto.InfoAssociationDTO;
import com.burkina.consulat.service.mapper.AssociationMapper;
import com.burkina.consulat.service.mapper.InfoAssociationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Service Implementation for managing InfoAssociation.
 */
@Service
@Transactional
public class InfoAssociationService {

    private final Logger log = LoggerFactory.getLogger(InfoAssociationService.class);

    private final InfoAssociationRepository infoAssociationRepository;

    private final InfoAssociationMapper infoAssociationMapper;

    private final AssociationMapper associationMapper;

    private final InfoPosteRepository infoPosteRepository;

    public InfoAssociationService(InfoAssociationRepository infoAssociationRepository, InfoAssociationMapper infoAssociationMapper, AssociationMapper associationMapper, InfoPosteRepository infoPosteRepository) {
        this.infoAssociationRepository = infoAssociationRepository;
        this.infoAssociationMapper = infoAssociationMapper;
        this.associationMapper = associationMapper;
        this.infoPosteRepository = infoPosteRepository;
    }

    /**
     * Save a infoAssociation.
     *
     * @param infoAssociationDTO the entity to save
     * @return the persisted entity
     */
    public InfoAssociationDTO save(InfoAssociationDTO infoAssociationDTO) {
        log.debug("Request to save InfoAssociation : {}", infoAssociationDTO);
        InfoAssociation infoAssociation = infoAssociationMapper.toEntity(infoAssociationDTO);
        infoAssociation = infoAssociationRepository.save(infoAssociation);
        return infoAssociationMapper.toDto(infoAssociation);
    }

    /**
     *  Get all the infoAssociations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InfoAssociationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InfoAssociations");
        return infoAssociationRepository.findAll(pageable)
            .map(infoAssociationMapper::toDto);
    }

    /**
     *  Get one infoAssociation by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public InfoAssociationDTO findOne(Long id) {
        log.debug("Request to get InfoAssociation : {}", id);
        InfoAssociation infoAssociation = infoAssociationRepository.findOne(id);
        return infoAssociationMapper.toDto(infoAssociation);
    }



    @Transactional(readOnly = true)
    public List<AssociationDTO> findByMembers(Long id) {

        List<InfoAssociation> infoAssociation = infoAssociationRepository.findByMembersId(id);
        List<Association> associationList = new ArrayList<>();
        for(InfoAssociation infoAssociation1:infoAssociation)
        {
          associationList.add(infoAssociation1.getAssociation());
        }
        List<AssociationDTO> associationDTOS = associationMapper.toDto(associationList);
        for(AssociationDTO associationDTO:associationDTOS)
        {
            InfoPoste infoPoste = infoPosteRepository.findByMembersIdAndAssociationIdAndActuelleIsTrue(id,associationDTO.getId());
            associationDTO.setPosteId(infoPoste.getPoste().getId());
            associationDTO.setDateFonction(infoPoste.getDateDebut());
        }
        return associationDTOS;
    }








    /**
     *  Delete the  infoAssociation by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete InfoAssociation : {}", id);
        infoAssociationRepository.delete(id);
    }
}
