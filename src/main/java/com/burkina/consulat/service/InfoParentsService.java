package com.burkina.consulat.service;

import com.burkina.consulat.domain.InfoParents;
import com.burkina.consulat.repository.InfoParentsRepository;
import com.burkina.consulat.service.dto.InfoParentsDTO;
import com.burkina.consulat.service.mapper.InfoParentsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing InfoParents.
 */
@Service
@Transactional
public class InfoParentsService {

    private final Logger log = LoggerFactory.getLogger(InfoParentsService.class);

    private final InfoParentsRepository infoParentsRepository;

    private final InfoParentsMapper infoParentsMapper;

    public InfoParentsService(InfoParentsRepository infoParentsRepository, InfoParentsMapper infoParentsMapper) {
        this.infoParentsRepository = infoParentsRepository;
        this.infoParentsMapper = infoParentsMapper;
    }

    /**
     * Save a infoParents.
     *
     * @param infoParentsDTO the entity to save
     * @return the persisted entity
     */
    public InfoParentsDTO save(InfoParentsDTO infoParentsDTO) {
        log.debug("Request to save InfoParents : {}", infoParentsDTO);
        InfoParents infoParents = infoParentsMapper.toEntity(infoParentsDTO);
        infoParents = infoParentsRepository.save(infoParents);
        return infoParentsMapper.toDto(infoParents);
    }

    /**
     *  Get all the infoParents.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InfoParentsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InfoParents");
        return infoParentsRepository.findAll(pageable)
            .map(infoParentsMapper::toDto);
    }

    /**
     *  Get one infoParents by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public InfoParentsDTO findOne(Long id) {
        log.debug("Request to get InfoParents : {}", id);
        InfoParents infoParents = infoParentsRepository.findOne(id);
        return infoParentsMapper.toDto(infoParents);
    }

    /**
     *  Delete the  infoParents by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete InfoParents : {}", id);
        infoParentsRepository.delete(id);
    }
}
