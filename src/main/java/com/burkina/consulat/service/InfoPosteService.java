package com.burkina.consulat.service;

import com.burkina.consulat.domain.InfoPoste;
import com.burkina.consulat.repository.InfoPosteRepository;
import com.burkina.consulat.service.dto.InfoPosteDTO;
import com.burkina.consulat.service.mapper.InfoPosteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing InfoPoste.
 */
@Service
@Transactional
public class InfoPosteService {

    private final Logger log = LoggerFactory.getLogger(InfoPosteService.class);

    private final InfoPosteRepository infoPosteRepository;

    private final InfoPosteMapper infoPosteMapper;

    public InfoPosteService(InfoPosteRepository infoPosteRepository, InfoPosteMapper infoPosteMapper) {
        this.infoPosteRepository = infoPosteRepository;
        this.infoPosteMapper = infoPosteMapper;
    }

    /**
     * Save a infoPoste.
     *
     * @param infoPosteDTO the entity to save
     * @return the persisted entity
     */
    public InfoPosteDTO save(InfoPosteDTO infoPosteDTO) {
        log.debug("Request to save InfoPoste : {}", infoPosteDTO);
        InfoPoste infoPoste = infoPosteMapper.toEntity(infoPosteDTO);
        infoPoste = infoPosteRepository.save(infoPoste);
        return infoPosteMapper.toDto(infoPoste);
    }

    /**
     *  Get all the infoPostes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InfoPosteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InfoPostes");
        return infoPosteRepository.findAll(pageable)
            .map(infoPosteMapper::toDto);
    }

    /**
     *  Get one infoPoste by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public InfoPosteDTO findOne(Long id) {
        log.debug("Request to get InfoPoste : {}", id);
        InfoPoste infoPoste = infoPosteRepository.findOne(id);
        return infoPosteMapper.toDto(infoPoste);
    }



    @Transactional(readOnly = true)
    public List<InfoPosteDTO> findByMemberId (Long id) {
        log.debug("Request to get InfoPoste : {}", id);
        List<InfoPoste> infoPoste = infoPosteRepository.findByMembersId(id);
        return infoPosteMapper.toDto(infoPoste);
    }






    /**
     *  Delete the  infoPoste by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete InfoPoste : {}", id);
        infoPosteRepository.delete(id);
    }
}
