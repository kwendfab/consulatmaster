package com.burkina.consulat.service;

import com.burkina.consulat.domain.InfoService;
import com.burkina.consulat.repository.InfoServiceRepository;
import com.burkina.consulat.service.dto.InfoServiceDTO;
import com.burkina.consulat.service.mapper.InfoServiceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing InfoService.
 */
@Service
@Transactional
public class InfoServiceService {

    private final Logger log = LoggerFactory.getLogger(InfoServiceService.class);

    private final InfoServiceRepository infoServiceRepository;

    private final InfoServiceMapper infoServiceMapper;

    public InfoServiceService(InfoServiceRepository infoServiceRepository, InfoServiceMapper infoServiceMapper) {
        this.infoServiceRepository = infoServiceRepository;
        this.infoServiceMapper = infoServiceMapper;
    }

    /**
     * Save a infoService.
     *
     * @param infoServiceDTO the entity to save
     * @return the persisted entity
     */
    public InfoServiceDTO save(InfoServiceDTO infoServiceDTO) {
        log.debug("Request to save InfoService : {}", infoServiceDTO);
        InfoService infoService = infoServiceMapper.toEntity(infoServiceDTO);
        infoService = infoServiceRepository.save(infoService);
        return infoServiceMapper.toDto(infoService);
    }

    /**
     *  Get all the infoServices.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InfoServiceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InfoServices");
        return infoServiceRepository.findAll(pageable)
            .map(infoServiceMapper::toDto);
    }

    /**
     *  Get one infoService by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public InfoServiceDTO findOne(Long id) {
        log.debug("Request to get InfoService : {}", id);
        InfoService infoService = infoServiceRepository.findOne(id);
        return infoServiceMapper.toDto(infoService);
    }

    /**
     *  Delete the  infoService by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete InfoService : {}", id);
        infoServiceRepository.delete(id);
    }
}
