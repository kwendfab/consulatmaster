package com.burkina.consulat.service;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.domain.enumeration.Sexe;
import com.burkina.consulat.repository.*;


import com.burkina.consulat.web.rest.errors.CustomParameterizedException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BarcodePDF417;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


import java.awt.Color;


import com.burkina.consulat.service.dto.*;
import com.burkina.consulat.service.mapper.AdressesMapper;
import com.burkina.consulat.service.mapper.MembersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

import javax.imageio.ImageIO;
import javax.script.ScriptException;
import java.awt.*;
import java.io.*;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;
import java.util.List;


/**
 * Service Implementation for managing Members.
 */
@Service
@Transactional
public class MembersService {

    private final Logger log = LoggerFactory.getLogger(MembersService.class);

    private final MembersRepository membersRepository;

    private final MembersMapper membersMapper;

    private final CountryIdDocService countryIdDocService;

    private final BfIdDocService bfIdDocService;

    private final InfoParentsService infoParentsService;

    private final AdressesService adressesService;

    private final InfoAssociationService infoAssociationService;

    private final InfoAssociationRepository infoAssociationRepository;

    private final AdressesMapper adressesMapper;

    private final CityRepository cityRepository;

    private final ConsulatRepository consulatRepository;

    private final StatesRepository statesRepository;

    private final UserExtraService userExtraService;

    private final InfoPosteService infoPosteService;

    private final InfoPosteRepository infoPosteRepository;

    public MembersService(MembersRepository membersRepository, MembersMapper membersMapper, CountryIdDocService countryIdDocService, BfIdDocService bfIdDocService, InfoParentsService infoParentsService, AdressesService adressesService, InfoAssociationService infoAssociationService, InfoAssociationRepository infoAssociationRepository, AdressesMapper adressesMapper, CityRepository cityRepository, ConsulatRepository consulatRepository, StatesRepository statesRepository, UserExtraService userExtraService, InfoPosteService infoPosteService, InfoPosteRepository infoPosteRepository) {
        this.membersRepository = membersRepository;
        this.membersMapper = membersMapper;
        this.countryIdDocService = countryIdDocService;
        this.bfIdDocService = bfIdDocService;
        this.infoParentsService = infoParentsService;
        this.adressesService = adressesService;
        this.infoAssociationService = infoAssociationService;
        this.infoAssociationRepository = infoAssociationRepository;
        this.adressesMapper = adressesMapper;
        this.cityRepository = cityRepository;
        this.consulatRepository = consulatRepository;
        this.statesRepository = statesRepository;
        this.userExtraService = userExtraService;
        this.infoPosteService = infoPosteService;
        this.infoPosteRepository = infoPosteRepository;
    }

    /**
     * Save a members.
     *
     * @param membersDTO the entity to save
     * @return the persisted entity
     */
    @Transactional
    public MembersDTO save(MembersDTO membersDTO) {
        log.debug("Request to save Members : {}", membersDTO);
        CountryIdDocDTO countryIdDocDTO = membersDTO.getCountry();
        BfIdDocDTO bfIdDocDTO = membersDTO.getBfIdDoc();
        InfoParentsDTO infoParentsDTO = membersDTO.getInfoParents();
        AdressesDTO adressesDTO = membersDTO.getAdresseDTO();

        if(membersDTO.getChangerAdresse()==null)
            membersDTO.setChangerAdresse(Boolean.FALSE);
;



        if( adressesDTO.getId()!=null && membersDTO.getChangerAdresse()==Boolean.TRUE )
        {
            AdressesDTO ancienneAdresse = adressesService.findOne(adressesDTO.getId());
            ancienneAdresse.setActuel(Boolean.FALSE);
            ancienneAdresse = adressesService.save(ancienneAdresse);
            adressesDTO.setId(null);
        }

        adressesDTO.setActuel(Boolean.TRUE);

        UserExtra userExtra = userExtraService.getUserExtraConnected();
        if(userExtra!=null) {
        membersDTO.setConsulatId(userExtra.getConsulat().getId());


            City citybf = cityRepository.findOne(bfIdDocDTO.getCityId());
            City cityCountry = cityRepository.findOne(countryIdDocDTO.getCityId());

            City cityAdresse = cityRepository.findOne(adressesDTO.getCityId());

            adressesDTO.setVille(cityAdresse.getLibelle());
            adressesDTO.setEtat(cityAdresse.getStates().getLibelle());


            bfIdDocDTO.setVille(citybf.getLibelle());
            countryIdDocDTO.setVille(cityCountry.getLibelle());
            countryIdDocDTO.setEtat(cityCountry.getStates().getLibelle());


            countryIdDocDTO = countryIdDocService.save(countryIdDocDTO);
            bfIdDocDTO = bfIdDocService.save(bfIdDocDTO);
            infoParentsDTO = infoParentsService.save(infoParentsDTO);
            adressesDTO = adressesService.save(adressesDTO);

            if (membersDTO.getMariage() == null)
                membersDTO.setMariage(Boolean.FALSE);

            if (membersDTO.getMemePaysConjoint() == null)
                membersDTO.setMemePaysConjoint(Boolean.FALSE);

            if (membersDTO.getMumInUsa() == null)
                membersDTO.setMumInUsa(Boolean.FALSE);

            if (membersDTO.getParentsUsa() == null)
                membersDTO.setParentsUsa(Boolean.FALSE);


            Set<InfoAssociationDTO> infoAssociations = new HashSet<>();

            membersDTO.setVilleResidence(countryIdDocDTO.getVille());
            membersDTO.setEtatResidence(countryIdDocDTO.getEtat());
            membersDTO.setTelephone(adressesDTO.getTelephone());

            membersDTO.setNumero(this.getNumeroMembers());

            membersDTO.setDateCreate(LocalDate.now());
            membersDTO.setDateExpr(LocalDate.now().plusYears(10l));


            if (countryIdDocDTO.getId() != null)
                membersDTO.setCountryId(countryIdDocDTO.getId());

            if (bfIdDocDTO.getId() != null)
                membersDTO.setBfIdDocId(bfIdDocDTO.getId());

            if (infoParentsDTO.getId() != null)
                membersDTO.setInfoParentsId(infoParentsDTO.getId());

            if (membersDTO.getNoConjoint() != null)
                membersDTO.setNomConjoint(membersDTO.getNomConjoint());

            if (membersDTO.getPrenomConjoint() != null)
                membersDTO.setPrenomConjoint(membersDTO.getPrenomConjoint());

            if (membersDTO.getParentsUsa() != null)
                membersDTO.setParentsUsa(membersDTO.getParentsUsa());

            if (membersDTO.getMumInUsa() != null)
                membersDTO.setMumInUsa(membersDTO.getMumInUsa());

            Members members = membersMapper.toEntity(membersDTO);

            members = membersRepository.save(members);

            if (membersDTO.getId() != null) {
                List<InfoAssociation> infoAssociationList = infoAssociationRepository.findByMembersId(membersDTO.getId());
                {
                    for (InfoAssociation infoAssociation : infoAssociationList) {
                        infoAssociationRepository.delete(infoAssociation);
                    }
                }

                List<InfoPoste> infoPostes = infoPosteRepository.findByMembersId(membersDTO.getId());
                {
                    for (InfoPoste infoPoste : infoPostes) {
                        infoPoste.setActuelle(Boolean.FALSE);
                        infoPosteRepository.save(infoPoste);
                    }
                }


                for (AssociationDTO associationDTO : membersDTO.getAssociations()) {
                    InfoAssociationDTO infoAssociationDTO = new InfoAssociationDTO();
                    infoAssociationDTO.setDateEntree(LocalDate.now());
                    infoAssociationDTO.setAssociationId(associationDTO.getId());
                    infoAssociationDTO.setMembersId(members.getId());
                    infoAssociationService.save(infoAssociationDTO);

                    /*enregistrement du poste occupée dans l'association*/
                    if(associationDTO.getPosteId()!=null) {
                        InfoPosteDTO infoPosteDTO = new InfoPosteDTO();
                        infoPosteDTO.setDateDebut(LocalDate.now());
                        infoPosteDTO.setMembersId(members.getId());
                        infoPosteDTO.setAssociationId(associationDTO.getId());
                        infoPosteDTO.setActuelle(Boolean.TRUE);
                        infoPosteDTO.setPosteId(associationDTO.getPosteId());
                        infoPosteService.save(infoPosteDTO);
                    }

                }
            } else {

                for (AssociationDTO associationDTO : membersDTO.getAssociations()) {
                    InfoAssociationDTO infoAssociationDTO = new InfoAssociationDTO();
                    infoAssociationDTO.setDateEntree(associationDTO.getDateFonction());
                    infoAssociationDTO.setAssociationId(associationDTO.getId());
                    infoAssociationDTO.setMembersId(members.getId());
                    infoAssociationService.save(infoAssociationDTO);

                    /*enregistrement du poste occupée dans l'association*/
                    if(associationDTO.getPosteId()!=null) {
                        InfoPosteDTO infoPosteDTO = new InfoPosteDTO();
                        infoPosteDTO.setDateDebut(associationDTO.getDateFonction());
                        infoPosteDTO.setMembersId(members.getId());
                        infoPosteDTO.setAssociationId(associationDTO.getId());
                        infoPosteDTO.setActuelle(Boolean.TRUE);
                        infoPosteDTO.setPosteId(associationDTO.getPosteId());
                        infoPosteService.save(infoPosteDTO);
                    }
                }
            }



        Adresses adresses = adressesMapper.toEntity(adressesDTO);

        adresses.setMembers(members);

        adressesService.save(adressesMapper.toDto(adresses));


        return membersMapper.toDto(members);
        }
        else
        {
            throw new CustomParameterizedException("L'administrateur ne peut enregistrer de membre ");
        }
    }




    public String getNumeroMembers() throws NumberFormatException {
        String code = null;
        if(membersRepository.getMaxNumeroMembers()!=null) {
            Long maximum = membersRepository.getMaxNumeroMembers();
            code = String.format("%09d", maximum + 1);
        }

        else
        {
            code = String.format("%09d", 1);
        }

        return code;
    }


    private Image generateBarCode(String code) throws IOException, DocumentException {

        BarcodePDF417 barcode = new BarcodePDF417();
        barcode.setText(code);
        java.awt.Image img = barcode.createAwtImage(Color.BLACK, Color.WHITE);
        return  img;

    }





    @Transactional(readOnly = true)
    public MembersDTO etatMembre(Long idMembre) throws IOException, DocumentException {
        log.debug("Request to get Membre  card: {}", idMembre);

        MembersDTO membreDTO = new MembersDTO();
        Members membre = membersRepository.findOne(idMembre);
        Consulat consulat = membre.getConsulat();
        membreDTO = membersMapper.toDto(membre);
        membreDTO.setSexes(membreDTO.getSexe()+"");

        membreDTO.setEnTete(consulat.getEnTete());

        membreDTO.setTitre(consulat.getTitre());

        membreDTO.setCodeCarte(consulat.getCodeCarte());

        membreDTO.setTexteVerso(consulat.getTexteVerso());

        if(consulat.getImageVerso()!=null) {
            Image verso = null;
            try {
                verso = ImageIO.read(new ByteArrayInputStream(consulat.getImageVerso()));

            } catch (IOException e) {
                e.printStackTrace();
            }
            membreDTO.setImageVerso(verso);
        }


        AdressesDTO adressesDTO = adressesService.findByMemberId(membreDTO.getId());

        membreDTO.setAdresse(adressesDTO.getRue()+","+adressesDTO.getVille()+","+adressesDTO.getEtat());
        membreDTO.setTelephoneConsulat(consulat.getAdresses().getTelephone());
        membreDTO.setAdresseConsulat(consulat.getAdresses().getRue());

        if(consulat.getLogoBf()!=null) {
            Image imageDroit = null;
            Image barCode = null;

             barCode = this.generateBarCode(membreDTO.getNumero());


            try {
                imageDroit = ImageIO.read(new ByteArrayInputStream(consulat.getLogoBf()));


            } catch (IOException e) {
                e.printStackTrace();
            }
            membreDTO.setLogoBf(imageDroit);

            membreDTO.setBarCode(barCode);

        }

        if(consulat.getSignature()!=null) {

            Image signatures = null;


            try {

                signatures = ImageIO.read(new ByteArrayInputStream(consulat.getSignature()));

            } catch (IOException e) {
                e.printStackTrace();
            }

            membreDTO.setSignatureAutorite(signatures);
        }


        if(membreDTO.getSignature()!=null) {

            Image signature = null;

            try {

                signature = ImageIO.read(new ByteArrayInputStream(membreDTO.getSignature()));

            } catch (IOException e) {
                e.printStackTrace();
            }

            membreDTO.setSignatureBearer(signature);
        }


        if(consulat.getArmoirie()!=null) {

            Image armoirie = null;

            try {

                armoirie = ImageIO.read(new ByteArrayInputStream(consulat.getArmoirie()));

            } catch (IOException e) {
                e.printStackTrace();
            }

            membreDTO.setArmoirie(armoirie);
        }

        if(membre.getPhoto()!=null) {
            Image imagePhoto = null;
            try {
                imagePhoto = ImageIO.read(new ByteArrayInputStream(membre.getPhoto()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            membreDTO.setPhotos(imagePhoto);
        }



        return membreDTO;
    }

    public ModelAndView genererPdfMembre(ApplicationContext appContext, Long idMembre) throws ScriptException, URISyntaxException, IOException, DocumentException {
        JasperReportsPdfView view = new JasperReportsPdfView();
        Map<String,Object> params = new HashMap<>();
        view.setUrl("classpath:/report/report_Landscape.jrxml");
        view.setApplicationContext(appContext);
        MembersDTO valeur = this.etatMembre(idMembre);
        JRDataSource dataSource = new JRBeanCollectionDataSource(Collections.singleton(valeur));
        params.put("datasource", dataSource);
        return new ModelAndView(view,params);
    }






    /**
     *  Get all the members.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MembersDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Members");
        List<MembersDTO> list = new ArrayList<>();
        Page<Members> membersPage = membersRepository.findAll(pageable);
        for(Members members:membersPage)
        {
            MembersDTO membersDTO = membersMapper.toDto(members);
            if(membersDTO.getAdresseDTO()!=null)
            {
                AdressesDTO adressesDTO = membersDTO.getAdresseDTO();
                States states =  cityRepository.findOne(adressesDTO.getCityId()).getStates();
                adressesDTO.setEtatId(states.getId());
                membersDTO.setAdresseDTO(adressesDTO);
            }
            list.add(membersDTO);
        }
         return new PageImpl(list, pageable, membersPage.getTotalElements());
    }


    /**
     *  Get all the members by multiple params.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MembersDTO> findByParams(String nom, String prenom, String numeroBf, String numeroCounty, LocalDate dateArrivee, Boolean mariage, Sexe  sexe, String metier, String occupation, Boolean parentsUsa, Boolean mumInUsa, String autorite, String villeResidence, String etatResidence, Pageable pageable) {
        List<MembersDTO> list = new ArrayList<>();
        log.debug("Request to get all Members");

        if(nom==null)
            nom="";
        if(prenom==null)
            prenom="";
        if(metier==null)
            metier="";
        if(occupation==null)
            occupation="";
        if(numeroBf==null)
            numeroBf="";
        if(numeroCounty==null)
            numeroCounty="";


        UserExtra userExtra = userExtraService.getUserExtraConnected();
        log.debug("*********************************" + userExtra);
       if(userExtra.getUser()!=null) {
           if (userExtra != null) {
               Consulat consulat = userExtra.getConsulat();
               Page<Members> membersPage = membersRepository.findByMultiParamMembre(nom, prenom, numeroBf, numeroCounty, dateArrivee, mariage, sexe, metier, occupation, parentsUsa, mumInUsa, autorite, villeResidence, etatResidence, consulat.getId(), pageable);
               for (Members members : membersPage) {

                   MembersDTO membersDTO = membersMapper.toDto(members);
                   if (membersDTO.getEtatArrivee() != null)
                       membersDTO.setStatesId(statesRepository.findByLibelle(membersDTO.getEtatArrivee()).getId());
                   AdressesDTO adressesDTO = adressesService.findByMemberId(membersDTO.getId());


                   log.debug("*********************************" + adressesDTO);


                   if (adressesDTO.getCityId() != null) {
                       States states = cityRepository.findOne(adressesDTO.getCityId()).getStates();
                       adressesDTO.setEtatId(states.getId());
                       membersDTO.setAdresseDTO(adressesDTO);
                   }
                   list.add(membersDTO);
               }
               return new PageImpl(list, pageable, membersPage.getTotalElements());
           } else {
               return this.findAll(pageable);

           }
       }
       else
       {
           throw new CustomParameterizedException("L'administrateur ne peut afficher les membres ");
       }
    }






    /**
     *  Get one members by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MembersDTO findOne(Long id) {
        log.debug("Request to get Members : {}", id);
        Members members = membersRepository.findOne(id);
        return membersMapper.toDto(members);
    }

    /**
     *  Delete the  members by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Members : {}", id);
        membersRepository.delete(id);
    }
}
