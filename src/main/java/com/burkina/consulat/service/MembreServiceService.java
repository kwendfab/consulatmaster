package com.burkina.consulat.service;

import com.burkina.consulat.domain.Consulat;
import com.burkina.consulat.domain.MembreService;
import com.burkina.consulat.domain.UserExtra;
import com.burkina.consulat.repository.ConsulatRepository;
import com.burkina.consulat.repository.MembreServiceRepository;
import com.burkina.consulat.service.dto.MembreServiceDTO;
import com.burkina.consulat.service.mapper.MembreServiceMapper;
import com.burkina.consulat.web.rest.errors.CustomParameterizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;


/**
 * Service Implementation for managing MembreService.
 */
@Service
@Transactional
public class MembreServiceService {

    private final Logger log = LoggerFactory.getLogger(MembreServiceService.class);

    private final MembreServiceRepository membreServiceRepository;

    private final MembreServiceMapper membreServiceMapper;

    private final UserExtraService userExtraService;

    private final ConsulatRepository consulatRepository;

    public MembreServiceService(MembreServiceRepository membreServiceRepository, MembreServiceMapper membreServiceMapper, UserExtraService userExtraService, ConsulatRepository consulatRepository) {
        this.membreServiceRepository = membreServiceRepository;
        this.membreServiceMapper = membreServiceMapper;
        this.userExtraService = userExtraService;
        this.consulatRepository = consulatRepository;
    }

    /**
     * Save a membreService.
     *
     * @param membreServiceDTO the entity to save
     * @return the persisted entity
     */
    public MembreServiceDTO save(MembreServiceDTO membreServiceDTO) {
        log.debug("Request to save MembreService : {}", membreServiceDTO);
        UserExtra userExtra = userExtraService.getUserExtraConnected();
        Consulat consulat = null;
        if(userExtra!=null) {
            if(userExtra.getConsulat()!=null) {
                consulat = consulatRepository.findOne(userExtra.getConsulat().getId());
            }


        membreServiceDTO.setUserExtraId(userExtra.getId());
            if(membreServiceDTO.getConsulat()==null) {
                membreServiceDTO.setConsulat(consulat.getNom());
            }
        membreServiceDTO.setNumero(this.getServiceNumber());
        membreServiceDTO.setDate(ZonedDateTime.now());
        MembreService membreService = membreServiceMapper.toEntity(membreServiceDTO);
        membreService = membreServiceRepository.save(membreService);
        return membreServiceMapper.toDto(membreService);
        }
        else
        {
            throw new CustomParameterizedException("L'administrateur ne peut enregistrer un service");

        }
    }

    public String getServiceNumber() throws NumberFormatException {
        String code = null;
        if(membreServiceRepository.getMaxServiceNumber()!=null) {
            Long maximum = membreServiceRepository.getMaxServiceNumber();
            code = String.format("%09d", maximum + 1);
        }

        else
        {
            code = String.format("%09d", 1);
        }

        return code;
    }

    /**
     *  Get all the membreServices.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MembreServiceDTO> findAll(ZonedDateTime debut,ZonedDateTime fin,Pageable pageable) {
        UserExtra userExtra = userExtraService.getUserExtraConnected();
        if(userExtra.getUser()!=null) {
            Page<MembreService> membreServices = null;
            if (debut == null && fin == null) {
                membreServices = membreServiceRepository.findByUserExtra(userExtra, pageable);
            } else {
                membreServices = membreServiceRepository.findBydateAndUserExtra(debut, fin, userExtra, pageable);
            }
            log.debug("Request to get all MembreServices");
            return membreServices
                .map(membreServiceMapper::toDto);
        }
        else{
            Page<MembreService> membreServices = membreServiceRepository.findAll(pageable);
            return membreServices
                .map(membreServiceMapper::toDto);        }
    }



    @Transactional(readOnly = true)
    public List<MembreServiceDTO> findTop10() {

        ZonedDateTime fin = ZonedDateTime.now();
        ZonedDateTime debut = fin.minusMonths(1);
        List<MembreService> listInformation = membreServiceRepository.findBydate(debut,"INFORMATION",fin);
        return membreServiceMapper.toDto(listInformation);
    }

     @Transactional(readOnly = true)
    public List<MembreServiceDTO> findTop10Even() {
         ZonedDateTime fin = ZonedDateTime.now();
         ZonedDateTime debut = fin.minusMonths(1);
        List<MembreService> listEvenement = membreServiceRepository.findBydate(debut,"EVENEMENT",fin);
        return membreServiceMapper.toDto(listEvenement);
    }








    /**
     *  Get one membreService by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MembreServiceDTO findOne(Long id) {
        log.debug("Request to get MembreService : {}", id);
        MembreService membreService = membreServiceRepository.findOne(id);
        return membreServiceMapper.toDto(membreService);
    }

    /**
     *  Delete the  membreService by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MembreService : {}", id);
        membreServiceRepository.delete(id);
    }
}
