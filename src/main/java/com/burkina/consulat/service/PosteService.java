package com.burkina.consulat.service;

import com.burkina.consulat.domain.Poste;
import com.burkina.consulat.repository.PosteRepository;
import com.burkina.consulat.service.dto.PosteDTO;
import com.burkina.consulat.service.mapper.PosteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Poste.
 */
@Service
@Transactional
public class PosteService {

    private final Logger log = LoggerFactory.getLogger(PosteService.class);

    private final PosteRepository posteRepository;

    private final PosteMapper posteMapper;

    public PosteService(PosteRepository posteRepository, PosteMapper posteMapper) {
        this.posteRepository = posteRepository;
        this.posteMapper = posteMapper;
    }

    /**
     * Save a poste.
     *
     * @param posteDTO the entity to save
     * @return the persisted entity
     */
    public PosteDTO save(PosteDTO posteDTO) {
        log.debug("Request to save Poste : {}", posteDTO);
        posteDTO.setNumero(this.getNumeroPoste());
        Poste poste = posteMapper.toEntity(posteDTO);
        poste = posteRepository.save(poste);
        return posteMapper.toDto(poste);
    }


    public String getNumeroPoste() throws NumberFormatException {
        String code = null;
        if(posteRepository.getMaxNumeroPoste()!=null) {
            Long maximum = posteRepository.getMaxNumeroPoste();
            code = String.format("%03d", maximum + 1);
        }

        else
        {
            code = String.format("%03d", 1);
        }

        return code;
    }





    /**
     *  Get all the postes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PosteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Postes");
        return posteRepository.findAll(pageable)
            .map(posteMapper::toDto);
    }

    /**
     *  Get one poste by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PosteDTO findOne(Long id) {
        log.debug("Request to get Poste : {}", id);
        Poste poste = posteRepository.findOne(id);
        return posteMapper.toDto(poste);
    }

    /**
     *  Delete the  poste by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Poste : {}", id);
        posteRepository.delete(id);
    }
}
