package com.burkina.consulat.service;

import com.burkina.consulat.domain.Authority;
import com.burkina.consulat.domain.Profil;
import com.burkina.consulat.domain.User;
import com.burkina.consulat.domain.UserExtra;
import com.burkina.consulat.repository.AuthorityRepository;
import com.burkina.consulat.repository.ProfilRepository;
import com.burkina.consulat.repository.UserExtraRepository;
import com.burkina.consulat.repository.UserRepository;
import com.burkina.consulat.security.AuthoritiesConstants;
import com.burkina.consulat.service.dto.ProfilDTO;
import com.burkina.consulat.service.mapper.ProfilMapper;
import com.burkina.consulat.web.rest.errors.CustomParameterizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing Profil.
 */
@Service
@Transactional
public class ProfilService {

    private final Logger log = LoggerFactory.getLogger(ProfilService.class);

    private final ProfilRepository profilRepository;

    private final ProfilMapper profilMapper;

    private final AuthorityRepository authorityRepository;

    private final UserExtraRepository userExtraRepository;

    private final UserRepository userRepository;

    private final UserService userService;

    private Integer total =0;

    public ProfilService(ProfilRepository profilRepository, ProfilMapper profilMapper, AuthorityRepository authorityRepository, UserExtraRepository userExtraRepository, UserRepository userRepository, UserService userService) {
        this.profilRepository = profilRepository;
        this.profilMapper = profilMapper;
        this.authorityRepository = authorityRepository;
        this.userExtraRepository = userExtraRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    /**
     * Save a profil.
     *
     * @param profilDTO the entity to save
     * @return the persisted entity
     */
    public ProfilDTO save(ProfilDTO profilDTO) { log.debug("Request to save Profil : {}", profilDTO);
        Profil profil = profilMapper.toEntity(profilDTO);
        if(profilDTO.getId() == null && profilRepository.existsByNom(profilDTO.getNom())) {
            throw new CustomParameterizedException("Le meme nom de profil existe déjà");
        }
        if (profilDTO.getAuthorities() != null) {
            Set<String> auths = profilDTO.getAuthorities();
            //Ajout du role USER par default
            auths.add(AuthoritiesConstants.USER);
            Set<Authority> authorities = profilDTO.getAuthorities().stream()
                .map(authorityRepository::findOne)
                .collect(Collectors.toSet());
            profil.setRoles(authorities);

         /*
            Mise à jour des roles des utilisateurs ayant ce profil
            Pour verifier  que les roles de l'utilisateur ont changes
            on utilise l'attribut profilChange
         */
            if (profilDTO.getProfilsChange() != null) {
                if (profilDTO.getProfilsChange()) {
                    log.debug("Etat du profil : " + profilDTO.getProfilsChange());
                    List<UserExtra> userExtras = userExtraRepository.findByProfilId(profilDTO.getId());
                    userExtras.forEach(userExtra -> {
                        User user = userRepository.findOne(userExtra.getUser().getId());
                        user.setAuthorities(authorities);
                        userRepository.save(user);
                    });
                    // throw new CustomParameterizedException("Mise à jour du profil"+userExtras.size()+ " "+authorities.size());
                }
            }
        }
        profil.setUser(userService.getUserWithAuthorities());
        profil = profilRepository.save(profil);
        return profilMapper.toDto(profil);
    }

    /**
     *  Get all the profils.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    /**
     * Get all the profils.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProfilDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Profils");
        User user = userService.getUserWithAuthorities();
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++"+user);
        if (user != null) {

                Page<ProfilDTO> profilDTOS;
                if(user.getLogin().equals("consulat")){
                    profilDTOS =profilRepository.findByAdmin(Boolean.TRUE,pageable)
                        .map(ProfilDTO::new);
                    total = profilRepository.countAllByAdmin(Boolean.TRUE);
                }else{
                    profilDTOS =profilRepository.findByAdmin(Boolean.FALSE,pageable)
                        .map(ProfilDTO::new);
                    total = profilRepository.countAllByAdmin(Boolean.FALSE);
                }
                for(ProfilDTO profilDTO : profilDTOS) {
                    Profil profil = profilRepository.findOneWithEagerRelationships(profilDTO.getId());
                    log.error(profil.getRoles().toString());
                    Set<String> authorities = new HashSet<>();
                    for(Authority authority : profil.getRoles()) {
                        authorities.add(authority.getName());
                    }
                    profilDTO.setAuthorities(authorities);

                    if(profil.getUser()!=null)
                    profilDTO.setCreateBy(profil.getUser().getLogin());
                }
                //return profilDTOS;
                return new PageImpl<>(profilDTOS.getContent(),pageable,total);

        }
        return new PageImpl<ProfilDTO>(Collections.emptyList());
    }






    /*
    Liste des profils ajouter par un utilisateur donné
 */
    @Transactional(readOnly = true)
    public List<ProfilDTO> findAllByUserCreated() {
        log.debug("Request to get all Profils");
        User user = userService.getUserWithAuthorities();
        if (user != null){

                List<ProfilDTO> profilDTOS = new ArrayList<>();
                if(user.getLogin().equals("consulat")){
                    profilDTOS =profilMapper.toDto(profilRepository.findByAdmin(Boolean.TRUE));
                }else{
                    profilDTOS =profilMapper.toDto(profilRepository.findByAdmin(Boolean.FALSE));
                }
                for(ProfilDTO profilDTO : profilDTOS) {
                    Profil profil = profilRepository.findOneWithEagerRelationships(profilDTO.getId());
                    log.error(profil.getRoles().toString());
                    Set<String> authorities = new HashSet<>();
                    for(Authority authority : profil.getRoles()) {
                        authorities.add(authority.getName());
                    }
                    profilDTO.setAuthorities(authorities);

                }
                return profilDTOS;

        }
        return Collections.emptyList();
    }




    /**
     *  Get one profil by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProfilDTO findOne(Long id) {
        log.debug("Request to get Profil : {}", id);
        Profil profil = profilRepository.findOneWithEagerRelationships(id);
        return profilMapper.toDto(profil);
    }

    /**
     *  Delete the  profil by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Profil : {}", id);
        profilRepository.delete(id);
    }
}
