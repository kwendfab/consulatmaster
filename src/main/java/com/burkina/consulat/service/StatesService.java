package com.burkina.consulat.service;

import com.burkina.consulat.domain.States;
import com.burkina.consulat.repository.StatesRepository;
import com.burkina.consulat.service.dto.StatesDTO;
import com.burkina.consulat.service.mapper.StatesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing States.
 */
@Service
@Transactional
public class StatesService {

    private final Logger log = LoggerFactory.getLogger(StatesService.class);

    private final StatesRepository statesRepository;

    private final StatesMapper statesMapper;

    public StatesService(StatesRepository statesRepository, StatesMapper statesMapper) {
        this.statesRepository = statesRepository;
        this.statesMapper = statesMapper;
    }

    /**
     * Save a states.
     *
     * @param statesDTO the entity to save
     * @return the persisted entity
     */
    public StatesDTO save(StatesDTO statesDTO) {
        log.debug("Request to save States : {}", statesDTO);
       // statesDTO.setCode(this.getStatesNumero());
        States states = statesMapper.toEntity(statesDTO);
        states = statesRepository.save(states);
        return statesMapper.toDto(states);
    }

    public String getStatesNumero() throws NumberFormatException {
        String code = null;
        if(statesRepository.getMaxNumeroStates()!=null) {
            Long maximum = statesRepository.getMaxNumeroStates();
            code = String.format("%09d", maximum + 1);
        }

        else
        {
            code = String.format("%09d", 1);
        }

        return code;
    }

    /**
     *  Get all the states.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StatesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all States");
        return statesRepository.findAll(pageable)
            .map(statesMapper::toDto);
    }

    /**
     *  Get one states by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public StatesDTO findOne(Long id) {
        log.debug("Request to get States : {}", id);
        States states = statesRepository.findOne(id);
        return statesMapper.toDto(states);
    }

    /**
     *  Get one states by country id.
     *
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public List<StatesDTO> findCountryId(Long id) {
        List<States> states = statesRepository.findByCountryId(id);
        return statesMapper.toDto(states);
    }

    /**
     *  Delete the  states by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete States : {}", id);
        statesRepository.delete(id);
    }
}
