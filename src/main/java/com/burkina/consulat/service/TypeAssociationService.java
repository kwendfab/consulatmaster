package com.burkina.consulat.service;

import com.burkina.consulat.domain.TypeAssociation;
import com.burkina.consulat.repository.TypeAssociationRepository;
import com.burkina.consulat.service.dto.TypeAssociationDTO;
import com.burkina.consulat.service.mapper.TypeAssociationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TypeAssociation}.
 */
@Service
@Transactional
public class TypeAssociationService {

    private final Logger log = LoggerFactory.getLogger(TypeAssociationService.class);

    private final TypeAssociationRepository typeAssociationRepository;

    private final TypeAssociationMapper typeAssociationMapper;

    public TypeAssociationService(TypeAssociationRepository typeAssociationRepository, TypeAssociationMapper typeAssociationMapper) {
        this.typeAssociationRepository = typeAssociationRepository;
        this.typeAssociationMapper = typeAssociationMapper;
    }

    /**
     * Save a typeAssociation.
     *
     * @param typeAssociationDTO the entity to save.
     * @return the persisted entity.
     */
    public TypeAssociationDTO save(TypeAssociationDTO typeAssociationDTO) {
        log.debug("Request to save TypeAssociation : {}", typeAssociationDTO);
        TypeAssociation typeAssociation = typeAssociationMapper.toEntity(typeAssociationDTO);
        typeAssociation = typeAssociationRepository.save(typeAssociation);
        return typeAssociationMapper.toDto(typeAssociation);
    }

    /**
     * Get all the typeAssociations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TypeAssociationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TypeAssociations");
        return typeAssociationRepository.findAll(pageable)
            .map(typeAssociationMapper::toDto);
    }


    /**
     * Get one typeAssociation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public TypeAssociationDTO findOne(Long id) {
        log.debug("Request to get TypeAssociation : {}", id);
        TypeAssociation typeAssociation = typeAssociationRepository.findOne(id);
            return typeAssociationMapper.toDto(typeAssociation);
    }

    /**
     * Delete the typeAssociation by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TypeAssociation : {}", id);
        typeAssociationRepository.delete(id);
    }
}
