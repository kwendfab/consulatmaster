package com.burkina.consulat.service;

import com.burkina.consulat.domain.User;
import com.burkina.consulat.domain.UserExtra;
import com.burkina.consulat.repository.UserExtraRepository;
import com.burkina.consulat.service.dto.ProfilDTO;
import com.burkina.consulat.service.dto.UserExtraDTO;
import com.burkina.consulat.service.mapper.UserExtraMapper;
import com.burkina.consulat.service.mapper.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Service Implementation for managing UserExtra.
 */
@Service
@Transactional
public class UserExtraService {

    private final Logger log = LoggerFactory.getLogger(UserExtraService.class);

    private final UserExtraRepository userExtraRepository;

    private final UserExtraMapper userExtraMapper;

    private final UserService userService;

    private final ProfilService profilService;

    private final UserMapper userMapper;

    private Integer total = 0;

    public UserExtraService(UserExtraRepository userExtraRepository, UserExtraMapper userExtraMapper, UserService userService, ProfilService profilService, UserMapper userMapper) {
        this.userExtraRepository = userExtraRepository;
        this.userExtraMapper = userExtraMapper;
        this.userService = userService;
        this.profilService = profilService;
        this.userMapper = userMapper;
    }

    /**
     * Save a userExtra.
     *
     * @param userExtraDTO the entity to save
     * @return the persisted entity
     */
    public UserExtraDTO save(UserExtraDTO userExtraDTO) {
        log.debug("Request to save UserExtra : {}", userExtraDTO);
        UserExtra userExtra = userExtraMapper.toEntity(userExtraDTO);
        userExtra = userExtraRepository.save(userExtra);
        return userExtraMapper.toDto(userExtra);
    }

    /**
     *  Get all the userExtras.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserExtraDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserExtras");
        return userExtraRepository.findAll(pageable)
            .map(userExtraMapper::toDto);
    }


    /*
     *Recupère toutes les informations de l'utilisateur
     */

    private ArrayList<UserExtraDTO> getUserExtraAllDTOS(List<UserExtra> userExtras) {
        ArrayList<UserExtraDTO> userExtraAllDTOs = new ArrayList<>();
        if (!userExtras.isEmpty()) {
            userExtras.forEach(userExtra -> {

                User user = userExtra.getUser();
                UserExtraDTO userExtraAllDTO = new UserExtraDTO();
                userExtraAllDTO = userExtraMapper.toDto(userExtra);

                if (user != null) {
                    userExtraAllDTO.setUserDTO(userMapper.userToUserDTO(user));

                }

                userExtraAllDTOs.add(userExtraAllDTO);
            });
        }
        return userExtraAllDTOs;
    }

    /*
        Liste des utilisateur en fonction d'un profil
     */
    @Transactional(readOnly = true)
    public List<UserExtra> findAllUserByProfil(ProfilDTO profilDTO,Pageable pageable) {
        total = total + userExtraRepository.countAllByProfilId(profilDTO.getId());
        return userExtraRepository.findByProfilId(profilDTO.getId(), pageable).getContent();
    }

    @Transactional(readOnly = true)
    public Page<UserExtraDTO> findAllUser(Pageable pageable) {
        ArrayList<UserExtraDTO> userExtraAllDTOs = new ArrayList<>();
        User user = userService.getUserWithAuthorities();
        if(!user.getLogin().equals("admin")) {
            List<ProfilDTO> profils = profilService.findAllByUserCreated();
            if (!profils.isEmpty()) {
                profils.stream().forEach(profilDTO -> {
                    userExtraAllDTOs.addAll(getUserExtraAllDTOS(this.findAllUserByProfil(profilDTO, pageable)));
                });
                return new PageImpl<>(userExtraAllDTOs, pageable, total);
            }
        }else{

                userExtraAllDTOs.addAll(getUserExtraAllDTOS(userExtraRepository.findAll(pageable).getContent()));
                total = userExtraRepository.countAllBy();

            return new PageImpl<>(userExtraAllDTOs, pageable, total);
        }
        return new PageImpl<>(Collections.emptyList());
    }



    /**
     *  Get one userExtra by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UserExtraDTO findOne(Long id) {
        log.debug("Request to get UserExtra : {}", id);
        UserExtra userExtra = userExtraRepository.findOne(id);
        return userExtraMapper.toDto(userExtra);
    }

    @Transactional(readOnly = true)
    public UserExtraDTO findUserExtraWithUserLogin(Long userId) {
        log.debug("Debut de la recuperation d'un UserExtra par l'Id du User");
        UserExtra userExtra = userExtraRepository.findOneByUserId(userId);
        return userExtraMapper.toDto(userExtra);
    }

    /*
      Les infos de l'utilisateur connecte
   */
    @Transactional(readOnly = true)
    public UserExtra getUserExtraConnected() {
        User user = userService.getUserWithAuthorities();
        log.debug("Le login de l'utilisateur est : " + user.getLogin() + " Son ID est : " + user.getId());
        if(this.findUserExtraWithUserLogin(user.getId())!=null) {
            return userExtraMapper.toEntity(this.findUserExtraWithUserLogin(user.getId()));
        }
        else
        {
            UserExtra userExtra = new UserExtra();
            return userExtra;
        }
    }

    /**
     *  Delete the  userExtra by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserExtra : {}", id);
        userExtraRepository.delete(id);
    }
}
