package com.burkina.consulat.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Adresses entity.
 */
public class AdressesDTO implements Serializable {

    private Long id;

    private String rue;

    private String noPorte;

    private String mail;

    private String telephone;

    private String zipCode;

    private String autre;

    private String ville;

    private String etat;

    private Long villeId;

    private Long posteId;

    private Long etatId;

    private Long cityId;

    private Boolean actuel;

    public Long getPosteId() {
        return posteId;
    }

    public void setPosteId(Long posteId) {
        this.posteId = posteId;
    }

    public Boolean getActuel() {
        return actuel;
    }

    public void setActuel(Boolean actuel) {
        this.actuel = actuel;
    }

    public Long getVilleId() {
        return villeId;
    }

    public void setVilleId(Long villeId) {
        this.villeId = villeId;
    }

    public Long getEtatId() {
        return etatId;
    }

    public void setEtatId(Long etatId) {
        this.etatId = etatId;
    }

    private Long membersId;

    private String membersNom;

    public String getMembersNom() {
        return membersNom;
    }

    public void setMembersNom(String membersNom) {
        this.membersNom = membersNom;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getNoPorte() {
        return noPorte;
    }

    public void setNoPorte(String noPorte) {
        this.noPorte = noPorte;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAutre() {
        return autre;
    }

    public void setAutre(String autre) {
        this.autre = autre;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Long getMembersId() {
        return membersId;
    }

    public void setMembersId(Long membersId) {
        this.membersId = membersId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdressesDTO adressesDTO = (AdressesDTO) o;
        if(adressesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), adressesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdressesDTO{" +
            "id=" + getId() +
            ", rue='" + getRue() + "'" +
            ", noPorte='" + getNoPorte() + "'" +
            ", mail='" + getMail() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", zipCode='" + getZipCode() + "'" +
            ", autre='" + getAutre() + "'" +
            ", ville='" + getVille() + "'" +
            ", etat='" + getEtat() + "'" +
            "}";
    }
}
