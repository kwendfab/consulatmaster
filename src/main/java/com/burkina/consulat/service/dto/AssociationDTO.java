package com.burkina.consulat.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Association entity.
 */
public class AssociationDTO implements Serializable {

    private Long id;

    private String numero;

    private String nom;

    private String poste;

    private LocalDate dateCreation;

    private String type;

    private Long adressesId;

    private AdressesDTO adresseDTO;

    private String siege;

    private Long posteId;

    LocalDate dateFonction;

    public LocalDate getDateFonction() {
        return dateFonction;
    }

    public void setDateFonction(LocalDate dateFonction) {
        this.dateFonction = dateFonction;
    }

    public Long getPosteId() {
        return posteId;
    }

    public void setPosteId(Long posteId) {
        this.posteId = posteId;
    }

    public AdressesDTO getAdresseDTO() {
        return adresseDTO;
    }

    public void setAdresseDTO(AdressesDTO adresseDTO) {
        this.adresseDTO = adresseDTO;
    }

    public String getSiege() {
        return siege;
    }

    public void setSiege(String siege) {
        this.siege = siege;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public LocalDate getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDate dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getAdressesId() {
        return adressesId;
    }

    public void setAdressesId(Long adressesId) {
        this.adressesId = adressesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AssociationDTO associationDTO = (AssociationDTO) o;
        if(associationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), associationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AssociationDTO{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", nom='" + getNom() + "'" +
            ", poste='" + getPoste() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
