package com.burkina.consulat.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.burkina.consulat.domain.enumeration.DocId;

/**
 * A DTO for the BfIdDoc entity.
 */
public class BfIdDocDTO implements Serializable {

    private Long id;

    @NotNull
    private String numero;

    private String autorite;

    private DocId docId;

    private LocalDate dateDelivr;

    private LocalDate dateExpr;

    private String ville;

    private Long cityId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAutorite() {
        return autorite;
    }

    public void setAutorite(String autorite) {
        this.autorite = autorite;
    }

    public DocId getDocId() {
        return docId;
    }

    public void setDocId(DocId docId) {
        this.docId = docId;
    }

    public LocalDate getDateDelivr() {
        return dateDelivr;
    }

    public void setDateDelivr(LocalDate dateDelivr) {
        this.dateDelivr = dateDelivr;
    }

    public LocalDate getDateExpr() {
        return dateExpr;
    }

    public void setDateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BfIdDocDTO bfIdDocDTO = (BfIdDocDTO) o;
        if(bfIdDocDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bfIdDocDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BfIdDocDTO{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", autorite='" + getAutorite() + "'" +
            ", docId='" + getDocId() + "'" +
            ", dateDelivr='" + getDateDelivr() + "'" +
            ", dateExpr='" + getDateExpr() + "'" +
            ", ville='" + getVille() + "'" +
            "}";
    }
}
