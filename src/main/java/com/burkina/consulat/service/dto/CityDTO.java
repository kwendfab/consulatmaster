package com.burkina.consulat.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the City entity.
 */
public class CityDTO implements Serializable {

    private Long id;

    private String code;

    private String libelle;

    private Long countriesId;

    private String countriesLibelle;

    private Long statesId;

    private String statesLibelle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Long getCountriesId() {
        return countriesId;
    }

    public void setCountriesId(Long countryId) {
        this.countriesId = countryId;
    }

    public String getCountriesLibelle() {
        return countriesLibelle;
    }

    public void setCountriesLibelle(String countryLibelle) {
        this.countriesLibelle = countryLibelle;
    }

    public Long getStatesId() {
        return statesId;
    }

    public void setStatesId(Long statesId) {
        this.statesId = statesId;
    }

    public String getStatesLibelle() {
        return statesLibelle;
    }

    public void setStatesLibelle(String statesLibelle) {
        this.statesLibelle = statesLibelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CityDTO cityDTO = (CityDTO) o;
        if(cityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CityDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}
