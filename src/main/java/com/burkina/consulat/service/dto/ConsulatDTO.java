package com.burkina.consulat.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the Consulat entity.
 */
public class ConsulatDTO implements Serializable {

    private Long id;

    private String code;

    private String nom;

    @Size(max = 1000)
    private String description;

    @Lob
    private byte[] logoBf;
    private String logoBfContentType;

    @Lob
    private byte[] logoUsa;
    private String logoUsaContentType;

    @Lob
    private byte[] armoirie;
    private String armoirieContentType;

    @Lob
    private byte[] signature;
    private String signatureContentType;

    @Size(max = 50)
    private String enTete;

    @Size(max = 50)
    private String titre;

    @Lob
    private byte[] imageVerso;
    private String imageVersoContentType;

    @Size(max = 1000)
    private String texteVerso;

    @Size(max = 10)
    private String codeCarte;

    private Long userExtraId;

    private Long adressesId;

    private AdressesDTO adresseDTO;

    private Long countryId;


    public AdressesDTO getAdresseDTO() {
        return adresseDTO;
    }

    public void setAdresseDTO(AdressesDTO adresseDTO) {
        this.adresseDTO = adresseDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getLogoBf() {
        return logoBf;
    }

    public void setLogoBf(byte[] logoBf) {
        this.logoBf = logoBf;
    }

    public String getLogoBfContentType() {
        return logoBfContentType;
    }

    public void setLogoBfContentType(String logoBfContentType) {
        this.logoBfContentType = logoBfContentType;
    }

    public byte[] getLogoUsa() {
        return logoUsa;
    }

    public void setLogoUsa(byte[] logoUsa) {
        this.logoUsa = logoUsa;
    }

    public String getLogoUsaContentType() {
        return logoUsaContentType;
    }

    public void setLogoUsaContentType(String logoUsaContentType) {
        this.logoUsaContentType = logoUsaContentType;
    }

    public byte[] getArmoirie() {
        return armoirie;
    }

    public void setArmoirie(byte[] armoirie) {
        this.armoirie = armoirie;
    }

    public String getArmoirieContentType() {
        return armoirieContentType;
    }

    public void setArmoirieContentType(String armoirieContentType) {
        this.armoirieContentType = armoirieContentType;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public String getSignatureContentType() {
        return signatureContentType;
    }

    public void setSignatureContentType(String signatureContentType) {
        this.signatureContentType = signatureContentType;
    }

    public String getEnTete() {
        return enTete;
    }

    public void setEnTete(String enTete) {
        this.enTete = enTete;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public byte[] getImageVerso() {
        return imageVerso;
    }

    public void setImageVerso(byte[] imageVerso) {
        this.imageVerso = imageVerso;
    }

    public String getImageVersoContentType() {
        return imageVersoContentType;
    }

    public void setImageVersoContentType(String imageVersoContentType) {
        this.imageVersoContentType = imageVersoContentType;
    }

    public String getTexteVerso() {
        return texteVerso;
    }

    public void setTexteVerso(String texteVerso) {
        this.texteVerso = texteVerso;
    }

    public String getCodeCarte() {
        return codeCarte;
    }

    public void setCodeCarte(String codeCarte) {
        this.codeCarte = codeCarte;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    public Long getAdressesId() {
        return adressesId;
    }

    public void setAdressesId(Long adressesId) {
        this.adressesId = adressesId;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConsulatDTO consulatDTO = (ConsulatDTO) o;
        if(consulatDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), consulatDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConsulatDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", nom='" + getNom() + "'" +
            ", description='" + getDescription() + "'" +
            ", logoBf='" + getLogoBf() + "'" +
            ", logoUsa='" + getLogoUsa() + "'" +
            ", armoirie='" + getArmoirie() + "'" +
            ", signature='" + getSignature() + "'" +
            ", enTete='" + getEnTete() + "'" +
            ", titre='" + getTitre() + "'" +
            ", imageVerso='" + getImageVerso() + "'" +
            ", texteVerso='" + getTexteVerso() + "'" +
            ", codeCarte='" + getCodeCarte() + "'" +
            "}";
    }
}
