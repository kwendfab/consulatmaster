package com.burkina.consulat.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the CountryIdDoc entity.
 */
public class CountryIdDocDTO implements Serializable {

    private Long id;

    @NotNull
    private String numero;

    private String autorite;

    private LocalDate dateDelivr;

    private LocalDate dateExpr;

    private String etat;

    private String ville;

    private Long cityId;

    private Long statesId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAutorite() {
        return autorite;
    }

    public void setAutorite(String autorite) {
        this.autorite = autorite;
    }

    public LocalDate getDateDelivr() {
        return dateDelivr;
    }

    public void setDateDelivr(LocalDate dateDelivr) {
        this.dateDelivr = dateDelivr;
    }

    public LocalDate getDateExpr() {
        return dateExpr;
    }

    public void setDateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getStatesId() {
        return statesId;
    }

    public void setStatesId(Long statesId) {
        this.statesId = statesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CountryIdDocDTO countryIdDocDTO = (CountryIdDocDTO) o;
        if(countryIdDocDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), countryIdDocDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CountryIdDocDTO{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", autorite='" + getAutorite() + "'" +
            ", dateDelivr='" + getDateDelivr() + "'" +
            ", dateExpr='" + getDateExpr() + "'" +
            ", etat='" + getEtat() + "'" +
            ", ville='" + getVille() + "'" +
            "}";
    }
}
