package com.burkina.consulat.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the DocumentLegal entity.
 */
public class DocumentLegalDTO implements Serializable {

    private Long id;

    private String numero;

    private LocalDate dateExpr;

    private String libelle;

    private Long countryId;

    private String countryNom;

    public String getCountryNom() {
        return countryNom;
    }

    public void setCountryNom(String countryNom) {
        this.countryNom = countryNom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public LocalDate getDateExpr() {
        return dateExpr;
    }

    public void setDateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DocumentLegalDTO documentLegalDTO = (DocumentLegalDTO) o;
        if(documentLegalDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documentLegalDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DocumentLegalDTO{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", dateExpr='" + getDateExpr() + "'" +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}
