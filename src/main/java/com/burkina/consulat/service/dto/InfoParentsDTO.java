package com.burkina.consulat.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the InfoParents entity.
 */
public class InfoParentsDTO implements Serializable {

    private Long id;

    private String numero;

    private String nomPere;

    private String prenomPere;

    private String nomMere;

    private String prenomMere;

    private String noPere;

    private String noMere;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNomPere() {
        return nomPere;
    }

    public void setNomPere(String nomPere) {
        this.nomPere = nomPere;
    }

    public String getPrenomPere() {
        return prenomPere;
    }

    public void setPrenomPere(String prenomPere) {
        this.prenomPere = prenomPere;
    }

    public String getNomMere() {
        return nomMere;
    }

    public void setNomMere(String nomMere) {
        this.nomMere = nomMere;
    }

    public String getPrenomMere() {
        return prenomMere;
    }

    public void setPrenomMere(String prenomMere) {
        this.prenomMere = prenomMere;
    }

    public String getNoPere() {
        return noPere;
    }

    public void setNoPere(String noPere) {
        this.noPere = noPere;
    }

    public String getNoMere() {
        return noMere;
    }

    public void setNoMere(String noMere) {
        this.noMere = noMere;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InfoParentsDTO infoParentsDTO = (InfoParentsDTO) o;
        if(infoParentsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), infoParentsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InfoParentsDTO{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", nomPere='" + getNomPere() + "'" +
            ", prenomPere='" + getPrenomPere() + "'" +
            ", nomMere='" + getNomMere() + "'" +
            ", prenomMere='" + getPrenomMere() + "'" +
            ", noPere='" + getNoPere() + "'" +
            ", noMere='" + getNoMere() + "'" +
            "}";
    }
}
