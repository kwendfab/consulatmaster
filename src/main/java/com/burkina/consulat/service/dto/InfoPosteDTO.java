package com.burkina.consulat.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the InfoPoste entity.
 */
public class InfoPosteDTO implements Serializable {

    private Long id;

    private LocalDate dateDebut;

    private LocalDate dateFin;

    private Boolean actuelle;

    private Long membersId;

    private String membersNom;

    private Long posteId;

    private String posteLibelle;

    private Long associationId;

    private String associationNom;

    public String getAssociationNom() {
        return associationNom;
    }

    public void setAssociationNom(String associationNom) {
        this.associationNom = associationNom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public Boolean isActuelle() {
        return actuelle;
    }

    public void setActuelle(Boolean actuelle) {
        this.actuelle = actuelle;
    }

    public Long getMembersId() {
        return membersId;
    }

    public void setMembersId(Long membersId) {
        this.membersId = membersId;
    }

    public String getMembersNom() {
        return membersNom;
    }

    public void setMembersNom(String membersNom) {
        this.membersNom = membersNom;
    }

    public Long getPosteId() {
        return posteId;
    }

    public void setPosteId(Long posteId) {
        this.posteId = posteId;
    }

    public String getPosteLibelle() {
        return posteLibelle;
    }

    public void setPosteLibelle(String posteLibelle) {
        this.posteLibelle = posteLibelle;
    }

    public Long getAssociationId() {
        return associationId;
    }

    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InfoPosteDTO infoPosteDTO = (InfoPosteDTO) o;
        if(infoPosteDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), infoPosteDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InfoPosteDTO{" +
            "id=" + getId() +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", actuelle='" + isActuelle() + "'" +
            "}";
    }
}
