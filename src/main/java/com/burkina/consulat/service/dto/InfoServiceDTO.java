package com.burkina.consulat.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the InfoService entity.
 */
public class InfoServiceDTO implements Serializable {

    private Long id;

    private LocalDate dateIntegration;

    private Long membreServiceId;

    private Long membersId;

    private String membersNom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateIntegration() {
        return dateIntegration;
    }

    public void setDateIntegration(LocalDate dateIntegration) {
        this.dateIntegration = dateIntegration;
    }

    public Long getMembreServiceId() {
        return membreServiceId;
    }

    public void setMembreServiceId(Long membreServiceId) {
        this.membreServiceId = membreServiceId;
    }

    public Long getMembersId() {
        return membersId;
    }

    public void setMembersId(Long membersId) {
        this.membersId = membersId;
    }

    public String getMembersNom() {
        return membersNom;
    }

    public void setMembersNom(String membersNom) {
        this.membersNom = membersNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InfoServiceDTO infoServiceDTO = (InfoServiceDTO) o;
        if(infoServiceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), infoServiceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InfoServiceDTO{" +
            "id=" + getId() +
            ", dateIntegration='" + getDateIntegration() + "'" +
            "}";
    }
}
