package com.burkina.consulat.service.dto;


import java.awt.*;
import java.time.LocalDate;
import javax.persistence.Lob;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.burkina.consulat.domain.enumeration.Sexe;

/**
 * A DTO for the Members entity.
 */
public class MembersDTO implements Serializable {

    private Long id;

    private String numero;

    private String nom;

    private String prenom;

    private LocalDate dateNaissance;

    private LocalDate dateCreate;

    private LocalDate dateExpr;

    private String telephoneConsulat;

    private String adresseConsulat;

    private String lieuNaissance;

    private Sexe sexe;

    private String sexes;

    private Boolean mariage;

    private String telephone;

    private LocalDate dateArrivee;

    private String villeArrivee;

    private String etatArrivee;

    private String metier;

    private String noConjoint;

    private String dateDelivrance;

    private String dateExpiration;

    private Boolean memePaysConjoint;

    private String nomConjoint;

    private String prenomConjoint;

    private Boolean parentsUsa;

    private Boolean mumInUsa;

    private Long userExtraId;

    private Long countryId;

    private Long bfIdDocId;

    private Long documentLegalId;

    private Long infoParentsId;

    private Long villeArriveId;

    private Long consulatId;

    private String consulatNom;

    private String occupation;

    private String villeResidence;

    private String etatResidence;

    private CountryIdDocDTO country;

    private BfIdDocDTO bfIdDoc;

    private InfoParentsDTO infoParents;

    private AdressesDTO adresseDTO;

    private String adresse;


    private Image logoBf;

    private Image logoUsa;

    private Image armoirie;

    private Image photos;

    private Image barCode;

    private Image signatureBearer;

    private Image imageVerso;

    private Image signatureAutorite;


    private String taille;

    private Long statesId;

    private Boolean changerAdresse;


    private Set<AssociationDTO> associations = new HashSet<>();

    @Lob
    private byte[] photo;
    private String photoContentType;

    @Lob
    private byte[] signature;
    private String signatureContentType;

    @Size(max = 50)
    private String enTete;

    @Size(max = 50)
    private String titre;

    @Size(max = 10)
    private String codeCarte;

    @Size(max = 1000)
    private String texteVerso;

    public Image getSignatureAutorite() {
        return signatureAutorite;
    }

    public void setSignatureAutorite(Image signatureAutorite) {
        this.signatureAutorite = signatureAutorite;
    }

    public Image getImageVerso() {
        return imageVerso;
    }

    public void setImageVerso(Image imageVerso) {
        this.imageVerso = imageVerso;
    }

    public String getEnTete() {
        return enTete;
    }

    public void setEnTete(String enTete) {
        this.enTete = enTete;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getCodeCarte() {
        return codeCarte;
    }

    public void setCodeCarte(String codeCarte) {
        this.codeCarte = codeCarte;
    }

    public String getTexteVerso() {
        return texteVerso;
    }

    public void setTexteVerso(String texteVerso) {
        this.texteVerso = texteVerso;
    }

    public Image getSignatureBearer() {
        return signatureBearer;
    }

    public void setSignatureBearer(Image signatureBearer) {
        this.signatureBearer = signatureBearer;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public String getSignatureContentType() {
        return signatureContentType;
    }

    public void setSignatureContentType(String signatureContentType) {
        this.signatureContentType = signatureContentType;
    }


    public Boolean getChangerAdresse() {
        return changerAdresse;
    }

    public void setChangerAdresse(Boolean changerAdresse) {
        this.changerAdresse = changerAdresse;
    }

    public Long getStatesId() {
        return statesId;
    }

    public void setStatesId(Long statesId) {
        this.statesId = statesId;
    }

    public String getTelephoneConsulat() {
        return telephoneConsulat;
    }

    public void setTelephoneConsulat(String telephoneConsulat) {
        this.telephoneConsulat = telephoneConsulat;
    }

    public String getAdresseConsulat() {
        return adresseConsulat;
    }

    public void setAdresseConsulat(String adresseConsulat) {
        this.adresseConsulat = adresseConsulat;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public LocalDate getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(LocalDate dateCreate) {
        this.dateCreate = dateCreate;
    }

    public LocalDate getDateExpr() {
        return dateExpr;
    }

    public void setDateExpr(LocalDate dateExpr) {
        this.dateExpr = dateExpr;
    }

    public String getEtatArrivee() {
        return etatArrivee;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    public void setEtatArrivee(String etatArrivee) {
        this.etatArrivee = etatArrivee;
    }

    public String getSexes() {
        return sexes;
    }

    public void setSexes(String sexes) {
        this.sexes = sexes;
    }

    public String getDateDelivrance() {
        return dateDelivrance;
    }

    public void setDateDelivrance(String dateDelivrance) {
        this.dateDelivrance = dateDelivrance;
    }

    public String getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(String dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public Image getBarCode() {
        return barCode;
    }

    public void setBarCode(Image barCode) {
        this.barCode = barCode;
    }

    public Image getLogoBf() {
        return logoBf;
    }

    public void setLogoBf(Image logoBf) {
        this.logoBf = logoBf;
    }

    public Image getLogoUsa() {
        return logoUsa;
    }

    public void setLogoUsa(Image logoUsa) {
        this.logoUsa = logoUsa;
    }

    public Image getArmoirie() {
        return armoirie;
    }

    public void setArmoirie(Image armoirie) {
        this.armoirie = armoirie;
    }

    public Image getPhotos() {
        return photos;
    }

    public void setPhotos(Image photos) {
        this.photos = photos;
    }

    public String getVilleResidence() {
        return villeResidence;
    }

    public void setVilleResidence(String villeResidence) {
        this.villeResidence = villeResidence;
    }

    public String getEtatResidence() {
        return etatResidence;
    }

    public void setEtatResidence(String etatResidence) {
        this.etatResidence = etatResidence;
    }

    public CountryIdDocDTO getCountry() {
        return country;
    }

    public void setCountry(CountryIdDocDTO country) {
        this.country = country;
    }

    public BfIdDocDTO getBfIdDoc() {
        return bfIdDoc;
    }

    public void setBfIdDoc(BfIdDocDTO bfIdDoc) {
        this.bfIdDoc = bfIdDoc;
    }

    public InfoParentsDTO getInfoParents() {
        return infoParents;
    }

    public void setInfoParents(InfoParentsDTO infoParents) {
        this.infoParents = infoParents;
    }

    public AdressesDTO getAdresseDTO() {
        return adresseDTO;
    }

    public void setAdresseDTO(AdressesDTO adresseDTO) {
        this.adresseDTO = adresseDTO;
    }

    public Set<AssociationDTO> getAssociations() {
        return associations;
    }

    public void setAssociations(Set<AssociationDTO> associations) {
        this.associations = associations;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public Sexe getSexe() {
        return sexe;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public Boolean isMariage() {
        return mariage;
    }

    public void setMariage(Boolean mariage) {
        this.mariage = mariage;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public LocalDate getDateArrivee() {
        return dateArrivee;
    }

    public void setDateArrivee(LocalDate dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    public String getVilleArrivee() {
        return villeArrivee;
    }

    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    public String getMetier() {
        return metier;
    }

    public void setMetier(String metier) {
        this.metier = metier;
    }

    public String getNoConjoint() {
        return noConjoint;
    }

    public void setNoConjoint(String noConjoint) {
        this.noConjoint = noConjoint;
    }

    public Boolean isMemePaysConjoint() {
        return memePaysConjoint;
    }

    public void setMemePaysConjoint(Boolean memePaysConjoint) {
        this.memePaysConjoint = memePaysConjoint;
    }

    public String getNomConjoint() {
        return nomConjoint;
    }

    public void setNomConjoint(String nomConjoint) {
        this.nomConjoint = nomConjoint;
    }

    public String getPrenomConjoint() {
        return prenomConjoint;
    }

    public void setPrenomConjoint(String prenomConjoint) {
        this.prenomConjoint = prenomConjoint;
    }

    public Boolean isParentsUsa() {
        return parentsUsa;
    }

    public void setParentsUsa(Boolean parentsUsa) {
        this.parentsUsa = parentsUsa;
    }

    public Boolean isMumInUsa() {
        return mumInUsa;
    }

    public void setMumInUsa(Boolean mumInUsa) {
        this.mumInUsa = mumInUsa;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryIdDocId) {
        this.countryId = countryIdDocId;
    }

    public Long getBfIdDocId() {
        return bfIdDocId;
    }

    public void setBfIdDocId(Long bFIdDocId) {
        this.bfIdDocId = bFIdDocId;
    }

    public Long getDocumentLegalId() {
        return documentLegalId;
    }

    public void setDocumentLegalId(Long documentLegalId) {
        this.documentLegalId = documentLegalId;
    }

    public Long getInfoParentsId() {
        return infoParentsId;
    }

    public void setInfoParentsId(Long infoParentsId) {
        this.infoParentsId = infoParentsId;
    }

    public Long getVilleArriveId() {
        return villeArriveId;
    }

    public void setVilleArriveId(Long cityId) {
        this.villeArriveId = cityId;
    }

    public Long getConsulatId() {
        return consulatId;
    }

    public void setConsulatId(Long consulatId) {
        this.consulatId = consulatId;
    }

    public Boolean getMariage() {
        return mariage;
    }

    public Boolean getMemePaysConjoint() {
        return memePaysConjoint;
    }

    public Boolean getParentsUsa() {
        return parentsUsa;
    }

    public Boolean getMumInUsa() {
        return mumInUsa;
    }

    public String getConsulatNom() {
        return consulatNom;
    }

    public void setConsulatNom(String consulatNom) {
        this.consulatNom = consulatNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MembersDTO membersDTO = (MembersDTO) o;
        if(membersDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), membersDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MembersDTO{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", dateNaissance='" + getDateNaissance() + "'" +
            ", lieuNaissance='" + getLieuNaissance() + "'" +
            ", sexe='" + getSexe() + "'" +
            ", mariage='" + isMariage() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", dateArrivee='" + getDateArrivee() + "'" +
            ", villeArrivee='" + getVilleArrivee() + "'" +
            ", metier='" + getMetier() + "'" +
            ", noConjoint='" + getNoConjoint() + "'" +
            ", memePaysConjoint='" + isMemePaysConjoint() + "'" +
            ", nomConjoint='" + getNomConjoint() + "'" +
            ", prenomConjoint='" + getPrenomConjoint() + "'" +
            ", parentsUsa='" + isParentsUsa() + "'" +
            ", mumInUsa='" + isMumInUsa() + "'" +
            "}";
    }
}
