package com.burkina.consulat.service.dto;

import com.burkina.consulat.domain.enumeration.Sexe;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class MembersNewDTO {

    Long id;

    private String numero;

    private String nom;

    private String prenom;

    private LocalDate dateNaissance;

    private String lieuNaissance;

    private Sexe sexe;

    private Boolean mariage;

    private String telephone;

    private LocalDate dateArrivee;

    private String villeArrivee;

    private String metier;

    private String noConjoint;

    private Boolean memePaysConjoint;

    private String nomConjoint;

    private String prenomConjoint;

    private Boolean parentsUsa;

    private Boolean mumInUsa;



    private Long countryId;

    private Long bfIdDocId;

    private Long documentLegalId;

    private Long infoParentsId;

    private Long villeArriveId;

    private Long consulatId;

    private String consulatNom;

    private CountryIdDocDTO country;

    private BfIdDocDTO bfIdDoc;

    private InfoParentsDTO infoParents;

    private AdressesDTO adresses;

    private Set<AssociationDTO> associations = new HashSet<>();

    private String occupation;

    @Lob
    private byte[] photo;
    private String photoContentType;

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Set<AssociationDTO> getAssociations() {
        return associations;
    }

    public void setAssociations(Set<AssociationDTO> associations) {
        this.associations = associations;
    }

    public Boolean getMumInUsa() {
        return mumInUsa;
    }

    public void setMumInUsa(Boolean mumInUsa) {
        this.mumInUsa = mumInUsa;
    }

    public Boolean getParentsUsa() {
        return parentsUsa;
    }

    public void setParentsUsa(Boolean parentsUsa) {
        this.parentsUsa = parentsUsa;
    }

    public String getNomConjoint() {
        return nomConjoint;
    }

    public void setNomConjoint(String nomConjoint) {
        this.nomConjoint = nomConjoint;
    }

    public String getPrenomConjoint() {
        return prenomConjoint;
    }

    public void setPrenomConjoint(String prenomConjoint) {
        this.prenomConjoint = prenomConjoint;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public Sexe getSexe() {
        return sexe;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public Boolean getMariage() {
        return mariage;
    }

    public void setMariage(Boolean mariage) {
        this.mariage = mariage;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public LocalDate getDateArrivee() {
        return dateArrivee;
    }

    public void setDateArrivee(LocalDate dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    public String getVilleArrivee() {
        return villeArrivee;
    }

    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    public String getMetier() {
        return metier;
    }

    public void setMetier(String metier) {
        this.metier = metier;
    }

    public String getNoConjoint() {
        return noConjoint;
    }

    public void setNoConjoint(String noConjoint) {
        this.noConjoint = noConjoint;
    }

    public Boolean getMemePaysConjoint() {
        return memePaysConjoint;
    }

    public void setMemePaysConjoint(Boolean memePaysConjoint) {
        this.memePaysConjoint = memePaysConjoint;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getBfIdDocId() {
        return bfIdDocId;
    }

    public void setBfIdDocId(Long bfIdDocId) {
        this.bfIdDocId = bfIdDocId;
    }

    public Long getDocumentLegalId() {
        return documentLegalId;
    }

    public void setDocumentLegalId(Long documentLegalId) {
        this.documentLegalId = documentLegalId;
    }

    public Long getInfoParentsId() {
        return infoParentsId;
    }

    public void setInfoParentsId(Long infoParentsId) {
        this.infoParentsId = infoParentsId;
    }

    public Long getVilleArriveId() {
        return villeArriveId;
    }

    public void setVilleArriveId(Long villeArriveId) {
        this.villeArriveId = villeArriveId;
    }

    public Long getConsulatId() {
        return consulatId;
    }

    public void setConsulatId(Long consulatId) {
        this.consulatId = consulatId;
    }

    public String getConsulatNom() {
        return consulatNom;
    }

    public void setConsulatNom(String consulatNom) {
        this.consulatNom = consulatNom;
    }

    public CountryIdDocDTO getCountry() {
        return country;
    }

    public void setCountry(CountryIdDocDTO country) {
        this.country = country;
    }

    public BfIdDocDTO getBfIdDoc() {
        return bfIdDoc;
    }

    public void setBfIdDoc(BfIdDocDTO bfIdDoc) {
        this.bfIdDoc = bfIdDoc;
    }

    public InfoParentsDTO getInfoParents() {
        return infoParents;
    }

    public void setInfoParents(InfoParentsDTO infoParents) {
        this.infoParents = infoParents;
    }

    public AdressesDTO getAdresses() {
        return adresses;
    }

    public void setAdresses(AdressesDTO adresses) {
        this.adresses = adresses;
    }
}
