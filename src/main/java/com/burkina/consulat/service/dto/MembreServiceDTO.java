package com.burkina.consulat.service.dto;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.burkina.consulat.domain.MembreService} entity.
 */
public class MembreServiceDTO implements Serializable {

    private Long id;

    private String numero;

    @Size(max = 1000)
    private String description;

    private String libelle;

    private ZonedDateTime date;

    private String type;

    private String consulat;


    private Long userExtraId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConsulat() {
        return consulat;
    }

    public void setConsulat(String consulat) {
        this.consulat = consulat;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MembreServiceDTO membreServiceDTO = (MembreServiceDTO) o;
        if (membreServiceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), membreServiceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MembreServiceDTO{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", description='" + getDescription() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", date='" + getDate() + "'" +
            ", type='" + getType() + "'" +
            ", consulat='" + getConsulat() + "'" +
            ", userExtra=" + getUserExtraId() +
            "}";
    }
}
