package com.burkina.consulat.service.dto;


import com.burkina.consulat.domain.Authority;
import com.burkina.consulat.domain.Profil;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A DTO for the Profil entity.
 */
public class ProfilDTO implements Serializable {

    private Long id;

    private String nom;

    private String description;

    private Set<String> authorities;

    private Long userId;

    private String userLogin;

    private String CreateBy;

    private Boolean profilsChange;

    private Boolean admin;



    public ProfilDTO() {
    }

    public ProfilDTO(Profil profil) {
        this(profil.getId(),profil.getNom(),profil.getDescription(),
            profil.getRoles().stream().map(Authority::getName)
                .collect(Collectors.toSet()));
    }

    public ProfilDTO(Long id, String nom, String description,Set<String> authorities) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.authorities = authorities;
    }


    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String createBy) {
        CreateBy = createBy;
    }

    public Boolean getProfilsChange() {
        return profilsChange;
    }

    public void setProfilsChange(Boolean profilsChange) {
        this.profilsChange = profilsChange;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProfilDTO profilDTO = (ProfilDTO) o;
        if(profilDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), profilDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProfilDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
