package com.burkina.consulat.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.burkina.consulat.domain.TypeAssociation} entity.
 */
public class TypeAssociationDTO implements Serializable {

    private Long id;

    private String libelle;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TypeAssociationDTO typeAssociationDTO = (TypeAssociationDTO) o;
        if (typeAssociationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), typeAssociationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TypeAssociationDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}
