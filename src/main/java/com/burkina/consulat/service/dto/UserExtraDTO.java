package com.burkina.consulat.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the UserExtra entity.
 */
public class UserExtraDTO implements Serializable {

    private Long id;

    private String telephone;

    @Lob
    private byte[] photo;

    private String photoContentType;

    private Long userId;

    private Long consulatId;

    private Long profilId;

    private Long membersId;

    private String profilNom;

    private String consulatNom;

    private UserDTO userDTO;


    public String getProfilNom() {
        return profilNom;
    }

    public void setProfilNom(String profilNom) {
        this.profilNom = profilNom;
    }

    public String getConsulatNom() {
        return consulatNom;
    }

    public void setConsulatNom(String consulatNom) {
        this.consulatNom = consulatNom;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProfilId() {
        return profilId;
    }

    public void setProfilId(Long profilId) {
        this.profilId = profilId;
    }

    public Long getMembersId() {
        return membersId;
    }

    public void setMembersId(Long membersId) {
        this.membersId = membersId;
    }

    public Long getConsulatId() {
        return consulatId;
    }

    public void setConsulatId(Long consulatId) {
        this.consulatId = consulatId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserExtraDTO userExtraDTO = (UserExtraDTO) o;
        if (userExtraDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userExtraDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserExtraDTO{" +
            "id=" + getId() +
            ", telephone='" + getTelephone() + "'" +
            ", photo='" + getPhoto() + "'" +
            ", user=" + getUserId() +
            ", profil=" + getProfilId() +
            ", members=" + getMembersId() +
            ", consulat=" + getConsulatId() +
            "}";
    }
}
