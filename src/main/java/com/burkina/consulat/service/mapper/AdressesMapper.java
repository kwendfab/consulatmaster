package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.AdressesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Adresses and its DTO AdressesDTO.
 */
@Mapper(componentModel = "spring", uses = {MembersMapper.class,CityMapper.class})
public interface AdressesMapper extends EntityMapper<AdressesDTO, Adresses> {

    @Mapping(source = "members.id", target = "membersId")
    @Mapping(source = "members.nom", target = "membersNom")
    @Mapping(source = "city.id", target = "cityId")

    AdressesDTO toDto(Adresses adresses);

    @Mapping(source = "membersId", target = "members")
    @Mapping(source = "cityId", target = "city")

    Adresses toEntity(AdressesDTO adressesDTO);

    default Adresses fromId(Long id) {
        if (id == null) {
            return null;
        }
        Adresses adresses = new Adresses();
        adresses.setId(id);
        return adresses;
    }
}
