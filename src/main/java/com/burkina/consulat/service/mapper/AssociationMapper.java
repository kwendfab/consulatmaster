package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.AssociationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Association and its DTO AssociationDTO.
 */
@Mapper(componentModel = "spring", uses = {AdressesMapper.class})
public interface AssociationMapper extends EntityMapper<AssociationDTO, Association> {

    @Mapping(source = "adresses.id", target = "adressesId")
    @Mapping(source = "adresses.ville", target = "siege")
    AssociationDTO toDto(Association association);

    @Mapping(target = "infoAssociations", ignore = true)
    @Mapping(source = "adressesId", target = "adresses")
    Association toEntity(AssociationDTO associationDTO);

    default Association fromId(Long id) {
        if (id == null) {
            return null;
        }
        Association association = new Association();
        association.setId(id);
        return association;
    }
}
