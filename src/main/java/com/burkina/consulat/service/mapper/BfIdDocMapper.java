package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.BfIdDocDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity BfIdDoc and its DTO BfIdDocDTO.
 */
@Mapper(componentModel = "spring", uses = {CityMapper.class})
public interface BfIdDocMapper extends EntityMapper<BfIdDocDTO, BfIdDoc> {

    @Mapping(source = "city.id", target = "cityId")
    BfIdDocDTO toDto(BfIdDoc bfIdDoc);

    @Mapping(source = "cityId", target = "city")
    BfIdDoc toEntity(BfIdDocDTO bfIdDocDTO);

    default BfIdDoc fromId(Long id) {
        if (id == null) {
            return null;
        }
        BfIdDoc bfIdDoc = new BfIdDoc();
        bfIdDoc.setId(id);
        return bfIdDoc;
    }
}
