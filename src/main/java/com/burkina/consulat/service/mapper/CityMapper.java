package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.CityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity City and its DTO CityDTO.
 */
@Mapper(componentModel = "spring", uses = {CountryMapper.class, StatesMapper.class})
public interface CityMapper extends EntityMapper<CityDTO, City> {

    @Mapping(source = "countries.id", target = "countriesId")
    @Mapping(source = "countries.libele", target = "countriesLibelle")
    @Mapping(source = "states.id", target = "statesId")
    @Mapping(source = "states.libelle", target = "statesLibelle")
    CityDTO toDto(City city);

    @Mapping(source = "countriesId", target = "countries")
    @Mapping(source = "statesId", target = "states")
    City toEntity(CityDTO cityDTO);

    default City fromId(Long id) {
        if (id == null) {
            return null;
        }
        City city = new City();
        city.setId(id);
        return city;
    }
}
