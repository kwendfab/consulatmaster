package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.ConsulatDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Consulat and its DTO ConsulatDTO.
 */
@Mapper(componentModel = "spring", uses = {UserExtraMapper.class, AdressesMapper.class, CountryMapper.class})
public interface ConsulatMapper extends EntityMapper<ConsulatDTO, Consulat> {

    @Mapping(source = "userExtra.id", target = "userExtraId")
    @Mapping(source = "adresses.id", target = "adressesId")
    @Mapping(source = "country.id", target = "countryId")
    ConsulatDTO toDto(Consulat consulat); 

    @Mapping(source = "userExtraId", target = "userExtra")
    @Mapping(source = "adressesId", target = "adresses")
    @Mapping(source = "countryId", target = "country")
    @Mapping(target = "members", ignore = true)
    Consulat toEntity(ConsulatDTO consulatDTO);

    default Consulat fromId(Long id) {
        if (id == null) {
            return null;
        }
        Consulat consulat = new Consulat();
        consulat.setId(id);
        return consulat;
    }
}
