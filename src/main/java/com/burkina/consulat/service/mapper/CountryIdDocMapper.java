package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.CountryIdDocDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CountryIdDoc and its DTO CountryIdDocDTO.
 */
@Mapper(componentModel = "spring", uses = {CityMapper.class, StatesMapper.class})
public interface CountryIdDocMapper extends EntityMapper<CountryIdDocDTO, CountryIdDoc> {

    @Mapping(source = "city.id", target = "cityId")
    @Mapping(source = "states.id", target = "statesId")
    CountryIdDocDTO toDto(CountryIdDoc countryIdDoc); 

    @Mapping(source = "cityId", target = "city")
    @Mapping(source = "statesId", target = "states")
    CountryIdDoc toEntity(CountryIdDocDTO countryIdDocDTO);

    default CountryIdDoc fromId(Long id) {
        if (id == null) {
            return null;
        }
        CountryIdDoc countryIdDoc = new CountryIdDoc();
        countryIdDoc.setId(id);
        return countryIdDoc;
    }
}
