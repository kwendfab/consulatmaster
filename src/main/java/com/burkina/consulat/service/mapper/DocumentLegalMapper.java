package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.DocumentLegalDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DocumentLegal and its DTO DocumentLegalDTO.
 */
@Mapper(componentModel = "spring", uses = {CountryMapper.class})
public interface DocumentLegalMapper extends EntityMapper<DocumentLegalDTO, DocumentLegal> {

    @Mapping(source = "country.id", target = "countryId")
    @Mapping(source = "country.libele", target = "countryNom")

    DocumentLegalDTO toDto(DocumentLegal documentLegal);

    @Mapping(source = "countryId", target = "country")
    DocumentLegal toEntity(DocumentLegalDTO documentLegalDTO);

    default DocumentLegal fromId(Long id) {
        if (id == null) {
            return null;
        }
        DocumentLegal documentLegal = new DocumentLegal();
        documentLegal.setId(id);
        return documentLegal;
    }
}
