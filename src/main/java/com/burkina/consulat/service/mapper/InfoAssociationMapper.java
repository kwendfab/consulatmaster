package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.InfoAssociationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity InfoAssociation and its DTO InfoAssociationDTO.
 */
@Mapper(componentModel = "spring", uses = {MembersMapper.class, AssociationMapper.class})
public interface InfoAssociationMapper extends EntityMapper<InfoAssociationDTO, InfoAssociation> {

    @Mapping(source = "members.id", target = "membersId")
    @Mapping(source = "association.id", target = "associationId")
    InfoAssociationDTO toDto(InfoAssociation infoAssociation);

    @Mapping(source = "membersId", target = "members")
    @Mapping(source = "associationId", target = "association")
    InfoAssociation toEntity(InfoAssociationDTO infoAssociationDTO);

    default InfoAssociation fromId(Long id) {
        if (id == null) {
            return null;
        }
        InfoAssociation infoAssociation = new InfoAssociation();
        infoAssociation.setId(id);
        return infoAssociation;
    }
}
