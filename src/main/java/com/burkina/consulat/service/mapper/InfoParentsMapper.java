package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.InfoParentsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity InfoParents and its DTO InfoParentsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InfoParentsMapper extends EntityMapper<InfoParentsDTO, InfoParents> {

    

    

    default InfoParents fromId(Long id) {
        if (id == null) {
            return null;
        }
        InfoParents infoParents = new InfoParents();
        infoParents.setId(id);
        return infoParents;
    }
}
