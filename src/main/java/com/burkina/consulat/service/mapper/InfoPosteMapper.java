package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.InfoPosteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity InfoPoste and its DTO InfoPosteDTO.
 */
@Mapper(componentModel = "spring", uses = {MembersMapper.class, PosteMapper.class, AssociationMapper.class})
public interface InfoPosteMapper extends EntityMapper<InfoPosteDTO, InfoPoste> {

    @Mapping(source = "members.id", target = "membersId")
    @Mapping(source = "members.nom", target = "membersNom")
    @Mapping(source = "poste.id", target = "posteId")
    @Mapping(source = "poste.libelle", target = "posteLibelle")
    @Mapping(source = "association.id", target = "associationId")
    @Mapping(source = "association.nom", target = "associationNom")

    InfoPosteDTO toDto(InfoPoste infoPoste);

    @Mapping(source = "membersId", target = "members")
    @Mapping(source = "posteId", target = "poste")
    @Mapping(source = "associationId", target = "association")

    InfoPoste toEntity(InfoPosteDTO infoPosteDTO);

    default InfoPoste fromId(Long id) {
        if (id == null) {
            return null;
        }
        InfoPoste infoPoste = new InfoPoste();
        infoPoste.setId(id);
        return infoPoste;
    }
}
