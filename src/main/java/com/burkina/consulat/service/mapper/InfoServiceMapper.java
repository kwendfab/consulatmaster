package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.InfoServiceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity InfoService and its DTO InfoServiceDTO.
 */
@Mapper(componentModel = "spring", uses = {MembreServiceMapper.class, MembersMapper.class})
public interface InfoServiceMapper extends EntityMapper<InfoServiceDTO, InfoService> {

    @Mapping(source = "membreService.id", target = "membreServiceId")
    @Mapping(source = "members.id", target = "membersId")
    @Mapping(source = "members.nom", target = "membersNom")
    InfoServiceDTO toDto(InfoService infoService); 

    @Mapping(source = "membreServiceId", target = "membreService")
    @Mapping(source = "membersId", target = "members")
    InfoService toEntity(InfoServiceDTO infoServiceDTO);

    default InfoService fromId(Long id) {
        if (id == null) {
            return null;
        }
        InfoService infoService = new InfoService();
        infoService.setId(id);
        return infoService;
    }
}
