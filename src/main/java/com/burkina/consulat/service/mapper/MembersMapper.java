package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.MembersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Members and its DTO MembersDTO.
 */
@Mapper(componentModel = "spring", uses = {UserExtraMapper.class, CountryIdDocMapper.class, BfIdDocMapper.class, DocumentLegalMapper.class, InfoParentsMapper.class, CityMapper.class, ConsulatMapper.class})
public interface MembersMapper extends EntityMapper<MembersDTO, Members> {

    @Mapping(source = "userExtra.id", target = "userExtraId")
    @Mapping(source = "country.id", target = "countryId")
    @Mapping(source = "bfIdDoc.id", target = "bfIdDocId")
    @Mapping(source = "documentLegal.id", target = "documentLegalId")
    @Mapping(source = "infoParents.id", target = "infoParentsId")
    @Mapping(source = "villeArrive.id", target = "villeArriveId")
    @Mapping(source = "consulat.id", target = "consulatId")
    @Mapping(source = "consulat.nom", target = "consulatNom")
    MembersDTO toDto(Members members);

    @Mapping(source = "userExtraId", target = "userExtra")
    @Mapping(source = "countryId", target = "country")
    @Mapping(source = "bfIdDocId", target = "bfIdDoc")
    @Mapping(source = "documentLegalId", target = "documentLegal")
    @Mapping(source = "infoParentsId", target = "infoParents")
    @Mapping(source = "villeArriveId", target = "villeArrive")
    @Mapping(target = "infoPostes", ignore = true)
    @Mapping(target = "adresses", ignore = true)
    @Mapping(target = "infoServices", ignore = true)
    @Mapping(source = "consulatId", target = "consulat")
    @Mapping(target = "infoAssociations", ignore = true)
    Members toEntity(MembersDTO membersDTO);

    default Members fromId(Long id) {
        if (id == null) {
            return null;
        }
        Members members = new Members();
        members.setId(id);
        return members;
    }
}
