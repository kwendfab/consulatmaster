package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.MembreServiceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MembreService} and its DTO {@link MembreServiceDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserExtraMapper.class})
public interface MembreServiceMapper extends EntityMapper<MembreServiceDTO, MembreService> {

    @Mapping(source = "userExtra.id", target = "userExtraId")
    MembreServiceDTO toDto(MembreService membreService);

    @Mapping(target = "infoServis", ignore = true)
    @Mapping(source = "userExtraId", target = "userExtra")
    MembreService toEntity(MembreServiceDTO membreServiceDTO);

    default MembreService fromId(Long id) {
        if (id == null) {
            return null;
        }
        MembreService membreService = new MembreService();
        membreService.setId(id);
        return membreService;
    }
}
