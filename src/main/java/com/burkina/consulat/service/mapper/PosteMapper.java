package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.PosteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Poste and its DTO PosteDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PosteMapper extends EntityMapper<PosteDTO, Poste> {

    

    @Mapping(target = "infoOccupations", ignore = true)
    Poste toEntity(PosteDTO posteDTO);

    default Poste fromId(Long id) {
        if (id == null) {
            return null;
        }
        Poste poste = new Poste();
        poste.setId(id);
        return poste;
    }
}
