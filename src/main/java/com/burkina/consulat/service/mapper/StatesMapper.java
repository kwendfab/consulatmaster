package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.StatesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity States and its DTO StatesDTO.
 */
@Mapper(componentModel = "spring", uses = {CountryMapper.class})
public interface StatesMapper extends EntityMapper<StatesDTO, States> {

    @Mapping(source = "country.id", target = "countryId")
    @Mapping(source = "country.libele", target = "countryLibelle")
    StatesDTO toDto(States states);

    @Mapping(target = "cities", ignore = true)
    @Mapping(source = "countryId", target = "country")
    States toEntity(StatesDTO statesDTO);

    default States fromId(Long id) {
        if (id == null) {
            return null;
        }
        States states = new States();
        states.setId(id);
        return states;
    }
}
