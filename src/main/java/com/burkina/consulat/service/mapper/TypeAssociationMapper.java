package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.TypeAssociationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TypeAssociation} and its DTO {@link TypeAssociationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TypeAssociationMapper extends EntityMapper<TypeAssociationDTO, TypeAssociation> {



    default TypeAssociation fromId(Long id) {
        if (id == null) {
            return null;
        }
        TypeAssociation typeAssociation = new TypeAssociation();
        typeAssociation.setId(id);
        return typeAssociation;
    }
}
