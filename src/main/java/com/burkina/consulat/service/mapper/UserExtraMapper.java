package com.burkina.consulat.service.mapper;

import com.burkina.consulat.domain.*;
import com.burkina.consulat.service.dto.UserExtraDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserExtra} and its DTO {@link UserExtraDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ProfilMapper.class, MembersMapper.class, ConsulatMapper.class})
public interface UserExtraMapper extends EntityMapper<UserExtraDTO, UserExtra> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "profil.id", target = "profilId")
    @Mapping(source = "members.id", target = "membersId")
    @Mapping(source = "consulat.id", target = "consulatId")
    @Mapping(source = "consulat.nom", target = "consulatNom")
    @Mapping(source = "profil.nom", target = "profilNom")

    UserExtraDTO toDto(UserExtra userExtra);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "profilId", target = "profil")
    @Mapping(source = "membersId", target = "members")
    @Mapping(source = "consulatId", target = "consulat")
    UserExtra toEntity(UserExtraDTO userExtraDTO);

    default UserExtra fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserExtra userExtra = new UserExtra();
        userExtra.setId(id);
        return userExtra;
    }
}
