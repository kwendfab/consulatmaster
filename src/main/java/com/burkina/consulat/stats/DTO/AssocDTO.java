package com.burkina.consulat.stats.DTO;

public class AssocDTO {
    private String[] types;
    private Integer[] data;

    public AssocDTO(Integer n) {
        this.types=new String[n];
        this.data=new Integer[n];
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    public Integer[] getData() {
        return data;
    }

    public void setData(Integer[] data) {
        this.data = data;
    }
}
