package com.burkina.consulat.stats.DTO;

public class EtatMembreDTO {
    private String[] etats;
    private Integer[] data;

    public EtatMembreDTO(Integer n) {
        this.etats=new String[n];
        this.data=new Integer[n];
    }

    public String[] getEtats() {
        return etats;
    }

    public void setEtats(String[] etats) {
        this.etats = etats;
    }

    public Integer[] getData() {
        return data;
    }

    public void setData(Integer[] data) {
        this.data = data;
    }
}
