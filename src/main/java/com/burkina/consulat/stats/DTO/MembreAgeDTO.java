package com.burkina.consulat.stats.DTO;

public class MembreAgeDTO{


    private String[] age;
    private Integer[] data;

    public MembreAgeDTO(Integer n) {
        this.age=new String[n];
        this.data=new Integer[n];
    }

    public String[] getAge() {
        return age;
    }

    public void setAge(String[] age) {
        this.age = age;
    }

    public Integer[] getData() {
        return data;
    }

    public void setData(Integer[] data) {
        this.data = data;
    }
}
