package com.burkina.consulat.stats.DTO;

public class MembreDTO {
    private String[] etats = {"TOTAL", "MALE", "FEMALE"};
    private Integer[] data = {0, 0, 0};

    public String[] getEtats() {
        return etats;
    }

    public Integer[] getData() {
        return data;
    }
}
