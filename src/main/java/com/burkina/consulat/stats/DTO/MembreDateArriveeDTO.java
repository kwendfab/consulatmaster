package com.burkina.consulat.stats.DTO;

public class MembreDateArriveeDTO {

    private String[] total;
    private Integer[] data;

    public MembreDateArriveeDTO(Integer n) {
        this.total=new String[n];
        this.data=new Integer[n];
    }

    public String[] getTotal() {
        return total;
    }

    public void setTotal(String[] total) {
        this.total = total;
    }

    public Integer[] getData() {
        return data;
    }

    public void setData(Integer[] data) {
        this.data = data;
    }
}
