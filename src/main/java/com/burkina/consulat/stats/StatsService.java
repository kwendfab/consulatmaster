package com.burkina.consulat.stats;

import com.burkina.consulat.domain.enumeration.Sexe;
import com.burkina.consulat.repository.*;
import com.burkina.consulat.stats.DTO.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;


@Service
@Transactional
public class StatsService {
    private final MembersRepository membersRepository;

    private final AssociationRepository associationRepository;

    private final InfoAssociationRepository infoAssociationRepository;

    private final StatesRepository statesRepository;

    private final AdressesRepository adressesRepository;


    public StatsService(MembersRepository membersRepository, AssociationRepository associationRepository, InfoAssociationRepository infoAssociationRepository, StatesRepository statesRepository, AdressesRepository adressesRepository) {


        this.membersRepository = membersRepository;
        this.associationRepository = associationRepository;
        this.infoAssociationRepository = infoAssociationRepository;
        this.statesRepository = statesRepository;
        this.adressesRepository = adressesRepository;
    }


    /**
     * Calcul et retourne le nombre de membre par sexe.
     *
     * @return
     */
    public MembreDTO getMembreSexe() {
        MembreDTO membreDTO = new MembreDTO();
        membreDTO.getData()[0]=this.membersRepository.countAllBy();
        membreDTO.getData()[1]=this.membersRepository.countMembreSexe(Sexe.MALE);
        membreDTO.getData()[2]=this.membersRepository.countMembreSexe(Sexe.FEMALE);
        return membreDTO;
    }


    /**
     * Calcul et retourne le nombre de membre par association.
     *
     * @return
     */
    public AssocDTO getAssocByType() {
        String tab[] = this.associationRepository.TabOfAssoc();
        AssocDTO assocDTO = new AssocDTO(tab.length);
        for (int i = 0; i < tab.length; i++) {
            assocDTO.getTypes()[i] = tab[i];
        }

        for (int i = 0; i < assocDTO.getTypes().length; i++) {
            if (this.infoAssociationRepository.countInfoAssociation(assocDTO.getTypes()[i]) != null) {
                Integer n = 0;
                n = this.infoAssociationRepository.countInfoAssociation(assocDTO.getTypes()[i]);

                assocDTO.getData()[i] = n;
            } else {
                assocDTO.getData()[i] = 0;
            }
        }
        return assocDTO;
    }








    /**
     * Calcul et retourne le nombre de membre par etat.
     *
     * @return
     */
    public EtatMembreDTO getEtatMembre() {
        String tab[] = this.statesRepository.TabOfEtat();
        EtatMembreDTO etatMembreDTO = new EtatMembreDTO(tab.length);
        for (int i = 0; i < tab.length; i++) {
            etatMembreDTO.getEtats()[i] = tab[i];
        }

        for (int i = 0; i < etatMembreDTO.getEtats().length; i++) {
            if (this.adressesRepository.countEtatMembre(etatMembreDTO.getEtats()[i]) != null) {
                Integer n = 0;
                n = this.adressesRepository.countEtatMembre(etatMembreDTO.getEtats()[i]);

                etatMembreDTO.getData()[i] = n;
            } else {
                etatMembreDTO.getData()[i] = 0;
            }
        }
        return etatMembreDTO;
    }





    /**
     * Calcul et retourne le nombre de membre par tranche d'age.
     *
     * @return
     */


    public MembreAgeDTO getEtatMembreByDateNaissance() {

        String tab[] = {"DE 0 à 10","DE 10 à 20","DE 20 à 30","DE 30 à 40","DE 40 à 50","DE 50 à 60","DE 60 à 70","80 ET PLUS"};
        LocalDate dateJour = LocalDate.now();
        MembreAgeDTO membreDTO = new MembreAgeDTO(tab.length);
        for (int i = 0; i < tab.length; i++) {
            membreDTO.getAge()[i] = tab[i];
        }



        for (int i = 1; i <= membreDTO.getAge().length; i++) {

            LocalDate datedebut = dateJour.minusYears(i*10);
            LocalDate dateFin = dateJour.minusYears((i-1)*10);

            if(membreDTO.getAge().length==i)
            {
                datedebut = datedebut.minusYears(50);
                if (this.membersRepository.countByDateNaissance(datedebut,dateFin) != null) {
                    Integer n = 0;
                    n = this.membersRepository.countByDateNaissance(datedebut,dateFin);

                    membreDTO.getData()[i - 1] = n;
                } else {
                    membreDTO.getData()[i - 1] = 0;
                }

            }
            else {
                if (this.membersRepository.countByDateNaissance(datedebut, dateFin) != null) {
                    Integer n = 0;
                    n = this.membersRepository.countByDateNaissance(datedebut, dateFin);

                    membreDTO.getData()[i - 1] = n;
                } else {
                    membreDTO.getData()[i - 1] = 0;
                }
            }
        }

        return membreDTO;
    }


    /**
     * Calcul et retourne le nombre de membre par date d'arrivee.
     *
     * @return
     */
    public MembreDateArriveeDTO getEtatMembreByDateArrivee(LocalDate debut,LocalDate fin) {
        String tab[] = {"TOTAL_ARRIVEES_SUR_LA_PERIODE"};
        MembreDateArriveeDTO membreDTO = new MembreDateArriveeDTO(tab.length);
        membreDTO.getTotal()[0] = tab[0];

        int n = this.membersRepository.countByDateArrivee(debut,fin);

        membreDTO.getData()[0] = n;
        return membreDTO;
    }





}
