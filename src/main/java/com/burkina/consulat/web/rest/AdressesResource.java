package com.burkina.consulat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.AdressesService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.AdressesDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Adresses.
 */
@RestController
@RequestMapping("/api")
public class AdressesResource {

    private final Logger log = LoggerFactory.getLogger(AdressesResource.class);

    private static final String ENTITY_NAME = "adresses";

    private final AdressesService adressesService;

    public AdressesResource(AdressesService adressesService) {
        this.adressesService = adressesService;
    }

    /**
     * POST  /adresses : Create a new adresses.
     *
     * @param adressesDTO the adressesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new adressesDTO, or with status 400 (Bad Request) if the adresses has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/adresses")
    @Timed
    public ResponseEntity<AdressesDTO> createAdresses(@RequestBody AdressesDTO adressesDTO) throws URISyntaxException {
        log.debug("REST request to save Adresses : {}", adressesDTO);
        if (adressesDTO.getId() != null) {
            throw new BadRequestAlertException("A new adresses cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdressesDTO result = adressesService.save(adressesDTO);
        return ResponseEntity.created(new URI("/api/adresses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /adresses : Updates an existing adresses.
     *
     * @param adressesDTO the adressesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated adressesDTO,
     * or with status 400 (Bad Request) if the adressesDTO is not valid,
     * or with status 500 (Internal Server Error) if the adressesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/adresses")
    @Timed
    public ResponseEntity<AdressesDTO> updateAdresses(@RequestBody AdressesDTO adressesDTO) throws URISyntaxException {
        log.debug("REST request to update Adresses : {}", adressesDTO);
        if (adressesDTO.getId() == null) {
            return createAdresses(adressesDTO);
        }
        AdressesDTO result = adressesService.save(adressesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, adressesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /adresses : get all the adresses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of adresses in body
     */
    @GetMapping("/adresses")
    @Timed
    public ResponseEntity<List<AdressesDTO>> getAllAdresses(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Adresses");
        Page<AdressesDTO> page = adressesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/adresses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /adresses/:id : get the "id" adresses.
     *
     * @param id the id of the adressesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the adressesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/adresses/{id}")
    @Timed
    public ResponseEntity<AdressesDTO> getAdresses(@PathVariable Long id) {
        log.debug("REST request to get Adresses : {}", id);
        AdressesDTO adressesDTO = adressesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(adressesDTO));
    }


    /**
     * GET  /members : get all the members.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of members in body
     */
    @GetMapping("/adresses/findByParams")
    @Timed
    public ResponseEntity<List<AdressesDTO>> getByParams(@ApiParam Pageable pageable, @RequestParam(required = false)String prenom, @RequestParam(required = false)String nom) {
        log.debug("REST request to get a page of Members");
        Page<AdressesDTO> page = adressesService.findByParams(nom,prenom,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/adresses/");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }









    @GetMapping("/adresses/member/{id}")
    @Timed
    public ResponseEntity<AdressesDTO> getAdressesByMember(@PathVariable Long id) {
        AdressesDTO adressesDTO = adressesService.findByMemberId(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(adressesDTO));
    }






    /**
     * DELETE  /adresses/:id : delete the "id" adresses.
     *
     * @param id the id of the adressesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/adresses/{id}")
    @Timed
    public ResponseEntity<Void> deleteAdresses(@PathVariable Long id) {
        log.debug("REST request to delete Adresses : {}", id);
        adressesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
