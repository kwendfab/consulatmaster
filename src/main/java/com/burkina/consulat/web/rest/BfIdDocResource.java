package com.burkina.consulat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.BfIdDocService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.BfIdDocDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BfIdDoc.
 */
@RestController
@RequestMapping("/api")
public class BfIdDocResource {

    private final Logger log = LoggerFactory.getLogger(BfIdDocResource.class);

    private static final String ENTITY_NAME = "bfIdDoc";

    private final BfIdDocService bfIdDocService;

    public BfIdDocResource(BfIdDocService bfIdDocService) {
        this.bfIdDocService = bfIdDocService;
    }

    /**
     * POST  /bf-id-docs : Create a new bfIdDoc.
     *
     * @param bfIdDocDTO the bfIdDocDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bfIdDocDTO, or with status 400 (Bad Request) if the bfIdDoc has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bf-id-docs")
    @Timed
    public ResponseEntity<BfIdDocDTO> createBfIdDoc(@Valid @RequestBody BfIdDocDTO bfIdDocDTO) throws URISyntaxException {
        log.debug("REST request to save BfIdDoc : {}", bfIdDocDTO);
        if (bfIdDocDTO.getId() != null) {
            throw new BadRequestAlertException("A new bfIdDoc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BfIdDocDTO result = bfIdDocService.save(bfIdDocDTO);
        return ResponseEntity.created(new URI("/api/bf-id-docs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bf-id-docs : Updates an existing bfIdDoc.
     *
     * @param bfIdDocDTO the bfIdDocDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bfIdDocDTO,
     * or with status 400 (Bad Request) if the bfIdDocDTO is not valid,
     * or with status 500 (Internal Server Error) if the bfIdDocDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bf-id-docs")
    @Timed
    public ResponseEntity<BfIdDocDTO> updateBfIdDoc(@Valid @RequestBody BfIdDocDTO bfIdDocDTO) throws URISyntaxException {
        log.debug("REST request to update BfIdDoc : {}", bfIdDocDTO);
        if (bfIdDocDTO.getId() == null) {
            return createBfIdDoc(bfIdDocDTO);
        }
        BfIdDocDTO result = bfIdDocService.save(bfIdDocDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bfIdDocDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bf-id-docs : get all the bfIdDocs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bfIdDocs in body
     */
    @GetMapping("/bf-id-docs")
    @Timed
    public ResponseEntity<List<BfIdDocDTO>> getAllBfIdDocs(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of BfIdDocs");
        Page<BfIdDocDTO> page = bfIdDocService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bf-id-docs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /bf-id-docs/:id : get the "id" bfIdDoc.
     *
     * @param id the id of the bfIdDocDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bfIdDocDTO, or with status 404 (Not Found)
     */
    @GetMapping("/bf-id-docs/{id}")
    @Timed
    public ResponseEntity<BfIdDocDTO> getBfIdDoc(@PathVariable Long id) {
        log.debug("REST request to get BfIdDoc : {}", id);
        BfIdDocDTO bfIdDocDTO = bfIdDocService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bfIdDocDTO));
    }

    /**
     * DELETE  /bf-id-docs/:id : delete the "id" bfIdDoc.
     *
     * @param id the id of the bfIdDocDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bf-id-docs/{id}")
    @Timed
    public ResponseEntity<Void> deleteBfIdDoc(@PathVariable Long id) {
        log.debug("REST request to delete BfIdDoc : {}", id);
        bfIdDocService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
