package com.burkina.consulat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.ConsulatService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.ConsulatDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Consulat.
 */
@RestController
@RequestMapping("/api")
public class ConsulatResource {

    private final Logger log = LoggerFactory.getLogger(ConsulatResource.class);

    private static final String ENTITY_NAME = "consulat";

    private final ConsulatService consulatService;

    public ConsulatResource(ConsulatService consulatService) {
        this.consulatService = consulatService;
    }

    /**
     * POST  /consulats : Create a new consulat.
     *
     * @param consulatDTO the consulatDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new consulatDTO, or with status 400 (Bad Request) if the consulat has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/consulats")
    @Timed
    public ResponseEntity<ConsulatDTO> createConsulat(@Valid @RequestBody ConsulatDTO consulatDTO) throws URISyntaxException {
        log.debug("REST request to save Consulat : {}", consulatDTO);
        if (consulatDTO.getId() != null) {
            throw new BadRequestAlertException("A new consulat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConsulatDTO result = consulatService.save(consulatDTO);
        return ResponseEntity.created(new URI("/api/consulats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /consulats : Updates an existing consulat.
     *
     * @param consulatDTO the consulatDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated consulatDTO,
     * or with status 400 (Bad Request) if the consulatDTO is not valid,
     * or with status 500 (Internal Server Error) if the consulatDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/consulats")
    @Timed
    public ResponseEntity<ConsulatDTO> updateConsulat(@Valid @RequestBody ConsulatDTO consulatDTO) throws URISyntaxException {
        log.debug("REST request to update Consulat : {}", consulatDTO);
        if (consulatDTO.getId() == null) {
            return createConsulat(consulatDTO);
        }
        ConsulatDTO result = consulatService.save(consulatDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, consulatDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /consulats : get all the consulats.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of consulats in body
     */
    @GetMapping("/consulats")
    @Timed
    public ResponseEntity<List<ConsulatDTO>> getAllConsulats(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Consulats");
        Page<ConsulatDTO> page = consulatService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/consulats");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /consulats/:id : get the "id" consulat.
     *
     * @param id the id of the consulatDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the consulatDTO, or with status 404 (Not Found)
     */
    @GetMapping("/consulats/{id}")
    @Timed
    public ResponseEntity<ConsulatDTO> getConsulat(@PathVariable Long id) {
        log.debug("REST request to get Consulat : {}", id);
        ConsulatDTO consulatDTO = consulatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(consulatDTO));
    }

    /**
     * DELETE  /consulats/:id : delete the "id" consulat.
     *
     * @param id the id of the consulatDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/consulats/{id}")
    @Timed
    public ResponseEntity<Void> deleteConsulat(@PathVariable Long id) {
        log.debug("REST request to delete Consulat : {}", id);
        consulatService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
