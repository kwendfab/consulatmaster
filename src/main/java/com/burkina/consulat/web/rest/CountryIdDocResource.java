package com.burkina.consulat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.CountryIdDocService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.CountryIdDocDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CountryIdDoc.
 */
@RestController
@RequestMapping("/api")
public class CountryIdDocResource {

    private final Logger log = LoggerFactory.getLogger(CountryIdDocResource.class);

    private static final String ENTITY_NAME = "countryIdDoc";

    private final CountryIdDocService countryIdDocService;

    public CountryIdDocResource(CountryIdDocService countryIdDocService) {
        this.countryIdDocService = countryIdDocService;
    }

    /**
     * POST  /country-id-docs : Create a new countryIdDoc.
     *
     * @param countryIdDocDTO the countryIdDocDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new countryIdDocDTO, or with status 400 (Bad Request) if the countryIdDoc has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/country-id-docs")
    @Timed
    public ResponseEntity<CountryIdDocDTO> createCountryIdDoc(@Valid @RequestBody CountryIdDocDTO countryIdDocDTO) throws URISyntaxException {
        log.debug("REST request to save CountryIdDoc : {}", countryIdDocDTO);
        if (countryIdDocDTO.getId() != null) {
            throw new BadRequestAlertException("A new countryIdDoc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CountryIdDocDTO result = countryIdDocService.save(countryIdDocDTO);
        return ResponseEntity.created(new URI("/api/country-id-docs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /country-id-docs : Updates an existing countryIdDoc.
     *
     * @param countryIdDocDTO the countryIdDocDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated countryIdDocDTO,
     * or with status 400 (Bad Request) if the countryIdDocDTO is not valid,
     * or with status 500 (Internal Server Error) if the countryIdDocDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/country-id-docs")
    @Timed
    public ResponseEntity<CountryIdDocDTO> updateCountryIdDoc(@Valid @RequestBody CountryIdDocDTO countryIdDocDTO) throws URISyntaxException {
        log.debug("REST request to update CountryIdDoc : {}", countryIdDocDTO);
        if (countryIdDocDTO.getId() == null) {
            return createCountryIdDoc(countryIdDocDTO);
        }
        CountryIdDocDTO result = countryIdDocService.save(countryIdDocDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, countryIdDocDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /country-id-docs : get all the countryIdDocs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of countryIdDocs in body
     */
    @GetMapping("/country-id-docs")
    @Timed
    public ResponseEntity<List<CountryIdDocDTO>> getAllCountryIdDocs(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of CountryIdDocs");
        Page<CountryIdDocDTO> page = countryIdDocService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/country-id-docs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /country-id-docs/:id : get the "id" countryIdDoc.
     *
     * @param id the id of the countryIdDocDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the countryIdDocDTO, or with status 404 (Not Found)
     */
    @GetMapping("/country-id-docs/{id}")
    @Timed
    public ResponseEntity<CountryIdDocDTO> getCountryIdDoc(@PathVariable Long id) {
        log.debug("REST request to get CountryIdDoc : {}", id);
        CountryIdDocDTO countryIdDocDTO = countryIdDocService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(countryIdDocDTO));
    }

    /**
     * DELETE  /country-id-docs/:id : delete the "id" countryIdDoc.
     *
     * @param id the id of the countryIdDocDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/country-id-docs/{id}")
    @Timed
    public ResponseEntity<Void> deleteCountryIdDoc(@PathVariable Long id) {
        log.debug("REST request to delete CountryIdDoc : {}", id);
        countryIdDocService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
