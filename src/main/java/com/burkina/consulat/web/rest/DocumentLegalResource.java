package com.burkina.consulat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.DocumentLegalService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.DocumentLegalDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DocumentLegal.
 */
@RestController
@RequestMapping("/api")
public class DocumentLegalResource {

    private final Logger log = LoggerFactory.getLogger(DocumentLegalResource.class);

    private static final String ENTITY_NAME = "documentLegal";

    private final DocumentLegalService documentLegalService;

    public DocumentLegalResource(DocumentLegalService documentLegalService) {
        this.documentLegalService = documentLegalService;
    }

    /**
     * POST  /document-legals : Create a new documentLegal.
     *
     * @param documentLegalDTO the documentLegalDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new documentLegalDTO, or with status 400 (Bad Request) if the documentLegal has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/document-legals")
    @Timed
    public ResponseEntity<DocumentLegalDTO> createDocumentLegal(@Valid @RequestBody DocumentLegalDTO documentLegalDTO) throws URISyntaxException {
        log.debug("REST request to save DocumentLegal : {}", documentLegalDTO);
        if (documentLegalDTO.getId() != null) {
            throw new BadRequestAlertException("A new documentLegal cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DocumentLegalDTO result = documentLegalService.save(documentLegalDTO);
        return ResponseEntity.created(new URI("/api/document-legals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /document-legals : Updates an existing documentLegal.
     *
     * @param documentLegalDTO the documentLegalDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated documentLegalDTO,
     * or with status 400 (Bad Request) if the documentLegalDTO is not valid,
     * or with status 500 (Internal Server Error) if the documentLegalDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/document-legals")
    @Timed
    public ResponseEntity<DocumentLegalDTO> updateDocumentLegal(@Valid @RequestBody DocumentLegalDTO documentLegalDTO) throws URISyntaxException {
        log.debug("REST request to update DocumentLegal : {}", documentLegalDTO);
        if (documentLegalDTO.getId() == null) {
            return createDocumentLegal(documentLegalDTO);
        }
        DocumentLegalDTO result = documentLegalService.save(documentLegalDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, documentLegalDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /document-legals : get all the documentLegals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of documentLegals in body
     */
    @GetMapping("/document-legals")
    @Timed
    public ResponseEntity<List<DocumentLegalDTO>> getAllDocumentLegals(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DocumentLegals");
        Page<DocumentLegalDTO> page = documentLegalService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/document-legals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /document-legals/:id : get the "id" documentLegal.
     *
     * @param id the id of the documentLegalDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the documentLegalDTO, or with status 404 (Not Found)
     */
    @GetMapping("/document-legals/{id}")
    @Timed
    public ResponseEntity<DocumentLegalDTO> getDocumentLegal(@PathVariable Long id) {
        log.debug("REST request to get DocumentLegal : {}", id);
        DocumentLegalDTO documentLegalDTO = documentLegalService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(documentLegalDTO));
    }

    /**
     * DELETE  /document-legals/:id : delete the "id" documentLegal.
     *
     * @param id the id of the documentLegalDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/document-legals/{id}")
    @Timed
    public ResponseEntity<Void> deleteDocumentLegal(@PathVariable Long id) {
        log.debug("REST request to delete DocumentLegal : {}", id);
        documentLegalService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
