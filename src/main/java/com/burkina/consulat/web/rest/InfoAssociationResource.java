package com.burkina.consulat.web.rest;

import com.burkina.consulat.domain.Association;
import com.burkina.consulat.service.dto.AssociationDTO;
import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.InfoAssociationService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.InfoAssociationDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InfoAssociation.
 */
@RestController
@RequestMapping("/api")
public class InfoAssociationResource {

    private final Logger log = LoggerFactory.getLogger(InfoAssociationResource.class);

    private static final String ENTITY_NAME = "infoAssociation";

    private final InfoAssociationService infoAssociationService;

    public InfoAssociationResource(InfoAssociationService infoAssociationService) {
        this.infoAssociationService = infoAssociationService;
    }

    /**
     * POST  /info-associations : Create a new infoAssociation.
     *
     * @param infoAssociationDTO the infoAssociationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new infoAssociationDTO, or with status 400 (Bad Request) if the infoAssociation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/info-associations")
    @Timed
    public ResponseEntity<InfoAssociationDTO> createInfoAssociation(@RequestBody InfoAssociationDTO infoAssociationDTO) throws URISyntaxException {
        log.debug("REST request to save InfoAssociation : {}", infoAssociationDTO);
        if (infoAssociationDTO.getId() != null) {
            throw new BadRequestAlertException("A new infoAssociation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InfoAssociationDTO result = infoAssociationService.save(infoAssociationDTO);
        return ResponseEntity.created(new URI("/api/info-associations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /info-associations : Updates an existing infoAssociation.
     *
     * @param infoAssociationDTO the infoAssociationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated infoAssociationDTO,
     * or with status 400 (Bad Request) if the infoAssociationDTO is not valid,
     * or with status 500 (Internal Server Error) if the infoAssociationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/info-associations")
    @Timed
    public ResponseEntity<InfoAssociationDTO> updateInfoAssociation(@RequestBody InfoAssociationDTO infoAssociationDTO) throws URISyntaxException {
        log.debug("REST request to update InfoAssociation : {}", infoAssociationDTO);
        if (infoAssociationDTO.getId() == null) {
            return createInfoAssociation(infoAssociationDTO);
        }
        InfoAssociationDTO result = infoAssociationService.save(infoAssociationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, infoAssociationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /info-associations : get all the infoAssociations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of infoAssociations in body
     */
    @GetMapping("/info-associations")
    @Timed
    public ResponseEntity<List<InfoAssociationDTO>> getAllInfoAssociations(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of InfoAssociations");
        Page<InfoAssociationDTO> page = infoAssociationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/info-associations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /info-associations/:id : get the "id" infoAssociation.
     *
     * @param id the id of the infoAssociationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the infoAssociationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/info-associations/{id}")
    @Timed
    public ResponseEntity<InfoAssociationDTO> getInfoAssociation(@PathVariable Long id) {
        log.debug("REST request to get InfoAssociation : {}", id);
        InfoAssociationDTO infoAssociationDTO = infoAssociationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(infoAssociationDTO));
    }





    @GetMapping("/info-associations/association/{id}")
    @Timed
    public List<AssociationDTO> getAssociationForMember(@PathVariable Long id) {
        List<AssociationDTO>associationList = infoAssociationService.findByMembers(id);
        return associationList;
    }


    /**
     * DELETE  /info-associations/:id : delete the "id" infoAssociation.
     *
     * @param id the id of the infoAssociationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/info-associations/{id}")
    @Timed
    public ResponseEntity<Void> deleteInfoAssociation(@PathVariable Long id) {
        log.debug("REST request to delete InfoAssociation : {}", id);
        infoAssociationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
