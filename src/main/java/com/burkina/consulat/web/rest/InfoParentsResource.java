package com.burkina.consulat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.InfoParentsService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.InfoParentsDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InfoParents.
 */
@RestController
@RequestMapping("/api")
public class InfoParentsResource {

    private final Logger log = LoggerFactory.getLogger(InfoParentsResource.class);

    private static final String ENTITY_NAME = "infoParents";

    private final InfoParentsService infoParentsService;

    public InfoParentsResource(InfoParentsService infoParentsService) {
        this.infoParentsService = infoParentsService;
    }

    /**
     * POST  /info-parents : Create a new infoParents.
     *
     * @param infoParentsDTO the infoParentsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new infoParentsDTO, or with status 400 (Bad Request) if the infoParents has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/info-parents")
    @Timed
    public ResponseEntity<InfoParentsDTO> createInfoParents(@RequestBody InfoParentsDTO infoParentsDTO) throws URISyntaxException {
        log.debug("REST request to save InfoParents : {}", infoParentsDTO);
        if (infoParentsDTO.getId() != null) {
            throw new BadRequestAlertException("A new infoParents cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InfoParentsDTO result = infoParentsService.save(infoParentsDTO);
        return ResponseEntity.created(new URI("/api/info-parents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /info-parents : Updates an existing infoParents.
     *
     * @param infoParentsDTO the infoParentsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated infoParentsDTO,
     * or with status 400 (Bad Request) if the infoParentsDTO is not valid,
     * or with status 500 (Internal Server Error) if the infoParentsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/info-parents")
    @Timed
    public ResponseEntity<InfoParentsDTO> updateInfoParents(@RequestBody InfoParentsDTO infoParentsDTO) throws URISyntaxException {
        log.debug("REST request to update InfoParents : {}", infoParentsDTO);
        if (infoParentsDTO.getId() == null) {
            return createInfoParents(infoParentsDTO);
        }
        InfoParentsDTO result = infoParentsService.save(infoParentsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, infoParentsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /info-parents : get all the infoParents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of infoParents in body
     */
    @GetMapping("/info-parents")
    @Timed
    public ResponseEntity<List<InfoParentsDTO>> getAllInfoParents(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of InfoParents");
        Page<InfoParentsDTO> page = infoParentsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/info-parents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /info-parents/:id : get the "id" infoParents.
     *
     * @param id the id of the infoParentsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the infoParentsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/info-parents/{id}")
    @Timed
    public ResponseEntity<InfoParentsDTO> getInfoParents(@PathVariable Long id) {
        log.debug("REST request to get InfoParents : {}", id);
        InfoParentsDTO infoParentsDTO = infoParentsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(infoParentsDTO));
    }

    /**
     * DELETE  /info-parents/:id : delete the "id" infoParents.
     *
     * @param id the id of the infoParentsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/info-parents/{id}")
    @Timed
    public ResponseEntity<Void> deleteInfoParents(@PathVariable Long id) {
        log.debug("REST request to delete InfoParents : {}", id);
        infoParentsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
