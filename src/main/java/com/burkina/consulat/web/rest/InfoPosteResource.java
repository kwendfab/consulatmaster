package com.burkina.consulat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.InfoPosteService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.InfoPosteDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InfoPoste.
 */
@RestController
@RequestMapping("/api")
public class InfoPosteResource {

    private final Logger log = LoggerFactory.getLogger(InfoPosteResource.class);

    private static final String ENTITY_NAME = "infoPoste";

    private final InfoPosteService infoPosteService;

    public InfoPosteResource(InfoPosteService infoPosteService) {
        this.infoPosteService = infoPosteService;
    }

    /**
     * POST  /info-postes : Create a new infoPoste.
     *
     * @param infoPosteDTO the infoPosteDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new infoPosteDTO, or with status 400 (Bad Request) if the infoPoste has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/info-postes")
    @Timed
    public ResponseEntity<InfoPosteDTO> createInfoPoste(@RequestBody InfoPosteDTO infoPosteDTO) throws URISyntaxException {
        log.debug("REST request to save InfoPoste : {}", infoPosteDTO);
        if (infoPosteDTO.getId() != null) {
            throw new BadRequestAlertException("A new infoPoste cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InfoPosteDTO result = infoPosteService.save(infoPosteDTO);
        return ResponseEntity.created(new URI("/api/info-postes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /info-postes : Updates an existing infoPoste.
     *
     * @param infoPosteDTO the infoPosteDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated infoPosteDTO,
     * or with status 400 (Bad Request) if the infoPosteDTO is not valid,
     * or with status 500 (Internal Server Error) if the infoPosteDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/info-postes")
    @Timed
    public ResponseEntity<InfoPosteDTO> updateInfoPoste(@RequestBody InfoPosteDTO infoPosteDTO) throws URISyntaxException {
        log.debug("REST request to update InfoPoste : {}", infoPosteDTO);
        if (infoPosteDTO.getId() == null) {
            return createInfoPoste(infoPosteDTO);
        }
        InfoPosteDTO result = infoPosteService.save(infoPosteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, infoPosteDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /info-postes : get all the infoPostes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of infoPostes in body
     */
    @GetMapping("/info-postes")
    @Timed
    public ResponseEntity<List<InfoPosteDTO>> getAllInfoPostes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of InfoPostes");
        Page<InfoPosteDTO> page = infoPosteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/info-postes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /info-postes/:id : get the "id" infoPoste.
     *
     * @param id the id of the infoPosteDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the infoPosteDTO, or with status 404 (Not Found)
     */
    @GetMapping("/info-postes/{id}")
    @Timed
    public ResponseEntity<InfoPosteDTO> getInfoPoste(@PathVariable Long id) {
        log.debug("REST request to get InfoPoste : {}", id);
        InfoPosteDTO infoPosteDTO = infoPosteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(infoPosteDTO));
    }



    /**
     * GET  /info-postes/ByMember/:id : get the "id" infoPoste.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the infoPosteDTO, or with status 404 (Not Found)
     */
    @GetMapping("/info-postes/ByMember/{id}")
    @Timed
    public List<InfoPosteDTO> getByMemberId(@PathVariable Long id) {
        log.debug("REST request to get InfoPoste : {}", id);
        List<InfoPosteDTO> infoPosteDTO = infoPosteService.findByMemberId(id);
        return infoPosteDTO;
    }








    /**
     * DELETE  /info-postes/:id : delete the "id" infoPoste.
     *
     * @param id the id of the infoPosteDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/info-postes/{id}")
    @Timed
    public ResponseEntity<Void> deleteInfoPoste(@PathVariable Long id) {
        log.debug("REST request to delete InfoPoste : {}", id);
        infoPosteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
