package com.burkina.consulat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.InfoServiceService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.InfoServiceDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InfoService.
 */
@RestController
@RequestMapping("/api")
public class InfoServiceResource {

    private final Logger log = LoggerFactory.getLogger(InfoServiceResource.class);

    private static final String ENTITY_NAME = "infoService";

    private final InfoServiceService infoServiceService;

    public InfoServiceResource(InfoServiceService infoServiceService) {
        this.infoServiceService = infoServiceService;
    }

    /**
     * POST  /info-services : Create a new infoService.
     *
     * @param infoServiceDTO the infoServiceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new infoServiceDTO, or with status 400 (Bad Request) if the infoService has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/info-services")
    @Timed
    public ResponseEntity<InfoServiceDTO> createInfoService(@RequestBody InfoServiceDTO infoServiceDTO) throws URISyntaxException {
        log.debug("REST request to save InfoService : {}", infoServiceDTO);
        if (infoServiceDTO.getId() != null) {
            throw new BadRequestAlertException("A new infoService cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InfoServiceDTO result = infoServiceService.save(infoServiceDTO);
        return ResponseEntity.created(new URI("/api/info-services/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /info-services : Updates an existing infoService.
     *
     * @param infoServiceDTO the infoServiceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated infoServiceDTO,
     * or with status 400 (Bad Request) if the infoServiceDTO is not valid,
     * or with status 500 (Internal Server Error) if the infoServiceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/info-services")
    @Timed
    public ResponseEntity<InfoServiceDTO> updateInfoService(@RequestBody InfoServiceDTO infoServiceDTO) throws URISyntaxException {
        log.debug("REST request to update InfoService : {}", infoServiceDTO);
        if (infoServiceDTO.getId() == null) {
            return createInfoService(infoServiceDTO);
        }
        InfoServiceDTO result = infoServiceService.save(infoServiceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, infoServiceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /info-services : get all the infoServices.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of infoServices in body
     */
    @GetMapping("/info-services")
    @Timed
    public ResponseEntity<List<InfoServiceDTO>> getAllInfoServices(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of InfoServices");
        Page<InfoServiceDTO> page = infoServiceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/info-services");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /info-services/:id : get the "id" infoService.
     *
     * @param id the id of the infoServiceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the infoServiceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/info-services/{id}")
    @Timed
    public ResponseEntity<InfoServiceDTO> getInfoService(@PathVariable Long id) {
        log.debug("REST request to get InfoService : {}", id);
        InfoServiceDTO infoServiceDTO = infoServiceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(infoServiceDTO));
    }

    /**
     * DELETE  /info-services/:id : delete the "id" infoService.
     *
     * @param id the id of the infoServiceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/info-services/{id}")
    @Timed
    public ResponseEntity<Void> deleteInfoService(@PathVariable Long id) {
        log.debug("REST request to delete InfoService : {}", id);
        infoServiceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
