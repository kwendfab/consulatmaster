package com.burkina.consulat.web.rest;

import com.burkina.consulat.domain.enumeration.Sexe;
import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.MembersService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.MembersDTO;
import com.itextpdf.text.DocumentException;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.script.ScriptException;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Members.
 */
@RestController
@RequestMapping("/api")
public class MembersResource {

    private final Logger log = LoggerFactory.getLogger(MembersResource.class);

    private static final String ENTITY_NAME = "members";

    private final MembersService membersService;

    private final ApplicationContext appContext;

    public MembersResource(MembersService membersService, ApplicationContext appContext) {
        this.membersService = membersService;
        this.appContext = appContext;
    }

    /**
     * POST  /members : Create a new members.
     *
     * @param membersDTO the membersDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new membersDTO, or with status 400 (Bad Request) if the members has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/members")
    @Timed
    public ResponseEntity<MembersDTO> createMembers(@Valid @RequestBody MembersDTO membersDTO) throws URISyntaxException {
        log.debug("REST request to save Members : {}", membersDTO);
        if (membersDTO.getId() != null) {
            throw new BadRequestAlertException("A new members cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MembersDTO result = membersService.save(membersDTO);
        return ResponseEntity.created(new URI("/api/members/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /members : Updates an existing members.
     *
     * @param membersDTO the membersDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated membersDTO,
     * or with status 400 (Bad Request) if the membersDTO is not valid,
     * or with status 500 (Internal Server Error) if the membersDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/members")
    @Timed
    public ResponseEntity<MembersDTO> updateMembers(@Valid @RequestBody MembersDTO membersDTO) throws URISyntaxException {
        log.debug("REST request to update Members : {}", membersDTO);
        if (membersDTO.getId() == null) {
            return createMembers(membersDTO);
        }
        MembersDTO result = membersService.save(membersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, membersDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /members : get all the members.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of members in body
     */
    @GetMapping("/members")
    @Timed
    public ResponseEntity<List<MembersDTO>> getAllMembers(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Members");
        Page<MembersDTO> page = membersService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/members");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /membres/cartePDF : get the mariage.
     *
     * @param idMembre
     * @return the ResponseEntity with status 200 (OK) and the mariage in body
     * @throws java.net.URISyntaxException
     */
    @GetMapping("/members/cartePDF")
    @Timed
    public ModelAndView carte(@RequestParam(required = true) Long idMembre) throws ScriptException, URISyntaxException, IOException, DocumentException {
        return this.membersService.genererPdfMembre(appContext, idMembre);
    }



    /**
     * GET  /members : get all the members.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of members in body
     */
    @GetMapping("/members/findByParams")
    @Timed
    public ResponseEntity<List<MembersDTO>> getByParams(@ApiParam Pageable pageable, @RequestParam(required = false)String nom, @RequestParam(required = false)String prenom, @RequestParam(required = false)String numeroBf, @RequestParam(required = false)String numeroCountry, @RequestParam(required = false)LocalDate dateArrivee, @RequestParam(required = false)Boolean mariage, @RequestParam(required = false)Sexe sexe, @RequestParam(required = false)String metier, @RequestParam(required = false)String occupation, @RequestParam(required = false)Boolean parentsUsa, @RequestParam(required = false)Boolean mumInUsa, @RequestParam(required = false)String autorite, @RequestParam(required = false)String villeResidence, @RequestParam(required = false)String etatResidence) {
        log.debug("REST request to get a page of Members");
        Page<MembersDTO> page = membersService.findByParams(nom,prenom,numeroBf,numeroCountry,dateArrivee,mariage,sexe,metier,occupation,parentsUsa,mumInUsa,autorite,villeResidence,etatResidence,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/members/");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }






    /**
     * GET  /members/:id : get the "id" members.
     *
     * @param id the id of the membersDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the membersDTO, or with status 404 (Not Found)
     */
    @GetMapping("/members/{id}")
    @Timed
    public ResponseEntity<MembersDTO> getMembers(@PathVariable Long id) {
        log.debug("REST request to get Members : {}", id);
        MembersDTO membersDTO = membersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(membersDTO));
    }

    /**
     * DELETE  /members/:id : delete the "id" members.
     *
     * @param id the id of the membersDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/members/{id}")
    @Timed
    public ResponseEntity<Void> deleteMembers(@PathVariable Long id) {
        log.debug("REST request to delete Members : {}", id);
        membersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
