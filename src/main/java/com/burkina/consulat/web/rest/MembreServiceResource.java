package com.burkina.consulat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.MembreServiceService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.MembreServiceDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MembreService.
 */
@RestController
@RequestMapping("/api")
public class MembreServiceResource {

    private final Logger log = LoggerFactory.getLogger(MembreServiceResource.class);

    private static final String ENTITY_NAME = "membreService";

    private final MembreServiceService membreServiceService;

    public MembreServiceResource(MembreServiceService membreServiceService) {
        this.membreServiceService = membreServiceService;
    }

    /**
     * POST  /membre-services : Create a new membreService.
     *
     * @param membreServiceDTO the membreServiceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new membreServiceDTO, or with status 400 (Bad Request) if the membreService has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/membre-services")
    @Timed
    public ResponseEntity<MembreServiceDTO> createMembreService(@Valid @RequestBody MembreServiceDTO membreServiceDTO) throws URISyntaxException {
        log.debug("REST request to save MembreService : {}", membreServiceDTO);
        if (membreServiceDTO.getId() != null) {
            throw new BadRequestAlertException("A new membreService cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MembreServiceDTO result = membreServiceService.save(membreServiceDTO);
        return ResponseEntity.created(new URI("/api/membre-services/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /membre-services : Updates an existing membreService.
     *
     * @param membreServiceDTO the membreServiceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated membreServiceDTO,
     * or with status 400 (Bad Request) if the membreServiceDTO is not valid,
     * or with status 500 (Internal Server Error) if the membreServiceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/membre-services")
    @Timed
    public ResponseEntity<MembreServiceDTO> updateMembreService(@Valid @RequestBody MembreServiceDTO membreServiceDTO) throws URISyntaxException {
        log.debug("REST request to update MembreService : {}", membreServiceDTO);
        if (membreServiceDTO.getId() == null) {
            return createMembreService(membreServiceDTO);
        }
        MembreServiceDTO result = membreServiceService.save(membreServiceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, membreServiceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /membre-services : get all the membreServices.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of membreServices in body
     */
    @GetMapping("/membre-services")
    @Timed
    public ResponseEntity<List<MembreServiceDTO>> getAllMembreServices(@RequestParam(required = false)ZonedDateTime debut,@RequestParam(required = false)ZonedDateTime fin,@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of MembreServices");
        Page<MembreServiceDTO> page = membreServiceService.findAll(debut,fin,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/membre-services");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }






    @GetMapping("/membre-services/findTop10")
    @Timed
    public List<MembreServiceDTO>getAllMembreServicesTop10() {
        log.debug("REST request to get a page of MembreServices");
        List<MembreServiceDTO> list = membreServiceService.findTop10();
        return list;
    }

     @GetMapping("/membre-services/findTop10Even")
    @Timed
    public List<MembreServiceDTO>getAllMembreServicesTop10Even() {
        log.debug("REST request to get a page of MembreServices");
        List<MembreServiceDTO> list = membreServiceService.findTop10Even();
        return list;
    }











    /**
     * GET  /membre-services/:id : get the "id" membreService.
     *
     * @param id the id of the membreServiceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the membreServiceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/membre-services/{id}")
    @Timed
    public ResponseEntity<MembreServiceDTO> getMembreService(@PathVariable Long id) {
        log.debug("REST request to get MembreService : {}", id);
        MembreServiceDTO membreServiceDTO = membreServiceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(membreServiceDTO));
    }

    /**
     * DELETE  /membre-services/:id : delete the "id" membreService.
     *
     * @param id the id of the membreServiceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/membre-services/{id}")
    @Timed
    public ResponseEntity<Void> deleteMembreService(@PathVariable Long id) {
        log.debug("REST request to delete MembreService : {}", id);
        membreServiceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
