package com.burkina.consulat.web.rest;

import com.burkina.consulat.stats.DTO.*;
import com.burkina.consulat.stats.StatsService;
import io.github.jhipster.web.util.ResponseUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class StatRessource {
    private final Logger log = LoggerFactory.getLogger(StatRessource.class);
    private final StatsService statsService;

    public StatRessource(StatsService statsService) {
        this.statsService = statsService;
    }



        @GetMapping("/statistiques/membreSexe")
        public ResponseEntity<MembreDTO> getMembre() {
            return ResponseUtil.wrapOrNotFound(Optional.ofNullable(this.statsService.getMembreSexe()));
        }

    @GetMapping("/statistiques/assocMembers")
    public ResponseEntity<AssocDTO> getAssocMembers() {
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(this.statsService.getAssocByType()));
    }

    @GetMapping("/statistiques/etatMembers")
    public ResponseEntity<EtatMembreDTO> getEtatMembers() {
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(this.statsService.getEtatMembre()));
    }

    @GetMapping("/statistiques/dateArriveeMembers")
    public ResponseEntity<MembreDateArriveeDTO> getDateArriveeMembers(@RequestParam(required = false) ZonedDateTime debut, @RequestParam(required = false)  ZonedDateTime fin) {
        LocalDate periodeDebutDate =null;
        LocalDate periodeFinDate = null;
        if (debut != null && fin!= null) {
            periodeDebutDate = debut.toLocalDate();
            periodeFinDate = fin.toLocalDate();
        }else if (debut != null && fin== null){
            periodeDebutDate = debut.toLocalDate();
            periodeFinDate = LocalDate.now();
        } else  {
            periodeDebutDate = LocalDate.now().minusYears(50);
            periodeFinDate = LocalDate.now();
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(this.statsService.getEtatMembreByDateArrivee(periodeDebutDate,periodeFinDate)));
    }
    @GetMapping("/statistiques/dateNaissanceMembers")
    public ResponseEntity<MembreAgeDTO> getByDateNaissance() {
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(this.statsService.getEtatMembreByDateNaissance()));
    }


}
