package com.burkina.consulat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.burkina.consulat.service.StatesService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.web.rest.util.HeaderUtil;
import com.burkina.consulat.web.rest.util.PaginationUtil;
import com.burkina.consulat.service.dto.StatesDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing States.
 */
@RestController
@RequestMapping("/api")
public class StatesResource {

    private final Logger log = LoggerFactory.getLogger(StatesResource.class);

    private static final String ENTITY_NAME = "states";

    private final StatesService statesService;

    public StatesResource(StatesService statesService) {
        this.statesService = statesService;
    }

    /**
     * POST  /states : Create a new states.
     *
     * @param statesDTO the statesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new statesDTO, or with status 400 (Bad Request) if the states has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/states")
    @Timed
    public ResponseEntity<StatesDTO> createStates(@Valid @RequestBody StatesDTO statesDTO) throws URISyntaxException {
        log.debug("REST request to save States : {}", statesDTO);
        if (statesDTO.getId() != null) {
            throw new BadRequestAlertException("A new states cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StatesDTO result = statesService.save(statesDTO);
        return ResponseEntity.created(new URI("/api/states/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /states : Updates an existing states.
     *
     * @param statesDTO the statesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated statesDTO,
     * or with status 400 (Bad Request) if the statesDTO is not valid,
     * or with status 500 (Internal Server Error) if the statesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/states")
    @Timed
    public ResponseEntity<StatesDTO> updateStates(@Valid @RequestBody StatesDTO statesDTO) throws URISyntaxException {
        log.debug("REST request to update States : {}", statesDTO);
        if (statesDTO.getId() == null) {
            return createStates(statesDTO);
        }
        StatesDTO result = statesService.save(statesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, statesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /states : get all the states.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of states in body
     */
    @GetMapping("/states")
    @Timed
    public ResponseEntity<List<StatesDTO>> getAllStates(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of States");
        Page<StatesDTO> page = statesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/states");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /states/:id : get the "id" states.
     *
     * @param id the id of the statesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the statesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/states/{id}")
    @Timed
    public ResponseEntity<StatesDTO> getStates(@PathVariable Long id) {
        log.debug("REST request to get States : {}", id);
        StatesDTO statesDTO = statesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(statesDTO));
    }


    /**
     * GET  /cities/statesId:id : get the "id" city.
     *
     * @param id the id of the cityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/states/country/{id}")
    @Timed
    public List<StatesDTO> getBySatesId(@PathVariable Long id) {
        List<StatesDTO> statesDTOS = statesService.findCountryId(id);
        System.out.print("------------------------------------------------------------------");
        return statesDTOS;
    }

    /**
     * DELETE  /states/:id : delete the "id" states.
     *
     * @param id the id of the statesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/states/{id}")
    @Timed
    public ResponseEntity<Void> deleteStates(@PathVariable Long id) {
        log.debug("REST request to delete States : {}", id);
        statesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
