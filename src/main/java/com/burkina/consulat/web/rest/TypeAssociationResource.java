package com.burkina.consulat.web.rest;

import com.burkina.consulat.service.TypeAssociationService;
import com.burkina.consulat.web.rest.errors.BadRequestAlertException;
import com.burkina.consulat.service.dto.TypeAssociationDTO;

import com.burkina.consulat.web.rest.util.HeaderUtil;


import com.burkina.consulat.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;

import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.burkina.consulat.domain.TypeAssociation}.
 */
@RestController
@RequestMapping("/api")
public class TypeAssociationResource {

    private final Logger log = LoggerFactory.getLogger(TypeAssociationResource.class);

    private static final String ENTITY_NAME = "typeAssociation";

    private String applicationName;

    private final TypeAssociationService typeAssociationService;

    public TypeAssociationResource(TypeAssociationService typeAssociationService) {
        this.typeAssociationService = typeAssociationService;
    }

    /**
     * {@code POST  /type-associations} : Create a new typeAssociation.
     *
     * @param typeAssociationDTO the typeAssociationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new typeAssociationDTO, or with status {@code 400 (Bad Request)} if the typeAssociation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/type-associations")
    public ResponseEntity<TypeAssociationDTO> createTypeAssociation(@RequestBody TypeAssociationDTO typeAssociationDTO) throws URISyntaxException {
        log.debug("REST request to save TypeAssociation : {}", typeAssociationDTO);
        if (typeAssociationDTO.getId() != null) {
            throw new BadRequestAlertException("A new typeAssociation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TypeAssociationDTO result = typeAssociationService.save(typeAssociationDTO);
        return ResponseEntity.created(new URI("/api/type-associations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /type-associations} : Updates an existing typeAssociation.
     *
     * @param typeAssociationDTO the typeAssociationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typeAssociationDTO,
     * or with status {@code 400 (Bad Request)} if the typeAssociationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the typeAssociationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/type-associations")
    public ResponseEntity<TypeAssociationDTO> updateTypeAssociation(@RequestBody TypeAssociationDTO typeAssociationDTO) throws URISyntaxException {
        log.debug("REST request to update TypeAssociation : {}", typeAssociationDTO);
        if (typeAssociationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TypeAssociationDTO result = typeAssociationService.save(typeAssociationDTO);
        return ResponseEntity.created(new URI("/api/type-associations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /type-associations} : get all the typeAssociations.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of typeAssociations in body.
     */
    @GetMapping("/type-associations")
    public ResponseEntity<List<TypeAssociationDTO>> getAllTypeAssociations(Pageable pageable) {
        log.debug("REST request to get a page of TypeAssociations");
        Page<TypeAssociationDTO> page = typeAssociationService.findAll(pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/type-associations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

    }

    /**
     * {@code GET  /type-associations/:id} : get the "id" typeAssociation.
     *
     * @param id the id of the typeAssociationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the typeAssociationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/type-associations/{id}")
    public ResponseEntity<TypeAssociationDTO> getTypeAssociation(@PathVariable Long id) {
        log.debug("REST request to get TypeAssociation : {}", id);
        TypeAssociationDTO typeAssociationDTO = typeAssociationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(typeAssociationDTO));
    }

    /**
     * {@code DELETE  /type-associations/:id} : delete the "id" typeAssociation.
     *
     * @param id the id of the typeAssociationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/type-associations/{id}")
    public ResponseEntity<Void> deleteTypeAssociation(@PathVariable Long id) {
        log.debug("REST request to delete TypeAssociation : {}", id);
        typeAssociationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
