/**
 * View Models used by Spring MVC REST controllers.
 */
package com.burkina.consulat.web.rest.vm;
