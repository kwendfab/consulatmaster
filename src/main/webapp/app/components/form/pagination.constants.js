(function() {
    'use strict';

    angular
        .module('consulatApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
