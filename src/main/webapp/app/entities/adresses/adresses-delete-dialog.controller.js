(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('AdressesDeleteController',AdressesDeleteController);

    AdressesDeleteController.$inject = ['$uibModalInstance', 'entity', 'Adresses'];

    function AdressesDeleteController($uibModalInstance, entity, Adresses) {
        var vm = this;

        vm.adresses = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Adresses.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
