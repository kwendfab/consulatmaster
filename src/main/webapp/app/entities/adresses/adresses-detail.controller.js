(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('AdressesDetailController', AdressesDetailController);

    AdressesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Adresses', 'Members'];

    function AdressesDetailController($scope, $rootScope, $stateParams, previousState, entity, Adresses, Members) {
        var vm = this;

        vm.adresses = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('consulatApp:adressesUpdate', function(event, result) {
            vm.adresses = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
