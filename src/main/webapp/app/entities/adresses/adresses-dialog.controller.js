(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('AdressesDialogController', AdressesDialogController);

    AdressesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Adresses', 'Members'];

    function AdressesDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Adresses, Members) {
        var vm = this;

        vm.adresses = entity;
        vm.clear = clear;
        vm.save = save;
        vm.members = Members.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.adresses.id !== null) {
                Adresses.update(vm.adresses, onSaveSuccess, onSaveError);
            } else {
                Adresses.save(vm.adresses, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:adressesUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
