(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('adresses', {
            parent: 'entity',
            url: '/adresses?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Adresses'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/adresses/adresses.html',
                    controller: 'AdressesController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('adresses-detail', {
            parent: 'adresses',
            url: '/adresses/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Adresses'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/adresses/adresses-detail.html',
                    controller: 'AdressesDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Adresses', function($stateParams, Adresses) {
                    return Adresses.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'adresses',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('adresses-detail.edit', {
            parent: 'adresses-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/adresses/adresses-dialog.html',
                    controller: 'AdressesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Adresses', function(Adresses) {
                            return Adresses.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('adresses.new', {
            parent: 'adresses',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/adresses/adresses-dialog.html',
                    controller: 'AdressesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                rue: null,
                                noPorte: null,
                                mail: null,
                                telephone: null,
                                zipCode: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('adresses', null, { reload: 'adresses' });
                }, function() {
                    $state.go('adresses');
                });
            }]
        })
        .state('adresses.edit', {
            parent: 'adresses',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/adresses/adresses-dialog.html',
                    controller: 'AdressesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Adresses', function(Adresses) {
                            return Adresses.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('adresses', null, { reload: 'adresses' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('adresses.delete', {
            parent: 'adresses',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/adresses/adresses-delete-dialog.html',
                    controller: 'AdressesDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Adresses', function(Adresses) {
                            return Adresses.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('adresses', null, { reload: 'adresses' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
