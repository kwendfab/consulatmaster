(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('AssociationDetailController', AssociationDetailController);

    AssociationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Association', 'InfoAssociation'];

    function AssociationDetailController($scope, $rootScope, $stateParams, previousState, entity, Association, InfoAssociation) {
        var vm = this;

        vm.association = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('consulatApp:associationUpdate', function(event, result) {
            vm.association = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
