(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('BfIdDocDeleteController',BfIdDocDeleteController);

    BfIdDocDeleteController.$inject = ['$uibModalInstance', 'entity', 'BfIdDoc'];

    function BfIdDocDeleteController($uibModalInstance, entity, BfIdDoc) {
        var vm = this;

        vm.bfIdDoc = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            BfIdDoc.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
