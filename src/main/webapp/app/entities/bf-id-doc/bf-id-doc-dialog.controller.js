(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('BfIdDocDialogController', BfIdDocDialogController);

    BfIdDocDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'BfIdDoc'];

    function BfIdDocDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, BfIdDoc) {
        var vm = this;

        vm.bfIdDoc = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.bfIdDoc.id !== null) {
                BfIdDoc.update(vm.bfIdDoc, onSaveSuccess, onSaveError);
            } else {
                BfIdDoc.save(vm.bfIdDoc, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:bfIdDocUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateDelivr = false;
        vm.datePickerOpenStatus.dateExpr = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
