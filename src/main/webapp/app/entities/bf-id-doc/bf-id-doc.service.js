(function() {
    'use strict';
    angular
        .module('consulatApp')
        .factory('BfIdDoc', BfIdDoc);

    BfIdDoc.$inject = ['$resource', 'DateUtils'];

    function BfIdDoc ($resource, DateUtils) {
        var resourceUrl =  'api/bf-id-docs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateDelivr = DateUtils.convertLocalDateFromServer(data.dateDelivr);
                        data.dateExpr = DateUtils.convertLocalDateFromServer(data.dateExpr);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateDelivr = DateUtils.convertLocalDateToServer(copy.dateDelivr);
                    copy.dateExpr = DateUtils.convertLocalDateToServer(copy.dateExpr);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateDelivr = DateUtils.convertLocalDateToServer(copy.dateDelivr);
                    copy.dateExpr = DateUtils.convertLocalDateToServer(copy.dateExpr);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
