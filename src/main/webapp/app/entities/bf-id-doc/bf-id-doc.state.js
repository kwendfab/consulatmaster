(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('bf-id-doc', {
            parent: 'entity',
            url: '/bf-id-doc?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'BfIdDocs'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/bf-id-doc/bf-id-docs.html',
                    controller: 'BfIdDocController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('bf-id-doc-detail', {
            parent: 'bf-id-doc',
            url: '/bf-id-doc/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'BfIdDoc'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/bf-id-doc/bf-id-doc-detail.html',
                    controller: 'BfIdDocDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'BfIdDoc', function($stateParams, BfIdDoc) {
                    return BfIdDoc.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'bf-id-doc',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('bf-id-doc-detail.edit', {
            parent: 'bf-id-doc-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bf-id-doc/bf-id-doc-dialog.html',
                    controller: 'BfIdDocDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['BfIdDoc', function(BfIdDoc) {
                            return BfIdDoc.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('bf-id-doc.new', {
            parent: 'bf-id-doc',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bf-id-doc/bf-id-doc-dialog.html',
                    controller: 'BfIdDocDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numero: null,
                                autorite: null,
                                docId: null,
                                dateDelivr: null,
                                dateExpr: null,
                                ville: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('bf-id-doc', null, { reload: 'bf-id-doc' });
                }, function() {
                    $state.go('bf-id-doc');
                });
            }]
        })
        .state('bf-id-doc.edit', {
            parent: 'bf-id-doc',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bf-id-doc/bf-id-doc-dialog.html',
                    controller: 'BfIdDocDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['BfIdDoc', function(BfIdDoc) {
                            return BfIdDoc.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('bf-id-doc', null, { reload: 'bf-id-doc' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('bf-id-doc.delete', {
            parent: 'bf-id-doc',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/bf-id-doc/bf-id-doc-delete-dialog.html',
                    controller: 'BfIdDocDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['BfIdDoc', function(BfIdDoc) {
                            return BfIdDoc.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('bf-id-doc', null, { reload: 'bf-id-doc' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
