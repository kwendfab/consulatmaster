(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('CityController', CityController);

    CityController.$inject = ['$scope', 'City','TableService'];

    function CityController($scope, City, TableService) {

        var vm = this;

        var addItemData ={
            templateUrl: 'app/entities/city/city-dialog.html',
            controller: 'CityDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'mg',
            resolve: {
                entity: function () {
                    return {
                        id: null,
                        libelle: null,
                        countriesLibelle: null,
                        statesLibelle: null
                    };
                }
            }
        };

        /**
         *Modification d'un élément
         */
        var editItemData = {
            templateUrl: 'app/entities/city/city-dialog.html',
            controller: 'CityDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'md',
            resolve: {
                entity: null
            }
        };

        /**
         * Suppresion d'un élément
         */
        var deleteItemData ={
            templateUrl: 'app/entities/city/city-delete-dialog.html',
            controller: 'CityDeleteController',
            controllerAs: 'vm',
            size: 'sm',
            resolve: {
                entity: null
            }
        };

          var detailItemData = {
            templateUrl: 'app/entities/city/city-detail.html',
            controller: 'CityDetailController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'md',
            resolve: {
                entity: null, //valeur déterminé à l'appel de la boite
                 isViewing: true
            }
        };

        var dialogData = {
            addData: addItemData,
            editData: editItemData,
            deleteData: deleteItemData,
            detailData: detailItemData
        };
        var entitySearchParams = function() {
            return {

            };
        };

        var entityParams = function() {
            return {

            };
        };

        var datas = {
            scope: $scope,
            vm: vm,
            entity: City,
            entitySearch: City,
            dialogData: dialogData,
            entitySearchParams: entitySearchParams,
            entityParams: entityParams
        };
        TableService.init(datas);
    }
})();