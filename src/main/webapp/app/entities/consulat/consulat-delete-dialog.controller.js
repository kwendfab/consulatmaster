(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('ConsulatDeleteController',ConsulatDeleteController);

    ConsulatDeleteController.$inject = ['$uibModalInstance', 'entity', 'Consulat'];

    function ConsulatDeleteController($uibModalInstance, entity, Consulat) {
        var vm = this;

        vm.consulat = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Consulat.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
