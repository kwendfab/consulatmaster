(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('ConsulatDialogController', ConsulatDialogController);

    ConsulatDialogController.$inject = ['$scope', 'States', '$uibModalInstance', 'CityByStateId', 'DataUtils', 'entity', 'Consulat', 'UserExtra', 'Adresses', 'City', 'Members'];

    function ConsulatDialogController ($scope, States, $uibModalInstance,CityByStateId, DataUtils, entity, Consulat, UserExtra, Adresses, City, Members) {
        var vm = this;
   
        vm.consulat = entity;
        console.log(vm.consulat);
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;

        
if(vm.consulat.adressesId!=null)
{
    console.log("*******************************************"+vm.consulat);
        Adresses.get({id:vm.consulat.adressesId},
            function (data) {
               vm.consulat.adresseDTO=data;
               

               City.get({id:data.cityId},
                function (datas) {
                   vm.pays=datas;
                   vm.consulat.adresseDTO.etatId=datas.statesId;
                   console.log(vm.pays);
                    }
                )
                }
            )
        }
      

        vm.clear = clear;
             /*****************************Gestionnaire des onglets**************************************/
             vm.steps = [
                'IDENTIFICATION',
                'ADRESSE'
            ];
    
    
            vm.selection = vm.steps[0];
            vm.error = false;
            vm.showOnglet = showOnglet;
            vm.getCurrentStepIndex = function() {
                return vm.steps.indexOf(vm.selection);
            };
        
                 function showOnglet(onglet) {
                var stepIndex = vm.getCurrentStepIndex();
                return (vm.selection.toUpperCase() === onglet.toUpperCase());
            }
    
            // Go to a defined step index
           
           vm.goToStep = function(index, editForm) {
            if (!editForm.$invalid || (vm.getCurrentStepIndex() > index) ) {
                if (!angular.isUndefined(vm.steps[index])) {
                    console.log("change step");
                    vm.selection = vm.steps[index];
                }
            }
        };
    
            vm.hasNextStep = function() {
                var stepIndex = vm.getCurrentStepIndex();
                var nextStep = stepIndex + 1;
                // Return true if there is a next step, false if not
                return !angular.isUndefined(vm.steps[nextStep]);
            };
    
            vm.hasPreviousStep = function() {
                var stepIndex = vm.getCurrentStepIndex();
                var previousStep = stepIndex - 1;
                // Return true if there is a next step, false if not
                return !angular.isUndefined(vm.steps[previousStep]);
            };
    
            vm.incrementStep = function() {
                if (vm.hasNextStep()) {
                    //Vérification des champs du step précedent
                    var stepIndex = vm.getCurrentStepIndex();
                    var nextStep = stepIndex + 1;
                    vm.selection = vm.steps[nextStep];
                }
            };
    
    
            vm.decrementStep = function() {
                if (vm.hasPreviousStep()) {
                    var stepIndex = vm.getCurrentStepIndex();
                    var previousStep = stepIndex - 1;
                    vm.selection = vm.steps[previousStep];
                }
            };
       

            vm.etatColumns = [{
                name: 'libelle',
                title: 'Libelle'
            }];
            $scope.$on("onLoadEtats", function () {
    
            });
    loadEtats();
            function loadEtats() {
                States.query({
                    size: 10000
                }, function (data) {
                    console.log("l'oganisation");
                    if (data != null) {
                        console.log(data);
                        vm.etats = [];
                        vm.etats = data;
                    }
                });
    
                
               // vm.members.lieuNaissance = test.libele;
            }
    
    
            vm.statesVilleColumns = [{
                name: 'libelle',
                title: 'Libelle'
            }];
            $scope.$on("onLoadStatesVille", function () {
    
            });


            $scope.$watch("vm.consulat.adresseDTO.etatId", function () {
                console.log("Dans le watch");
                loadEtats();
        if(vm.consulat.adresseDTO.etatId!=null )
        {
                CityByStateId.query ({
                        id: vm.consulat.adresseDTO.etatId
                    }, function (data) {
                        vm.villeAdresse = data;
                    })
                }
                
            });


            $scope.$watch("vm.consulat.adresseDTO.cityId", function () {
                console.log("Dans le watch");
        if(vm.consulat.adresseDTO.etatId!=null )
        {
                CityByStateId.query ({
                        id: vm.consulat.adresseDTO.etatId
                    }, function (data) {
                        vm.villeAdresse = data;
                    })
                }
                
            });




        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.consulat.id !== null) {
                Consulat.update(vm.consulat, onSaveSuccess, onSaveError);
            } else {
                Consulat.save(vm.consulat, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:consulatUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setLogoBf = function ($file, consulat) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        consulat.logoBf = base64Data;
                        consulat.logoBfContentType = $file.type;
                    });
                });
            }
        };

        vm.setLogoUsa = function ($file, consulat) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        consulat.imageVerso = base64Data;
                        consulat.imageVersoContentType = $file.type;
                    });
                });
            }
        };

        vm.setArmoirie = function ($file, consulat) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        console.log("-----------------------------");
                        consulat.armoirie = base64Data;
                        consulat.armoirieContentType = $file.type;
                    });
                });
            }
        };



        vm.setSignature = function ($file, consulat) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        console.log("-----------------------------");
                        consulat.signature = base64Data;
                        consulat.signatureContentType = $file.type;
                    });
                });
            }
        };







    }
})();
