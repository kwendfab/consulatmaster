(function() {
    'use strict';
    angular
        .module('consulatApp')
        .factory('Consulat', Consulat);

    Consulat.$inject = ['$resource'];

    function Consulat ($resource) {
        var resourceUrl =  'api/consulats/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
