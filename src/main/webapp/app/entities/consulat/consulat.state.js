(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('consulat', {
            parent: 'entity',
            url: '/consulat?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Consulats'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/consulat/consulats.html',
                    controller: 'ConsulatController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('consulat-detail', {
            parent: 'consulat',
            url: '/consulat/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Consulat'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/consulat/consulat-detail.html',
                    controller: 'ConsulatDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Consulat', function($stateParams, Consulat) {
                    return Consulat.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'consulat',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('consulat-detail.edit', {
            parent: 'consulat-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consulat/consulat-dialog.html',
                    controller: 'ConsulatDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Consulat', function(Consulat) {
                            return Consulat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('consulat.new', {
            parent: 'consulat',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consulat/consulat-dialog.html',
                    controller: 'ConsulatDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                code: null,
                                nom: null,
                                description: null,
                                logoBf: null,
                                logoBfContentType: null,
                                logoUsa: null,
                                logoUsaContentType: null,
                                armoirie: null,
                                armoirieContentType: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('consulat', null, { reload: 'consulat' });
                }, function() {
                    $state.go('consulat');
                });
            }]
        })
        .state('consulat.edit', {
            parent: 'consulat',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consulat/consulat-dialog.html',
                    controller: 'ConsulatDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Consulat', function(Consulat) {
                            return Consulat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('consulat', null, { reload: 'consulat' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('consulat.delete', {
            parent: 'consulat',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consulat/consulat-delete-dialog.html',
                    controller: 'ConsulatDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Consulat', function(Consulat) {
                            return Consulat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('consulat', null, { reload: 'consulat' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
