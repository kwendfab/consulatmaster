(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('CountryIdDocDeleteController',CountryIdDocDeleteController);

    CountryIdDocDeleteController.$inject = ['$uibModalInstance', 'entity', 'CountryIdDoc'];

    function CountryIdDocDeleteController($uibModalInstance, entity, CountryIdDoc) {
        var vm = this;

        vm.countryIdDoc = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            CountryIdDoc.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
