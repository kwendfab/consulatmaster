(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('CountryIdDocDetailController', CountryIdDocDetailController);

    CountryIdDocDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'CountryIdDoc'];

    function CountryIdDocDetailController($scope, $rootScope, $stateParams, previousState, entity, CountryIdDoc) {
        var vm = this;

        vm.countryIdDoc = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('consulatApp:countryIdDocUpdate', function(event, result) {
            vm.countryIdDoc = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
