(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('CountryIdDocDialogController', CountryIdDocDialogController);

    CountryIdDocDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'CountryIdDoc'];

    function CountryIdDocDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, CountryIdDoc) {
        var vm = this;

        vm.countryIdDoc = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.countryIdDoc.id !== null) {
                CountryIdDoc.update(vm.countryIdDoc, onSaveSuccess, onSaveError);
            } else {
                CountryIdDoc.save(vm.countryIdDoc, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:countryIdDocUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateDelivr = false;
        vm.datePickerOpenStatus.dateExpr = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
