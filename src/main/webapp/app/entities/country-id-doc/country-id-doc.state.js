(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('country-id-doc', {
            parent: 'entity',
            url: '/country-id-doc?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'CountryIdDocs'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/country-id-doc/country-id-docs.html',
                    controller: 'CountryIdDocController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('country-id-doc-detail', {
            parent: 'country-id-doc',
            url: '/country-id-doc/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'CountryIdDoc'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/country-id-doc/country-id-doc-detail.html',
                    controller: 'CountryIdDocDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'CountryIdDoc', function($stateParams, CountryIdDoc) {
                    return CountryIdDoc.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'country-id-doc',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('country-id-doc-detail.edit', {
            parent: 'country-id-doc-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/country-id-doc/country-id-doc-dialog.html',
                    controller: 'CountryIdDocDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CountryIdDoc', function(CountryIdDoc) {
                            return CountryIdDoc.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('country-id-doc.new', {
            parent: 'country-id-doc',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/country-id-doc/country-id-doc-dialog.html',
                    controller: 'CountryIdDocDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numero: null,
                                autorite: null,
                                dateDelivr: null,
                                dateExpr: null,
                                etat: null,
                                ville: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('country-id-doc', null, { reload: 'country-id-doc' });
                }, function() {
                    $state.go('country-id-doc');
                });
            }]
        })
        .state('country-id-doc.edit', {
            parent: 'country-id-doc',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/country-id-doc/country-id-doc-dialog.html',
                    controller: 'CountryIdDocDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CountryIdDoc', function(CountryIdDoc) {
                            return CountryIdDoc.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('country-id-doc', null, { reload: 'country-id-doc' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('country-id-doc.delete', {
            parent: 'country-id-doc',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/country-id-doc/country-id-doc-delete-dialog.html',
                    controller: 'CountryIdDocDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['CountryIdDoc', function(CountryIdDoc) {
                            return CountryIdDoc.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('country-id-doc', null, { reload: 'country-id-doc' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
