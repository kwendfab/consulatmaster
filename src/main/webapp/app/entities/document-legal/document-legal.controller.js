(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('DocumentLegalController', DocumentLegalController);

    DocumentLegalController.$inject = ['$scope','DocumentLegal', 'TableService'];

    function DocumentLegalController($scope,DocumentLegal, TableService) {

        var vm = this;

        var addItemData ={
            templateUrl: 'app/entities/document-legal/document-legal-dialog.html',
            controller: 'DocumentLegalDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'mg',
            resolve: {
                entity: function () {
                    return {
                        id: null,
                        libelle: null
                    };
                }
            }
        };

        /**
         *Modification d'un élément
         */
        var editItemData = {
            templateUrl: 'app/entities/document-legal/document-legal-dialog.html',
            controller: 'DocumentLegalDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'md',
            resolve: {
                entity: null
            }
        };

        /**
         * Suppresion d'un élément
         */
        var deleteItemData ={
            templateUrl: 'app/entities/document-legal/document-legal-delete-dialog.html',
            controller: 'DocumentLegalDeleteController',
            controllerAs: 'vm',
            size: 'sm',
            resolve: {
                entity: null
            }
        };

          var detailItemData = {
            templateUrl: 'app/entities/document-legal/document-legal-detail.html',
            controller: 'DocumentLegalDetailController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'md',
            resolve: {
                entity: null, //valeur déterminé à l'appel de la boite
                 isViewing: true
            }
        };

        var dialogData = {
            addData: addItemData,
            editData: editItemData,
            deleteData: deleteItemData,
            detailData: detailItemData
        };
        var entitySearchParams = function() {
            return {

            };
        };

        var entityParams = function() {
            return {

            };
        };

        var datas = {
            scope: $scope,
            vm: vm,
            entity: DocumentLegal,
            entitySearch: DocumentLegal,
            dialogData: dialogData,
            entitySearchParams: entitySearchParams,
            entityParams: entityParams
        };
        TableService.init(datas);
    }
})();
