(function() {
    'use strict';
    angular
        .module('consulatApp')
        .factory('DocumentLegal', DocumentLegal);

    DocumentLegal.$inject = ['$resource', 'DateUtils'];

    function DocumentLegal ($resource, DateUtils) {
        var resourceUrl =  'api/document-legals/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateExpr = DateUtils.convertLocalDateFromServer(data.dateExpr);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateExpr = DateUtils.convertLocalDateToServer(copy.dateExpr);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateExpr = DateUtils.convertLocalDateToServer(copy.dateExpr);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
