(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('document-legal', {
            parent: 'entity',
            url: '/document-legal?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DocumentLegals'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/document-legal/document-legals.html',
                    controller: 'DocumentLegalController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('document-legal-detail', {
            parent: 'document-legal',
            url: '/document-legal/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DocumentLegal'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/document-legal/document-legal-detail.html',
                    controller: 'DocumentLegalDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'DocumentLegal', function($stateParams, DocumentLegal) {
                    return DocumentLegal.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'document-legal',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('document-legal-detail.edit', {
            parent: 'document-legal-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/document-legal/document-legal-dialog.html',
                    controller: 'DocumentLegalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DocumentLegal', function(DocumentLegal) {
                            return DocumentLegal.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('document-legal.new', {
            parent: 'document-legal',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/document-legal/document-legal-dialog.html',
                    controller: 'DocumentLegalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numero: null,
                                dateExpr: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('document-legal', null, { reload: 'document-legal' });
                }, function() {
                    $state.go('document-legal');
                });
            }]
        })
        .state('document-legal.edit', {
            parent: 'document-legal',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/document-legal/document-legal-dialog.html',
                    controller: 'DocumentLegalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DocumentLegal', function(DocumentLegal) {
                            return DocumentLegal.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('document-legal', null, { reload: 'document-legal' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('document-legal.delete', {
            parent: 'document-legal',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/document-legal/document-legal-delete-dialog.html',
                    controller: 'DocumentLegalDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DocumentLegal', function(DocumentLegal) {
                            return DocumentLegal.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('document-legal', null, { reload: 'document-legal' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
