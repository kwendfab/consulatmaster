(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('InfoAssociationDeleteController',InfoAssociationDeleteController);

    InfoAssociationDeleteController.$inject = ['$uibModalInstance', 'entity', 'InfoAssociation'];

    function InfoAssociationDeleteController($uibModalInstance, entity, InfoAssociation) {
        var vm = this;

        vm.infoAssociation = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InfoAssociation.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
