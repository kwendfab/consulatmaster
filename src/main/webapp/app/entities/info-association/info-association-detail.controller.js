(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('InfoAssociationDetailController', InfoAssociationDetailController);

    InfoAssociationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'InfoAssociation'];

    function InfoAssociationDetailController($scope, $rootScope, $stateParams, previousState, entity, InfoAssociation) {
        var vm = this;

        vm.infoAssociation = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('consulatApp:infoAssociationUpdate', function(event, result) {
            vm.infoAssociation = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
