(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('InfoAssociationDialogController', InfoAssociationDialogController);

    InfoAssociationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'InfoAssociation'];

    function InfoAssociationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, InfoAssociation) {
        var vm = this;

        vm.infoAssociation = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.infoAssociation.id !== null) {
                InfoAssociation.update(vm.infoAssociation, onSaveSuccess, onSaveError);
            } else {
                InfoAssociation.save(vm.infoAssociation, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:infoAssociationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateEntree = false;
        vm.datePickerOpenStatus.dateSortie = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
