(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('info-association', {
            parent: 'entity',
            url: '/info-association?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'InfoAssociations'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/info-association/info-associations.html',
                    controller: 'InfoAssociationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('info-association-detail', {
            parent: 'info-association',
            url: '/info-association/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'InfoAssociation'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/info-association/info-association-detail.html',
                    controller: 'InfoAssociationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'InfoAssociation', function($stateParams, InfoAssociation) {
                    return InfoAssociation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'info-association',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('info-association-detail.edit', {
            parent: 'info-association-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-association/info-association-dialog.html',
                    controller: 'InfoAssociationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InfoAssociation', function(InfoAssociation) {
                            return InfoAssociation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('info-association.new', {
            parent: 'info-association',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-association/info-association-dialog.html',
                    controller: 'InfoAssociationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                dateEntree: null,
                                dateSortie: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('info-association', null, { reload: 'info-association' });
                }, function() {
                    $state.go('info-association');
                });
            }]
        })
        .state('info-association.edit', {
            parent: 'info-association',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-association/info-association-dialog.html',
                    controller: 'InfoAssociationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InfoAssociation', function(InfoAssociation) {
                            return InfoAssociation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('info-association', null, { reload: 'info-association' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('info-association.delete', {
            parent: 'info-association',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-association/info-association-delete-dialog.html',
                    controller: 'InfoAssociationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InfoAssociation', function(InfoAssociation) {
                            return InfoAssociation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('info-association', null, { reload: 'info-association' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
