(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('InfoParentsDeleteController',InfoParentsDeleteController);

    InfoParentsDeleteController.$inject = ['$uibModalInstance', 'entity', 'InfoParents'];

    function InfoParentsDeleteController($uibModalInstance, entity, InfoParents) {
        var vm = this;

        vm.infoParents = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InfoParents.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
