(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('InfoParentsDialogController', InfoParentsDialogController);

    InfoParentsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'InfoParents'];

    function InfoParentsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, InfoParents) {
        var vm = this;

        vm.infoParents = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.infoParents.id !== null) {
                InfoParents.update(vm.infoParents, onSaveSuccess, onSaveError);
            } else {
                InfoParents.save(vm.infoParents, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:infoParentsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
