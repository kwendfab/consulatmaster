(function() {
    'use strict';
    angular
        .module('consulatApp')
        .factory('InfoParents', InfoParents);

    InfoParents.$inject = ['$resource'];

    function InfoParents ($resource) {
        var resourceUrl =  'api/info-parents/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
