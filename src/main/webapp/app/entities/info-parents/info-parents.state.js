(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('info-parents', {
            parent: 'entity',
            url: '/info-parents?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'InfoParents'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/info-parents/info-parents.html',
                    controller: 'InfoParentsController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('info-parents-detail', {
            parent: 'info-parents',
            url: '/info-parents/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'InfoParents'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/info-parents/info-parents-detail.html',
                    controller: 'InfoParentsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'InfoParents', function($stateParams, InfoParents) {
                    return InfoParents.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'info-parents',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('info-parents-detail.edit', {
            parent: 'info-parents-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-parents/info-parents-dialog.html',
                    controller: 'InfoParentsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InfoParents', function(InfoParents) {
                            return InfoParents.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('info-parents.new', {
            parent: 'info-parents',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-parents/info-parents-dialog.html',
                    controller: 'InfoParentsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numero: null,
                                nomPere: null,
                                prenomPere: null,
                                nomMere: null,
                                prenomMere: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('info-parents', null, { reload: 'info-parents' });
                }, function() {
                    $state.go('info-parents');
                });
            }]
        })
        .state('info-parents.edit', {
            parent: 'info-parents',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-parents/info-parents-dialog.html',
                    controller: 'InfoParentsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InfoParents', function(InfoParents) {
                            return InfoParents.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('info-parents', null, { reload: 'info-parents' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('info-parents.delete', {
            parent: 'info-parents',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-parents/info-parents-delete-dialog.html',
                    controller: 'InfoParentsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InfoParents', function(InfoParents) {
                            return InfoParents.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('info-parents', null, { reload: 'info-parents' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
