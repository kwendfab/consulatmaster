(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('InfoPosteDeleteController',InfoPosteDeleteController);

    InfoPosteDeleteController.$inject = ['$uibModalInstance', 'entity', 'InfoPoste'];

    function InfoPosteDeleteController($uibModalInstance, entity, InfoPoste) {
        var vm = this;

        vm.infoPoste = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InfoPoste.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
