(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('InfoPosteDialogController', InfoPosteDialogController);

    InfoPosteDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'InfoPoste', 'Members', 'Poste'];

    function InfoPosteDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, InfoPoste, Members, Poste) {
        var vm = this;

        vm.infoPoste = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.members = Members.query();
        vm.postes = Poste.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.infoPoste.id !== null) {
                InfoPoste.update(vm.infoPoste, onSaveSuccess, onSaveError);
            } else {
                InfoPoste.save(vm.infoPoste, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:infoPosteUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateDebut = false;
        vm.datePickerOpenStatus.dateFin = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
