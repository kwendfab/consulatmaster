(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('info-poste', {
            parent: 'entity',
            url: '/info-poste?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'InfoPostes'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/info-poste/info-postes.html',
                    controller: 'InfoPosteController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('info-poste-detail', {
            parent: 'info-poste',
            url: '/info-poste/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'InfoPoste'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/info-poste/info-poste-detail.html',
                    controller: 'InfoPosteDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'InfoPoste', function($stateParams, InfoPoste) {
                    return InfoPoste.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'info-poste',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('info-poste-detail.edit', {
            parent: 'info-poste-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-poste/info-poste-dialog.html',
                    controller: 'InfoPosteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InfoPoste', function(InfoPoste) {
                            return InfoPoste.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('info-poste.new', {
            parent: 'info-poste',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-poste/info-poste-dialog.html',
                    controller: 'InfoPosteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                dateDebut: null,
                                dateFin: null,
                                actuelle: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('info-poste', null, { reload: 'info-poste' });
                }, function() {
                    $state.go('info-poste');
                });
            }]
        })
        .state('info-poste.edit', {
            parent: 'info-poste',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-poste/info-poste-dialog.html',
                    controller: 'InfoPosteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InfoPoste', function(InfoPoste) {
                            return InfoPoste.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('info-poste', null, { reload: 'info-poste' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('info-poste.delete', {
            parent: 'info-poste',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-poste/info-poste-delete-dialog.html',
                    controller: 'InfoPosteDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InfoPoste', function(InfoPoste) {
                            return InfoPoste.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('info-poste', null, { reload: 'info-poste' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
