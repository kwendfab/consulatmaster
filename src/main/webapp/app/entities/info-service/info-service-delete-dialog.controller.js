(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('InfoServiceDeleteController',InfoServiceDeleteController);

    InfoServiceDeleteController.$inject = ['$uibModalInstance', 'entity', 'InfoService'];

    function InfoServiceDeleteController($uibModalInstance, entity, InfoService) {
        var vm = this;

        vm.infoService = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InfoService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
