(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('InfoServiceDetailController', InfoServiceDetailController);

    InfoServiceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'InfoService', 'MembreService', 'Members'];

    function InfoServiceDetailController($scope, $rootScope, $stateParams, previousState, entity, InfoService, MembreService, Members) {
        var vm = this;

        vm.infoService = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('consulatApp:infoServiceUpdate', function(event, result) {
            vm.infoService = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
