(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('InfoServiceDialogController', InfoServiceDialogController);

    InfoServiceDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'InfoService', 'MembreService', 'Members'];

    function InfoServiceDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, InfoService, MembreService, Members) {
        var vm = this;

        vm.infoService = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.membreservices = MembreService.query();
        vm.members = Members.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.infoService.id !== null) {
                InfoService.update(vm.infoService, onSaveSuccess, onSaveError);
            } else {
                InfoService.save(vm.infoService, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:infoServiceUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateIntegration = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
