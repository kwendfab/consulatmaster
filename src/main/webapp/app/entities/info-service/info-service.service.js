(function() {
    'use strict';
    angular
        .module('consulatApp')
        .factory('InfoService', InfoService);

    InfoService.$inject = ['$resource', 'DateUtils'];

    function InfoService ($resource, DateUtils) {
        var resourceUrl =  'api/info-services/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateIntegration = DateUtils.convertLocalDateFromServer(data.dateIntegration);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateIntegration = DateUtils.convertLocalDateToServer(copy.dateIntegration);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateIntegration = DateUtils.convertLocalDateToServer(copy.dateIntegration);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
