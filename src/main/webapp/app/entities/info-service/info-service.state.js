(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('info-service', {
            parent: 'entity',
            url: '/info-service?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'InfoServices'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/info-service/info-services.html',
                    controller: 'InfoServiceController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('info-service-detail', {
            parent: 'info-service',
            url: '/info-service/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'InfoService'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/info-service/info-service-detail.html',
                    controller: 'InfoServiceDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'InfoService', function($stateParams, InfoService) {
                    return InfoService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'info-service',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('info-service-detail.edit', {
            parent: 'info-service-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-service/info-service-dialog.html',
                    controller: 'InfoServiceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InfoService', function(InfoService) {
                            return InfoService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('info-service.new', {
            parent: 'info-service',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-service/info-service-dialog.html',
                    controller: 'InfoServiceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                dateIntegration: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('info-service', null, { reload: 'info-service' });
                }, function() {
                    $state.go('info-service');
                });
            }]
        })
        .state('info-service.edit', {
            parent: 'info-service',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-service/info-service-dialog.html',
                    controller: 'InfoServiceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InfoService', function(InfoService) {
                            return InfoService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('info-service', null, { reload: 'info-service' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('info-service.delete', {
            parent: 'info-service',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/info-service/info-service-delete-dialog.html',
                    controller: 'InfoServiceDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InfoService', function(InfoService) {
                            return InfoService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('info-service', null, { reload: 'info-service' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
