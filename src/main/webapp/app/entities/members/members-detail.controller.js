(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('MembersDetailController', MembersDetailController);

        MembersDetailController.$inject = ['$scope','$http','DataUtils','Members', 'InfoAssociationByMember', '$window', '$uibModalInstance', 'Country', 'entity', 'CityByStateId', 'Association', 'CityByCountryId', 'Poste', 'States', 'City', 'DocumentLegal', 'Adresses', 'InfoAssociation', 'InfoService', 'Consulat'];

    function MembersDetailController ($scope,$http,DataUtils,Members, InfoAssociationByMember, $window, $uibModalInstance, Country, entity, CityByStateId, Association, CityByCountryId,Poste, States, City, DocumentLegal, Adresses, InfoAssociation, InfoService, Consulat) {
        var vm = this;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save =save;
        vm.changerAdresse = changerAdresse;

        vm.documents = DocumentLegal.query();


        function changerAdresse(bol) {
console.log(vm.members.changerAdresse);    
              }
    

        vm.setPhoto = function ($file, members) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        members.photo = base64Data;
                        members.photoContentType = $file.type;
                    });
                });
            }
        };

        vm.setSignature = function ($file, consulat) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        console.log("-----------------------------");
                        consulat.signature = base64Data;
                        consulat.signatureContentType = $file.type;
                    });
                });
            }
        };
          
        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }

        vm.members = entity;
        vm.members.dateArrivee = new Date(vm.members.dateArrivee);
if(vm.members.signature!=null)
{
        vm.members.signature="data:image/png;base64,"+vm.members.signature;
}


        var fileURLPDF;

        vm.etatCarte=etatCarte;

        vm.members.dateDelivr = new Date(vm.members.dateDelivr);

        vm.init = init;

        function init()
{
        InfoAssociationByMember.query({id:vm.members.id},
            function (data) {
                console.log(data);
               vm.members.associations=data;
                }
            )


          /*  AdressesByMember.query({id:vm.members.id},
                function (data) {
                   vm.members.adresseDTO=data;
                    }
                )*/
}

vm.init();

        console.log(vm.members);

        function save() {
            if(vm.members.signature!=null)
            {
            var canvas = document.createElement("canvas");
        var dataURL = canvas.toDataURL( "image/png" );
        vm.members.signature =  vm.members.signature.substring( "data:image/png;base64,".length );
    }
            vm.isSaving = true;
            if (vm.members.id !== null) {
                Members.update(vm.members, onSaveSuccess, onSaveError);
            } else {
                Members.save(vm.members, onSaveSuccess, onSaveError);
            }
           }

           function onSaveSuccess (resultat) {
            $scope.$emit('consulatApp:consulatUpdate', resultat);
            $uibModalInstance.close(resultat);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.clear = clear;
             /*****************************Gestionnaire des onglets**************************************/
             vm.steps = [
                'IDENTIFICATION',
                'ADRESSE',
                'DOC IDENTIFICATION',
                'ASSOCIATION'
            ];
    
    
            vm.selection = vm.steps[0];
            vm.error = false;
            vm.showOnglet = showOnglet;
            vm.getCurrentStepIndex = function() {
                return vm.steps.indexOf(vm.selection);
            };
        
                 function showOnglet(onglet) {
                var stepIndex = vm.getCurrentStepIndex();
                return (vm.selection.toUpperCase() === onglet.toUpperCase());
            }
    
          // Go to a defined step index
        vm.goToStep = function(index, editForm) {
            if (!editForm.$invalid || (vm.getCurrentStepIndex() > index) ) {
                if (!angular.isUndefined(vm.steps[index])) {
                    console.log("change step");
                    vm.selection = vm.steps[index];
                }
            }
        };
            vm.hasNextStep = function() {
                var stepIndex = vm.getCurrentStepIndex();
                var nextStep = stepIndex + 1;
                // Return true if there is a next step, false if not
                return !angular.isUndefined(vm.steps[nextStep]);
            };
    
            vm.hasPreviousStep = function() {
                var stepIndex = vm.getCurrentStepIndex();
                var previousStep = stepIndex - 1;
                // Return true if there is a next step, false if not
                return !angular.isUndefined(vm.steps[previousStep]);
            };
    
            vm.incrementStep = function() {
                if (vm.hasNextStep()) {
                    //Vérification des champs du step précedent
                    var stepIndex = vm.getCurrentStepIndex();
                    var nextStep = stepIndex + 1;
                    vm.selection = vm.steps[nextStep];
                }
            };
    
    
            vm.decrementStep = function() {
                if (vm.hasPreviousStep()) {
                    var stepIndex = vm.getCurrentStepIndex();
                    var previousStep = stepIndex - 1;
                    vm.selection = vm.steps[previousStep];
                }
            };



            function etatCarte() {
                $http.get('/api/members/cartePDF', {
                        responseType: 'arraybuffer',
                        params: {
                            idMembre: vm.members.id
                        }
    
                    })
                    .success(function(response) {
                        var file = new Blob([(response)], {
                            type: 'application/pdf'
                        });
                        fileURLPDF = URL.createObjectURL(file);
                        $window.open(fileURLPDF, '_blank');
                        console.log(file);
                        console.log(fileURLPDF);
    
    
                    });
               }





               vm.paysColumns = [{
                name: 'libele',
                title: 'Libelle'
            }];
            $scope.$on("onLoadPays", function () {
    
            });
            loadPays();
    
            function loadPays() {
                Country.query({
                    size: 10000
                }, function (data) {
                    console.log("l'oganisation");
                    if (data != null) {
                        console.log(data);
                        vm.pays = [];
                        vm.pays = data;
                    }
                });
    
    
               // vm.members.lieuNaissance = test.libele;
            }
    
            $scope.$watch("vm.members.lieuNaissanceId", function () {
                console.log("Dans le watch");
    if(vm.members.lieuNaissanceId!=null)
    {
                    Country.get ({
                        id: vm.members.lieuNaissanceId
                    }, function (data) {
                        vm.members.lieuNaissance = data.libele;
                    })
                }
    
            });
    
    
    
    
    
            vm.villeColumns = [{
                name: 'libelle',
                title: 'Libelle'
            }];
            $scope.$on("onLoadVilles", function () {
    
            });
            loadVilles();
    
            function loadVilles() {
                City.query({
                    size: 10000
                }, function (data) {
                    console.log("l'oganisation");
                    if (data != null) {
                        console.log(data);
    
                        vm.villes = data;
                    }
                });
    
    
            }


            vm.posteColumns = [{
                name: 'libelle',
                title: 'Libelle'
            }];
           
            loadPostes();
    
            function loadPostes() {
                vm.postes = Poste.query();

        }






    
    
    
    
            $scope.$on("onLoadVillesBF", function () {
    
            });
            loadVillesBF();
    
            function loadVillesBF() {
                CityByCountryId.query ({
                    id: 1001
                }, function (data) {
                    vm.countriesVille = data;
                })
    
    
               // vm.members.lieuNaissance = test.libele;
            }
    
    
            $scope.$on("onLoadVillesUSA", function () {
    
            });
            loadVillesUSA();
    
            function loadVillesUSA() {
                CityByCountryId.query ({
                    id: 1014
                }, function (data) {
                    vm.countriesVilleUSA = data;
                })
    
    
               // vm.members.lieuNaissance = test.libele;
            }
    
    
    
    
    
    
    
    
            $scope.$watch("vm.members.villeArriveeId", function () {
                console.log("Dans le watch");

    if(vm.members.villeArriveeId!=null)
    {
                    City.get ({
                        id: vm.members.villeArriveeId
                    }, function (data) {
                        vm.members.villeArrivee = data.libelle;
                        console.log(data);
                        vm.members.etatArrivee = data.statesLibelle;
                    })
                }
    
            });
    
            $scope.$watch("vm.members.villeId", function () {
                console.log("Dans le watch");
                if(vm.members.villeId!=null)
                {
    
                    City.get ({
                        id: vm.members.villeId
                    }, function (data) {
                        vm.members.ville = data.libelle;
                    })
                }
    
            });
    
    
            vm.etatColumns = [{
                name: 'libelle',
                title: 'Libelle'
            }];
            $scope.$on("onLoadEtats", function () {
    
            });
            loadEtats();
    
            function loadEtats() {
                States.query({
                    size: 10000
                }, function (data) {
                    console.log("l'oganisation");
                    if (data != null) {
                        console.log(data);
                        vm.etats = [];
                        vm.etats = data;
                    }
                });
    
    
               // vm.members.lieuNaissance = test.libele;
            }
    
    
            vm.statesVilleColumns = [{
                name: 'libelle',
                title: 'Libelle'
            }];
            $scope.$on("onLoadStatesVille", function () {
    
            });
    
    
            $scope.$watch("vm.members.lieuNaissanceId", function () {
                console.log("Dans le watch");
    if(vm.members.lieuNaissanceId!=null)
    {
                    Country.get ({
                        id: vm.members.lieuNaissanceId
                    }, function (data) {
                        vm.members.lieuNaissance = data.libele;
                    })
                }
    
            });


            $scope.$watch("vm.members.statesId", function () {
                console.log("Dans le watch");
                if(vm.members.statesId!=null)
                {
                CityByStateId.query ({
                        id: vm.members.statesId
                    }, function (data) {
                        vm.countriesArrivee = data;
                    })
                }
    
            });

            $scope.$watch("vm.associationId", function (ids) {

                Association.query ( function (data) {
                    vm.assocs = data;
                })
    
                console.log(ids);
                if (angular.isDefined(ids)) {
                    vm.members.associations = [];
                    angular.forEach(ids, function (value, index) {
                        var auth = _.find(vm.assocs, { 'id': value });
                        vm.members.associations.push(auth);
                    });
                }
            });
    
    
    
            $scope.$watch("vm.members.country.statesId", function () {
                console.log("Dans le watch");
                if(vm.members.country.statesId!=null)
                {
                CityByStateId.query ({
                        id: vm.members.country.statesId
                    }, function (data) {
                        vm.statesVille = data;
                    })
                }
    
            });
    
    
           /* $scope.$watch("vm.members.adresseDTO.etatId", function () {
    
                CityByStateId.query ({
                        id: vm.members.adresseDTO.etatId
                    }, function (data) {
                        vm.villeAdresse = data;
                    })
    
            });*/
    
            $scope.$watch("vm.members.adresseDTO.etatId", function () {
                console.log("Dans le watch");
                if(vm.members.adresseDTO.etatId!=null)
                {
                CityByStateId.query ({
                        id: vm.members.adresseDTO.etatId
                    }, function (data) {
                        vm.countriesVilleUSA = data;
                    })
                }
    
            });
    
    
            vm.colums = [{
                name: 'nom',
                title: 'Nom'
            }];
    
    
            $scope.$watch("vm.associationId", function (ids) {

                Association.query ( function (data) {
                    vm.assocs = data;
                })
    
                console.log(ids);
                if (angular.isDefined(ids)) {
                    vm.members.associations = [];
                    angular.forEach(ids, function (value, index) {
                        var auth = _.find(vm.assocs, { 'id': value });
                        vm.members.associations.push(auth);
                    });
                }
            });









    
            function clear () {
                $uibModalInstance.dismiss('cancel');
            }

   
    }
})();
