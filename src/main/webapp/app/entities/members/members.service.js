(function() {
    'use strict';
    angular
        .module('consulatApp')
        .factory('Members', Members)
        .factory('MemberMultiParams', MemberMultiParams);

    Members.$inject = ['$resource', 'DateUtils'];
    MemberMultiParams.$inject = ['$resource', 'DateUtils'];




    function MemberMultiParams ($resource, DateUtils) {
        var resourceUrl =  'api/members/findByParams';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateNaissance = DateUtils.convertLocalDateFromServer(data.dateNaissance);
                        data.dateArrivee = DateUtils.convertLocalDateFromServer(data.dateArrivee);
                    }
                    return data;
                }
            }
        });
    }






    function Members ($resource, DateUtils) {
        var resourceUrl =  'api/members/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateNaissance = DateUtils.convertLocalDateFromServer(data.dateNaissance);
                        data.dateArrivee = DateUtils.convertLocalDateFromServer(data.dateArrivee);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateNaissance = DateUtils.convertLocalDateToServer(copy.dateNaissance);
                    copy.dateArrivee = DateUtils.convertLocalDateToServer(copy.dateArrivee);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateNaissance = DateUtils.convertLocalDateToServer(copy.dateNaissance);
                    copy.dateArrivee = DateUtils.convertLocalDateToServer(copy.dateArrivee);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
