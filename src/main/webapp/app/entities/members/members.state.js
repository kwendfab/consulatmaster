(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('members', {
            parent: 'entity',
            url: '/members?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Members'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/members/members.html',
                    controller: 'MembersController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('members-stats', {
            parent: 'entity',
            url: '/members-stats?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Members-stats'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/members/members-stats.html',
                    controller: 'MembersStatsController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('members-detail', {
            parent: 'members',
            url: '/members/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Members'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/members/members-detail.html',
                    controller: 'MembersDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Members', function($stateParams, Members) {
                    return Members.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'members',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('members-detail.edit', {
            parent: 'members-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/members/members-dialog.html',
                    controller: 'MembersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Members', function(Members) {
                            return Members.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('members.new', {
            parent: 'members',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/members/members-dialog.html',
                    controller: 'MembersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numero: null,
                                nom: null,
                                prenom: null,
                                dateNaissance: null,
                                lieuNaissance: null,
                                sexe: null,
                                mariage: null,
                                telephone: null,
                                dateArrivee: null,
                                villeArrivee: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('members', null, { reload: 'members' });
                }, function() {
                    $state.go('members');
                });
            }]
        })
        .state('members.edit', {
            parent: 'members',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/members/members-dialog.html',
                    controller: 'MembersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Members', function(Members) {
                            return Members.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('members', null, { reload: 'members' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('members.delete', {
            parent: 'members',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/members/members-delete-dialog.html',
                    controller: 'MembersDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Members', function(Members) {
                            return Members.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('members', null, { reload: 'members' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
