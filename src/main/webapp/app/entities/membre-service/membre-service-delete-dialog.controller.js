(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('MembreServiceDeleteController',MembreServiceDeleteController);

    MembreServiceDeleteController.$inject = ['$uibModalInstance', 'entity', 'MembreService'];

    function MembreServiceDeleteController($uibModalInstance, entity, MembreService) {
        var vm = this;

        vm.membreService = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MembreService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
