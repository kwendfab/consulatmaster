(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('MembreServiceDialogController', MembreServiceDialogController);

    MembreServiceDialogController.$inject = ['UserExtraConnected','$timeout', '$scope', '$uibModalInstance', 'entity', 'MembreService', 'Association'];

    function MembreServiceDialogController (UserExtraConnected,$timeout, $scope, $uibModalInstance, entity, MembreService, Association) {
        var vm = this;

        vm.membreService = entity;
        vm.clear = clear;
        vm.save = save;
        vm.init =init;
        vm.user = UserExtraConnected.query();

        vm.associations = Association.query();
        console.log(vm.associations);

function init()
{
    vm.membreService.consulat=null;
}



        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.membreService.id !== null) {
                MembreService.update(vm.membreService, onSaveSuccess, onSaveError);
            } else {
                MembreService.save(vm.membreService, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:countryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
