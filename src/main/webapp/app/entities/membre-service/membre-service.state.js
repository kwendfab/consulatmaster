(function() {
    'use strict';

    angular
        .module('consulatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('membre-service', {
            parent: 'entity',
            url: '/membre-service?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'MembreServices'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/membre-service/membre-services.html',
                    controller: 'MembreServiceController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('membre-service-detail', {
            parent: 'membre-service',
            url: '/membre-service/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'MembreService'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/membre-service/membre-service-detail.html',
                    controller: 'MembreServiceDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'MembreService', function($stateParams, MembreService) {
                    return MembreService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'membre-service',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('membre-service-detail.edit', {
            parent: 'membre-service-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/membre-service/membre-service-dialog.html',
                    controller: 'MembreServiceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MembreService', function(MembreService) {
                            return MembreService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('membre-service.new', {
            parent: 'membre-service',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/membre-service/membre-service-dialog.html',
                    controller: 'MembreServiceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numero: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('membre-service', null, { reload: 'membre-service' });
                }, function() {
                    $state.go('membre-service');
                });
            }]
        })
        .state('membre-service.edit', {
            parent: 'membre-service',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/membre-service/membre-service-dialog.html',
                    controller: 'MembreServiceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MembreService', function(MembreService) {
                            return MembreService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('membre-service', null, { reload: 'membre-service' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('membre-service.delete', {
            parent: 'membre-service',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/membre-service/membre-service-delete-dialog.html',
                    controller: 'MembreServiceDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MembreService', function(MembreService) {
                            return MembreService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('membre-service', null, { reload: 'membre-service' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
