(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('PosteDialogController', PosteDialogController);

    PosteDialogController.$inject = ['$timeout', '$scope', '$uibModalInstance', 'entity', 'Poste', 'InfoPoste'];

    function PosteDialogController ($timeout, $scope, $uibModalInstance, entity, Poste, InfoPoste) {
        var vm = this;

        vm.poste = entity;
        vm.clear = clear;
        vm.save = save;
        vm.infopostes = InfoPoste.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.poste.id !== null) {
                Poste.update(vm.poste, onSaveSuccess, onSaveError);
            } else {
                Poste.save(vm.poste, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:posteUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
