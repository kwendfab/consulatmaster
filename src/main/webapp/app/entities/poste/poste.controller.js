(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('PosteController', PosteController);

    PosteController.$inject = ['$scope', 'Poste', 'TableService'];

    function PosteController($scope, Poste, TableService) {

        var vm = this;

        var addItemData ={
            templateUrl: 'app/entities/poste/poste-dialog.html',
            controller: 'PosteDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'mg',
            resolve: {
                entity: function () {
                    return {
                        id: null,
                        numero:null,
                        libelle: null,
                        description:null
                    };
                }
            }
        };

        /**
         *Modification d'un élément
         */
        var editItemData = {
            templateUrl: 'app/entities/poste/poste-dialog.html',
            controller: 'PosteDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'md',
            resolve: {
                entity: null
            }
        };

        /**
         * Suppresion d'un élément
         */
        var deleteItemData ={
            templateUrl: 'app/entities/poste/poste-delete-dialog.html',
            controller: 'PosteDeleteController',
            controllerAs: 'vm',
            size: 'sm',
            resolve: {
                entity: null
            }
        };

          var detailItemData = {
            templateUrl: 'app/entities/poste/poste-detail.html',
            controller: 'PosteDetailController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'md',
            resolve: {
                entity: null, //valeur déterminé à l'appel de la boite
                 isViewing: true
            }
        };

        var dialogData = {
            addData: addItemData,
            editData: editItemData,
            deleteData: deleteItemData,
            detailData: detailItemData
        };
        var entitySearchParams = function() {
            return {

            };
        };

        var entityParams = function() {
            return {

            };
        };

        var datas = {
            scope: $scope,
            vm: vm,
            entity: Poste,
            entitySearch: Poste,
            dialogData: dialogData,
            entitySearchParams: entitySearchParams,
            entityParams: entityParams
        };
        TableService.init(datas);
    }
})();