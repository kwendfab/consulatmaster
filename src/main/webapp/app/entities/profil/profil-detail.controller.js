(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('ProfilDetailController', ProfilDetailController);

    ProfilDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Profil', 'Authority', 'User'];

    function ProfilDetailController($scope, $rootScope, $stateParams, previousState, entity, Profil, Authority, User) {
        var vm = this;

        vm.profil = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('consulatApp:profilUpdate', function(event, result) {
            vm.profil = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
