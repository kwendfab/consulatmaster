(function() {
    'use strict';
    angular
        .module('consulatApp')
        .factory('Profil', Profil)
        .factory('ProfilByUser', ProfilByUser);


    Profil.$inject = ['$resource'];
    ProfilByUser.$inject = ['$resource'];

    
    function ProfilByUser ($resource) {
        var resourceUrl =  'api/profils/:byUserCreated';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }

    function Profil ($resource) {
        var resourceUrl =  'api/profils/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
