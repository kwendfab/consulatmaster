(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('StatesDeleteController',StatesDeleteController);

    StatesDeleteController.$inject = ['$uibModalInstance', 'entity', 'States'];

    function StatesDeleteController($uibModalInstance, entity, States) {
        var vm = this;

        vm.states = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            States.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
