(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('StatesDialogController', StatesDialogController);

    StatesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'States', 'City', 'Country'];

    function StatesDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, States, City, Country) {
        var vm = this;

        vm.states = entity;
        vm.clear = clear;
        vm.save = save;
      


        vm.countryColumns = [{
            name: 'libele',
            title: 'Libelle'
        }];
        $scope.$on("onLoadCountries", function () {

        });
        loadCountries();

        function loadCountries() {
            Country.query({
                size: 10000
            }, function (data) {
                console.log("les pays");
                if (data != null) {
                    console.log(data);
                    vm.countries = [];
                    vm.countries = data;
                }
            });


           // vm.members.lieuNaissance = test.libele;
        }









        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.states.id !== null) {
                States.update(vm.states, onSaveSuccess, onSaveError);
            } else {
                States.save(vm.states, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('consulatApp:cityUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
