(function() {
    'use strict';
    angular
        .module('consulatApp')
        .factory('States', States)
        .factory('StatesByCountry', StatesByCountry);

    States.$inject = ['$resource'];
    StatesByCountry.$inject = ['$resource'];

    function StatesByCountry ($resource) {
        var resourceUrl =  'api/states/country/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }

    function States ($resource) {
        var resourceUrl =  'api/states/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
