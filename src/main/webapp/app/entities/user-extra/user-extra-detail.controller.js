(function() {
    'use strict';

    angular
        .module('consulatApp')
        .controller('UserExtraDetailController', UserExtraDetailController);

    UserExtraDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'UserExtra', 'User', 'Consulat', 'Profil', 'Members'];

    function UserExtraDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, UserExtra, User, Consulat, Profil, Members) {
        var vm = this;

        vm.userExtra = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('consulatApp:userExtraUpdate', function(event, result) {
            vm.userExtra = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
