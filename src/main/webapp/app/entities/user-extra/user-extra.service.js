(function() {
    'use strict';
    angular
        .module('consulatApp')
        .factory('UserExtra', UserExtra)
        .factory('UserExtraConnected', UserExtraConnected);

    UserExtra.$inject = ['$resource'];
    UserExtraConnected.$inject = ['$resource'];

    function UserExtra ($resource) {
        var resourceUrl =  'api/user-extras/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }

    function UserExtraConnected ($resource) {
        var resourceUrl =  'api/user-extras/connected';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: false},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }






})();
